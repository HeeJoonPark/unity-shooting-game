﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    Transform startParent;
    private GameObject EquipSound;
    private Transform player;
    Transform transform0;
    Transform transform1;
    Transform transform2;
    Transform transform3;
    Vector2 WorldPoint;
    private Camera cam;
    private float a;
    private float b;
    private float c;
    private float d;
    private float min;

    public void Start()
    {
        cam = Camera.main;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        EquipSound = GameObject.Find("EquipSound");
    }

    public void OnBeginDrag(PointerEventData eventData)
    {

        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Application.isEditor) transform.position = Input.mousePosition;
        else transform.position = Input.GetTouch(0).position;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        min = 0;

        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        if (Application.isEditor) WorldPoint = cam.ScreenToWorldPoint(Input.mousePosition);
        else WorldPoint = cam.ScreenToWorldPoint(Input.GetTouch(0).position);
        Recursive(player, WorldPoint);
        if (transform.parent == startParent)
            transform.position = startPosition;
    }

    public void Recursive(Transform trans, Vector2 WorldPoint)
    {
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        min = 0;
        transform0 = trans.GetChild(0);
        transform1 = trans.GetChild(1);
        transform2 = trans.GetChild(2);
        transform3 = trans.GetChild(3);
        a = Vector2.Distance(transform0.position, WorldPoint);
        b = Vector2.Distance(transform1.position, WorldPoint);
        c = Vector2.Distance(transform2.position, WorldPoint);
        d = Vector2.Distance(transform3.position, WorldPoint);
        min = a;
        if (min > b)
        {
            min = b;
        }
        if (min > c)
        {
            min = c;
        }
        if (min > d)
        {
            min = d;
        }
            if (System.Math.Abs(min - a) < 0.01)
            {

                if (a <= 0.4)
                {
                    if (transform0.childCount <= 0)
                    {
                        GetComponent<Spawn>().SpawnDroppedItem(transform0, 0);
                        GameObject equipSound = Instantiate(EquipSound);
                        Destroy(equipSound, 1);
                        Destroy(gameObject);

                }
                    else { return; }
                }
                else
                {
                    if (transform0.childCount > 0)
                    {
                        if (transform0.GetChild(0).GetChild(0).childCount != 4)
                        {
                            if (b < c)
                            {
                                if (transform1.childCount > 0)
                                {
                                    if (transform1.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                            else
                            {
                                if (transform2.childCount > 0)
                                {
                                    if (transform2.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                        }
                        else
                        Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                    }
                    else
                    {
                        if (b < c)
                        {
                            if (transform1.childCount > 0)
                            {
                                if (transform1.GetChild(0).GetChild(0).childCount != 4) return;
                                Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                        else
                        {
                            if (transform2.childCount > 0)
                            {
                                if (transform2.GetChild(0).GetChild(0).childCount != 4) return;
                                Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                }
            }
            else if (System.Math.Abs(min - b) < 0.01)
            {
                if (b <= 0.4)
                {
                    if (transform1.childCount <= 0)
                    {
                        GetComponent<Spawn>().SpawnDroppedItem(transform1, 1);
                        GameObject equipSound = Instantiate(EquipSound);
                        Destroy(equipSound, 1);
                        Destroy(gameObject);
                    }
                    else { return; }
                }
                else
                {
                    if (transform1.childCount > 0)
                    {
                        if (transform1.GetChild(0).GetChild(0).childCount != 4)
                        {
                            if (a < d)
                            {
                                if (transform0.childCount > 0)
                                {
                                    if (transform0.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                            else
                            {
                                if (transform3.childCount > 0)
                                {
                                    if (transform3.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform3.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                        }
                        else
                        Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                    }
                    else
                    {
                        if (a < d)
                        {
                            if (transform0.childCount > 0)
                            {
                                if (transform0.GetChild(0).GetChild(0).childCount != 4) return;
                                Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                        else
                        {
                            if (transform3.childCount > 0)
                            {
                                if (transform3.GetChild(0).GetChild(0).childCount != 4) return;
                                Recursive2(transform3.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                }
            }
            else if (System.Math.Abs(min - c) < 0.01)
            {
                if (c <= 0.4)
                {
                    if (transform2.childCount <= 0)
                    {
                        GetComponent<Spawn>().SpawnDroppedItem(transform2, 2);
                        GameObject equipSound = Instantiate(EquipSound);
                        Destroy(equipSound, 1);
                        Destroy(gameObject);
                    }
                    else { return; }
                }
                else
                {
                    if (transform2.childCount > 0)
                    {
                        if (transform2.GetChild(0).GetChild(0).childCount != 4) 
                        {
                            if (a < d)
                            {
                                if (transform0.childCount > 0)
                                {
                                    if (transform0.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                            else
                            {
                                if (transform3.childCount > 0)
                                {
                                    if (transform3.GetChild(0).GetChild(0).childCount != 4) return;
                                    Recursive2(transform3.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                        }
                        else
                        Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                    }
                    else
                    {
                        if (a < d)
                        {
                            if (transform0.childCount > 0)
                            {
                               if (transform0.GetChild(0).GetChild(0).childCount != 4) return;
                               Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                        else
                        {
                            if (transform3.childCount > 0)
                            {
                                if (transform3.GetChild(0).GetChild(0).childCount != 4) return;
                                Recursive2(transform3.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                }
            }
            else if (System.Math.Abs(min - d) < 0.01)
            {
                if (d <= 0.4)
                {
                    if (transform3.childCount <= 0)
                    {
                        GetComponent<Spawn>().SpawnDroppedItem(transform3, 3);
                        GameObject equipSound = Instantiate(EquipSound);
                        Destroy(equipSound, 1);
                        Destroy(gameObject);
                    }
                    else { return; }
                }
                else
                {
                    if (transform3.childCount > 0)
                    {
                        if (transform3.GetChild(0).GetChild(0).childCount != 4) 
                        {
                            if (b < c)
                            {
                                if (transform1.childCount > 0)
                                {
                                    if (transform1.GetChild(0).GetChild(0).childCount != 4) return;

                                    Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                            else
                            {
                                if (transform2.childCount > 0)
                                {
                                    if (transform2.GetChild(0).GetChild(0).childCount != 4) return;

                                    Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                        }
                        else
                        Recursive2(transform3.GetChild(0).GetChild(0), WorldPoint);
                    }
                    else
                    {
                        if (b < c)
                        {
                            if (transform1.childCount > 0)
                            {
                            if (transform1.GetChild(0).GetChild(0).childCount != 4) return;

                            Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                        else
                        {
                            if (transform2.childCount > 0)
                            {
                            if (transform2.GetChild(0).GetChild(0).childCount != 4) return;

                            Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                }
            }
            else { return; }

    }
    public void Recursive2(Transform trans, Vector2 WorldPoint)
    {
        a = 0;
        b = 0;
        c = 0;
        min = 0;
        transform0 = trans.GetChild(0);
        transform1 = trans.GetChild(1);
        transform2 = trans.GetChild(2);
        a = Vector2.Distance(transform0.position, WorldPoint);
        b = Vector2.Distance(transform1.position, WorldPoint);
        c = Vector2.Distance(transform2.position, WorldPoint);
        min = a;
        if (min > b)
        {
            min = b;
        }
        if (min > c)
        {
            min = c;
        }
        if (System.Math.Abs(min - a) < 0.01)
        {

            if (a <= 0.4)
            {
                if (transform0.childCount <= 0)
                {
                    GetComponent<Spawn>().SpawnDroppedItem(transform0, 0);
                    GameObject equipSound = Instantiate(EquipSound);
                    Destroy(equipSound, 1);
                    Destroy(gameObject);
                }
                else { return; }
            }
            else
            {
                if (transform0.childCount > 0)
                {
                    if (transform0.GetChild(0).GetChild(0).childCount != 4)
                    {
                        if (b < c)
                        {
                            if (transform1.childCount > 0)
                            {
                                if (transform1.GetChild(0).GetChild(0).childCount != 4) return;

                                Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                        else
                        {
                            if (transform2.childCount > 0)
                            {
                                if (transform2.GetChild(0).GetChild(0).childCount != 4) return;

                                Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                    else
                    Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                }
                else
                {
                    if (b < c)
                    {
                        if (transform1.childCount > 0)
                        {
                            if (transform1.GetChild(0).GetChild(0).childCount != 4) 
                            {
                                if (transform2.childCount > 0)
                                {
                                    if (transform2.GetChild(0).GetChild(0).childCount != 4) return;

                                    Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                                }
                            }
                            else
                            Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                        }
                    }
                    else
                    {
                        if (transform2.childCount > 0)
                        {
                            if (transform2.GetChild(0).GetChild(0).childCount != 4) return;

                            Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                        }
                    }
                }
            }
        }
        else if (System.Math.Abs(min - b) < 0.01)
        {
            if (b <= 0.4)
            {
                if (transform1.childCount <= 0)
                {
                    GetComponent<Spawn>().SpawnDroppedItem(transform1, 1);
                    GameObject equipSound = Instantiate(EquipSound);
                    Destroy(equipSound, 1);
                    Destroy(gameObject);
                }
                else { return; }
            }
            else
            {
                if (transform1.childCount > 0)
                {
                    if (transform1.GetChild(0).GetChild(0).childCount != 4)
                    {
                        if (a < c)
                        {
                            if (transform0.childCount > 0)
                            {
                                if (transform0.GetChild(0).GetChild(0).childCount != 4) return;

                                Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                    else
                    Recursive2(transform1.GetChild(0).GetChild(0), WorldPoint);
                }
                else
                {
                    if (a < c)
                    {
                        if (transform0.childCount > 0)
                        {
                            if (transform0.GetChild(0).GetChild(0).childCount != 4) return;

                            Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                        }
                    }
                }
            }
        }
        else if (System.Math.Abs(min - c) < 0.01)
        {
            if (c <= 0.4)
            {
                if (transform2.childCount <= 0)
                {
                    GetComponent<Spawn>().SpawnDroppedItem(transform2, 2);
                    GameObject equipSound = Instantiate(EquipSound);
                    Destroy(equipSound, 1);
                    Destroy(gameObject);
                }
                else { return; }
            }
            else
            {
                if (transform2.childCount > 0)
                {
                    if (transform2.GetChild(0).GetChild(0).childCount != 4)
                    {
                        if (a < b)
                        {
                            if (transform0.childCount > 0)
                            {
                                if (transform0.GetChild(0).GetChild(0).childCount != 4) return;

                                Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                            }
                        }
                    }
                    else
                    Recursive2(transform2.GetChild(0).GetChild(0), WorldPoint);
                }
                else
                {
                    if (a < b)
                    {
                        if (transform0.childCount > 0)
                        {
                            if (transform0.GetChild(0).GetChild(0).childCount != 4) return;

                            Recursive2(transform0.GetChild(0).GetChild(0), WorldPoint);
                        }
                    }
                }
            }
        }
        else { return; }

    }
}
