﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public GameObject item;
    public GameObject ThreeDirection;
   
    public void SpawnDroppedItem(Transform trans,int i) {

        switch (i)
        {
            case 0:
                Instantiate(item, trans.position, trans.rotation).transform.SetParent(trans);
                if (ThreeDirection != null)
                    Instantiate(ThreeDirection, trans.position, trans.rotation).transform.SetParent(trans.GetChild(0));
                break;
            case 1:
                Instantiate(item, trans.position, trans.rotation).transform.SetParent(trans);
                if (ThreeDirection != null)
                    Instantiate(ThreeDirection, trans.position, trans.rotation).transform.SetParent(trans.GetChild(0));
                break;
            case 2:
                Instantiate(item, trans.position, trans.rotation).transform.SetParent(trans);
                if (ThreeDirection != null)
                    Instantiate(ThreeDirection, trans.position, trans.rotation).transform.SetParent(trans.GetChild(0));
                break;
            case 3:
                Instantiate(item, trans.position, trans.rotation).transform.SetParent(trans);
                if (ThreeDirection != null)
                    Instantiate(ThreeDirection, trans.position, trans.rotation).transform.SetParent(trans.GetChild(0));
                break;
        }
    }

}
