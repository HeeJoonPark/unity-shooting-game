﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SecondSlot : MonoBehaviour, IDropHandler
{

    public GameObject Item
    {
        get
        {
            return transform.childCount > 0 ? transform.GetChild(0).gameObject : null;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!Item)
        {
            DragHandler.itemBeingDragged.transform.SetParent(transform);
        }
    }
}
