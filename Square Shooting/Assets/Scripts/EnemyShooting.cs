﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    public GameObject bulletPrefab;

    public float fireDelay;

    private float Distance;

    public float distance = 6;

    private float cooldownTimer = 0.3f;

    // Update is called once per frame
    void Update()
    {
        GameObject go = GameObject.Find("Player");

        if (go != null)
        {
            Distance = Vector2.Distance(go.transform.position, transform.position);

            if (Distance < distance)
            {
                cooldownTimer -= Time.deltaTime;

                if (cooldownTimer <= 0)
                {
                    //Shoot!
                    cooldownTimer = fireDelay;

                    Instantiate(bulletPrefab, transform.position, transform.rotation);
                }
            }
        }
    }
}
