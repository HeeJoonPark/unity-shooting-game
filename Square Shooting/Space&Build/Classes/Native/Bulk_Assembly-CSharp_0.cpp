﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CameraFollow
struct CameraFollow_t129522575;
// ChangeMusicVolume
struct ChangeMusicVolume_t2599693512;
// ControllerScript
struct ControllerScript_t326548708;
// DamageHandler
struct DamageHandler_t2022420323;
// DamageHandlerEnemy
struct DamageHandlerEnemy_t1319748984;
// DamageHandlerHeart
struct DamageHandlerHeart_t1292438650;
// DragHandler
struct DragHandler_t4187724030;
// EnemyMove
struct EnemyMove_t2177327048;
// EnemyShooting
struct EnemyShooting_t2912033273;
// EnemySpawner
struct EnemySpawner_t2006493939;
// FacesEnemy
struct FacesEnemy_t493038826;
// FixedJoystick
struct FixedJoystick_t2618381211;
// FloatingJoystick
struct FloatingJoystick_t3402721920;
// FollowPlayer
struct FollowPlayer_t2788059413;
// Inventory
struct Inventory_t1050226016;
// JoyStick
struct JoyStick_t2364599988;
// Joystick
struct Joystick_t9498292;
// MainMenu
struct MainMenu_t3798339593;
// MoveForward
struct MoveForward_t2564727450;
// PauseMenu
struct PauseMenu_t3916167947;
// Pickup
struct Pickup_t999226966;
// PlayerController
struct PlayerController_t2064355688;
// PlayerMovement
struct PlayerMovement_t2731566919;
// PlayerShooting
struct PlayerShooting_t3504758017;
// SecondSlot
struct SecondSlot_t2904157877;
// SelfDestruct
struct SelfDestruct_t1181332799;
// Slot
struct Slot_t3333057830;
// Spawn
struct Spawn_t617419884;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t819399007;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TouchEvent
struct TouchEvent_t1418210080;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t3180273144;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// VariableJoystick
struct VariableJoystick_t2643911586;

extern RuntimeClass* Camera_t4157153871_il2cpp_TypeInfo_var;
extern RuntimeClass* DragHandler_t4187724030_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* PauseMenu_t3916167947_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_t3600365921_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1536896214;
extern String_t* _stringLiteral1555075383;
extern String_t* _stringLiteral1828639942;
extern String_t* _stringLiteral2176724968;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral2590551002;
extern String_t* _stringLiteral2670505167;
extern String_t* _stringLiteral2769116279;
extern String_t* _stringLiteral2804170526;
extern String_t* _stringLiteral2984908384;
extern String_t* _stringLiteral3326048179;
extern String_t* _stringLiteral3517658432;
extern String_t* _stringLiteral3567541360;
extern String_t* _stringLiteral3605365887;
extern String_t* _stringLiteral3914663099;
extern String_t* _stringLiteral3956951295;
extern String_t* _stringLiteral4200480571;
extern String_t* _stringLiteral760905195;
extern String_t* _stringLiteral811387782;
extern const RuntimeMethod* Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisInventory_t1050226016_m3648803814_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m4130575780_RuntimeMethod_var;
extern const uint32_t CameraFollow_Update_m2790158597_MetadataUsageId;
extern const uint32_t DamageHandlerEnemy_Die_m3879310850_MetadataUsageId;
extern const uint32_t DamageHandlerEnemy_OnTriggerEnter2D_m2407517138_MetadataUsageId;
extern const uint32_t DamageHandlerEnemy_Start_m408010825_MetadataUsageId;
extern const uint32_t DamageHandlerEnemy_Update_m3999026071_MetadataUsageId;
extern const uint32_t DamageHandlerHeart_Die_m3960869623_MetadataUsageId;
extern const uint32_t DamageHandlerHeart_OnTriggerEnter2D_m3333993096_MetadataUsageId;
extern const uint32_t DamageHandlerHeart_Start_m1905279168_MetadataUsageId;
extern const uint32_t DamageHandler_Die_m1148011466_MetadataUsageId;
extern const uint32_t DamageHandler_OnTriggerEnter2D_m1389998856_MetadataUsageId;
extern const uint32_t DamageHandler_Start_m1242061931_MetadataUsageId;
extern const uint32_t DragHandler_OnBeginDrag_m3151166853_MetadataUsageId;
extern const uint32_t DragHandler_OnDrag_m2523973970_MetadataUsageId;
extern const uint32_t DragHandler_OnEndDrag_m2610952961_MetadataUsageId;
extern const uint32_t DragHandler_Recursive2_m1786376718_MetadataUsageId;
extern const uint32_t DragHandler_Recursive_m2344347231_MetadataUsageId;
extern const uint32_t DragHandler_Start_m866278565_MetadataUsageId;
extern const uint32_t EnemyMove_Update_m1644472081_MetadataUsageId;
extern const uint32_t EnemyShooting_Update_m2738480705_MetadataUsageId;
extern const uint32_t EnemySpawner_Update_m306438552_MetadataUsageId;
extern const uint32_t FacesEnemy_GetClosestEnemy_m4059716479_MetadataUsageId;
extern const uint32_t FacesEnemy_Start_m2796397225_MetadataUsageId;
extern const uint32_t FacesEnemy_Update_m577662581_MetadataUsageId;
extern const uint32_t FacesEnemy__ctor_m1348940205_MetadataUsageId;
extern const uint32_t FixedJoystick_OnDrag_m1042755941_MetadataUsageId;
extern const uint32_t FixedJoystick_OnPointerUp_m763078753_MetadataUsageId;
extern const uint32_t FixedJoystick_Start_m649713002_MetadataUsageId;
extern const uint32_t FixedJoystick__ctor_m3165509026_MetadataUsageId;
extern const uint32_t FloatingJoystick_OnDrag_m1849813291_MetadataUsageId;
extern const uint32_t FloatingJoystick_OnPointerDown_m3753834403_MetadataUsageId;
extern const uint32_t FloatingJoystick_OnPointerUp_m2581807680_MetadataUsageId;
extern const uint32_t FloatingJoystick__ctor_m612783692_MetadataUsageId;
extern const uint32_t FollowPlayer_Update_m1580389478_MetadataUsageId;
extern const uint32_t JoyStick_OnDrag_m884893215_MetadataUsageId;
extern const uint32_t JoyStick_OnPointerUp_m2954618842_MetadataUsageId;
extern const uint32_t JoyStick_Start_m50952947_MetadataUsageId;
extern const uint32_t Joystick__ctor_m1272315817_MetadataUsageId;
extern const uint32_t MoveForward_Update_m1581178352_MetadataUsageId;
extern const uint32_t PauseMenu_LoadMenu_m3831042958_MetadataUsageId;
extern const uint32_t PauseMenu_Pause_m890285182_MetadataUsageId;
extern const uint32_t PauseMenu_Paused_m2854264695_MetadataUsageId;
extern const uint32_t PauseMenu_QuitGame_m3587716712_MetadataUsageId;
extern const uint32_t PauseMenu_Resume_m690786508_MetadataUsageId;
extern const uint32_t PauseMenu_Start_m517218302_MetadataUsageId;
extern const uint32_t Pickup_OnTriggerEnter2D_m3957925105_MetadataUsageId;
extern const uint32_t Pickup_Start_m928129965_MetadataUsageId;
extern const uint32_t PlayerController_Update_m848427540_MetadataUsageId;
extern const uint32_t PlayerMovement_Update_m1995258020_MetadataUsageId;
extern const uint32_t PlayerShooting_GetClosestEnemy_m980920295_MetadataUsageId;
extern const uint32_t PlayerShooting_Start_m3760994824_MetadataUsageId;
extern const uint32_t PlayerShooting_Update_m411263274_MetadataUsageId;
extern const uint32_t PlayerShooting__ctor_m2527024264_MetadataUsageId;
extern const uint32_t SecondSlot_OnDrop_m1888909481_MetadataUsageId;
extern const uint32_t SelfDestruct_Update_m3543763843_MetadataUsageId;
extern const uint32_t Slot_DropItem_m733037294_MetadataUsageId;
extern const uint32_t Slot_Start_m2837351715_MetadataUsageId;
extern const uint32_t Spawn_SpawnDroppedItem_m1584144246_MetadataUsageId;
extern const uint32_t TouchEvent_Update_m3259198139_MetadataUsageId;
extern const uint32_t VariableJoystick_OnDrag_m3080315221_MetadataUsageId;
extern const uint32_t VariableJoystick_OnFixed_m4141959043_MetadataUsageId;
extern const uint32_t VariableJoystick_OnFloat_m165815926_MetadataUsageId;
extern const uint32_t VariableJoystick_OnPointerDown_m4235218688_MetadataUsageId;
extern const uint32_t VariableJoystick_OnPointerUp_m3792644295_MetadataUsageId;
extern const uint32_t VariableJoystick__ctor_m1597116632_MetadataUsageId;

struct BooleanU5BU5D_t2897418192;
struct GameObjectU5BU5D_t3328599146;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SCENE_T2348375561_H
#define SCENE_T2348375561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t2348375561 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t2348375561, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T2348375561_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef JOYSTICKMODE_T3732604888_H
#define JOYSTICKMODE_T3732604888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickMode
struct  JoystickMode_t3732604888 
{
public:
	// System.Int32 JoystickMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoystickMode_t3732604888, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMODE_T3732604888_H
#ifndef DRIVENTRANSFORMPROPERTIES_T3813433528_H
#define DRIVENTRANSFORMPROPERTIES_T3813433528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t3813433528 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t3813433528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENTRANSFORMPROPERTIES_T3813433528_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef DIRECTION_T337909235_H
#define DIRECTION_T337909235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t337909235 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t337909235, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T337909235_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t190067161 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t190067161 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t190067161 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t190067161 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t190067161 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t190067161 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:
	// UnityEngine.Object UnityEngine.RectTransform::<drivenByObject>k__BackingField
	Object_t631007953 * ___U3CdrivenByObjectU3Ek__BackingField_6;
	// UnityEngine.DrivenTransformProperties UnityEngine.RectTransform::<drivenProperties>k__BackingField
	int32_t ___U3CdrivenPropertiesU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CdrivenByObjectU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025, ___U3CdrivenByObjectU3Ek__BackingField_6)); }
	inline Object_t631007953 * get_U3CdrivenByObjectU3Ek__BackingField_6() const { return ___U3CdrivenByObjectU3Ek__BackingField_6; }
	inline Object_t631007953 ** get_address_of_U3CdrivenByObjectU3Ek__BackingField_6() { return &___U3CdrivenByObjectU3Ek__BackingField_6; }
	inline void set_U3CdrivenByObjectU3Ek__BackingField_6(Object_t631007953 * value)
	{
		___U3CdrivenByObjectU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdrivenByObjectU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CdrivenPropertiesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025, ___U3CdrivenPropertiesU3Ek__BackingField_7)); }
	inline int32_t get_U3CdrivenPropertiesU3Ek__BackingField_7() const { return ___U3CdrivenPropertiesU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CdrivenPropertiesU3Ek__BackingField_7() { return &___U3CdrivenPropertiesU3Ek__BackingField_7; }
	inline void set_U3CdrivenPropertiesU3Ek__BackingField_7(int32_t value)
	{
		___U3CdrivenPropertiesU3Ek__BackingField_7 = value;
	}
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_5;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_5() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_5)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_5() const { return ___reapplyDrivenProperties_5; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_5() { return &___reapplyDrivenProperties_5; }
	inline void set_reapplyDrivenProperties_5(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_5 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef CAMERAFOLLOW_T129522575_H
#define CAMERAFOLLOW_T129522575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollow
struct  CameraFollow_t129522575  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraFollow::myTarget
	Transform_t3600365921 * ___myTarget_4;

public:
	inline static int32_t get_offset_of_myTarget_4() { return static_cast<int32_t>(offsetof(CameraFollow_t129522575, ___myTarget_4)); }
	inline Transform_t3600365921 * get_myTarget_4() const { return ___myTarget_4; }
	inline Transform_t3600365921 ** get_address_of_myTarget_4() { return &___myTarget_4; }
	inline void set_myTarget_4(Transform_t3600365921 * value)
	{
		___myTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___myTarget_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOW_T129522575_H
#ifndef CHANGEMUSICVOLUME_T2599693512_H
#define CHANGEMUSICVOLUME_T2599693512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChangeMusicVolume
struct  ChangeMusicVolume_t2599693512  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider ChangeMusicVolume::Volume
	Slider_t3903728902 * ___Volume_4;
	// UnityEngine.AudioSource ChangeMusicVolume::myMusic
	AudioSource_t3935305588 * ___myMusic_5;

public:
	inline static int32_t get_offset_of_Volume_4() { return static_cast<int32_t>(offsetof(ChangeMusicVolume_t2599693512, ___Volume_4)); }
	inline Slider_t3903728902 * get_Volume_4() const { return ___Volume_4; }
	inline Slider_t3903728902 ** get_address_of_Volume_4() { return &___Volume_4; }
	inline void set_Volume_4(Slider_t3903728902 * value)
	{
		___Volume_4 = value;
		Il2CppCodeGenWriteBarrier((&___Volume_4), value);
	}

	inline static int32_t get_offset_of_myMusic_5() { return static_cast<int32_t>(offsetof(ChangeMusicVolume_t2599693512, ___myMusic_5)); }
	inline AudioSource_t3935305588 * get_myMusic_5() const { return ___myMusic_5; }
	inline AudioSource_t3935305588 ** get_address_of_myMusic_5() { return &___myMusic_5; }
	inline void set_myMusic_5(AudioSource_t3935305588 * value)
	{
		___myMusic_5 = value;
		Il2CppCodeGenWriteBarrier((&___myMusic_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGEMUSICVOLUME_T2599693512_H
#ifndef CONTROLLERSCRIPT_T326548708_H
#define CONTROLLERSCRIPT_T326548708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControllerScript
struct  ControllerScript_t326548708  : public MonoBehaviour_t3962482529
{
public:
	// FloatingJoystick ControllerScript::joystick
	FloatingJoystick_t3402721920 * ___joystick_4;
	// System.Single ControllerScript::MoveSpeed
	float ___MoveSpeed_5;
	// System.Single ControllerScript::h
	float ___h_6;
	// System.Single ControllerScript::v
	float ___v_7;

public:
	inline static int32_t get_offset_of_joystick_4() { return static_cast<int32_t>(offsetof(ControllerScript_t326548708, ___joystick_4)); }
	inline FloatingJoystick_t3402721920 * get_joystick_4() const { return ___joystick_4; }
	inline FloatingJoystick_t3402721920 ** get_address_of_joystick_4() { return &___joystick_4; }
	inline void set_joystick_4(FloatingJoystick_t3402721920 * value)
	{
		___joystick_4 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_4), value);
	}

	inline static int32_t get_offset_of_MoveSpeed_5() { return static_cast<int32_t>(offsetof(ControllerScript_t326548708, ___MoveSpeed_5)); }
	inline float get_MoveSpeed_5() const { return ___MoveSpeed_5; }
	inline float* get_address_of_MoveSpeed_5() { return &___MoveSpeed_5; }
	inline void set_MoveSpeed_5(float value)
	{
		___MoveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_h_6() { return static_cast<int32_t>(offsetof(ControllerScript_t326548708, ___h_6)); }
	inline float get_h_6() const { return ___h_6; }
	inline float* get_address_of_h_6() { return &___h_6; }
	inline void set_h_6(float value)
	{
		___h_6 = value;
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(ControllerScript_t326548708, ___v_7)); }
	inline float get_v_7() const { return ___v_7; }
	inline float* get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(float value)
	{
		___v_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSCRIPT_T326548708_H
#ifndef DAMAGEHANDLER_T2022420323_H
#define DAMAGEHANDLER_T2022420323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageHandler
struct  DamageHandler_t2022420323  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DamageHandler::health
	int32_t ___health_4;
	// UnityEngine.GameObject DamageHandler::explode
	GameObject_t1113636619 * ___explode_5;
	// UnityEngine.GameObject DamageHandler::expsound
	GameObject_t1113636619 * ___expsound_6;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(DamageHandler_t2022420323, ___health_4)); }
	inline int32_t get_health_4() const { return ___health_4; }
	inline int32_t* get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(int32_t value)
	{
		___health_4 = value;
	}

	inline static int32_t get_offset_of_explode_5() { return static_cast<int32_t>(offsetof(DamageHandler_t2022420323, ___explode_5)); }
	inline GameObject_t1113636619 * get_explode_5() const { return ___explode_5; }
	inline GameObject_t1113636619 ** get_address_of_explode_5() { return &___explode_5; }
	inline void set_explode_5(GameObject_t1113636619 * value)
	{
		___explode_5 = value;
		Il2CppCodeGenWriteBarrier((&___explode_5), value);
	}

	inline static int32_t get_offset_of_expsound_6() { return static_cast<int32_t>(offsetof(DamageHandler_t2022420323, ___expsound_6)); }
	inline GameObject_t1113636619 * get_expsound_6() const { return ___expsound_6; }
	inline GameObject_t1113636619 ** get_address_of_expsound_6() { return &___expsound_6; }
	inline void set_expsound_6(GameObject_t1113636619 * value)
	{
		___expsound_6 = value;
		Il2CppCodeGenWriteBarrier((&___expsound_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEHANDLER_T2022420323_H
#ifndef DAMAGEHANDLERENEMY_T1319748984_H
#define DAMAGEHANDLERENEMY_T1319748984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageHandlerEnemy
struct  DamageHandlerEnemy_t1319748984  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DamageHandlerEnemy::SpawnItem
	GameObject_t1113636619 * ___SpawnItem_4;
	// System.Int32 DamageHandlerEnemy::health
	int32_t ___health_5;
	// UnityEngine.GameObject DamageHandlerEnemy::explode
	GameObject_t1113636619 * ___explode_6;
	// UnityEngine.GameObject DamageHandlerEnemy::expsound
	GameObject_t1113636619 * ___expsound_7;

public:
	inline static int32_t get_offset_of_SpawnItem_4() { return static_cast<int32_t>(offsetof(DamageHandlerEnemy_t1319748984, ___SpawnItem_4)); }
	inline GameObject_t1113636619 * get_SpawnItem_4() const { return ___SpawnItem_4; }
	inline GameObject_t1113636619 ** get_address_of_SpawnItem_4() { return &___SpawnItem_4; }
	inline void set_SpawnItem_4(GameObject_t1113636619 * value)
	{
		___SpawnItem_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnItem_4), value);
	}

	inline static int32_t get_offset_of_health_5() { return static_cast<int32_t>(offsetof(DamageHandlerEnemy_t1319748984, ___health_5)); }
	inline int32_t get_health_5() const { return ___health_5; }
	inline int32_t* get_address_of_health_5() { return &___health_5; }
	inline void set_health_5(int32_t value)
	{
		___health_5 = value;
	}

	inline static int32_t get_offset_of_explode_6() { return static_cast<int32_t>(offsetof(DamageHandlerEnemy_t1319748984, ___explode_6)); }
	inline GameObject_t1113636619 * get_explode_6() const { return ___explode_6; }
	inline GameObject_t1113636619 ** get_address_of_explode_6() { return &___explode_6; }
	inline void set_explode_6(GameObject_t1113636619 * value)
	{
		___explode_6 = value;
		Il2CppCodeGenWriteBarrier((&___explode_6), value);
	}

	inline static int32_t get_offset_of_expsound_7() { return static_cast<int32_t>(offsetof(DamageHandlerEnemy_t1319748984, ___expsound_7)); }
	inline GameObject_t1113636619 * get_expsound_7() const { return ___expsound_7; }
	inline GameObject_t1113636619 ** get_address_of_expsound_7() { return &___expsound_7; }
	inline void set_expsound_7(GameObject_t1113636619 * value)
	{
		___expsound_7 = value;
		Il2CppCodeGenWriteBarrier((&___expsound_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEHANDLERENEMY_T1319748984_H
#ifndef DAMAGEHANDLERHEART_T1292438650_H
#define DAMAGEHANDLERHEART_T1292438650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageHandlerHeart
struct  DamageHandlerHeart_t1292438650  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 DamageHandlerHeart::health
	int32_t ___health_4;
	// UnityEngine.GameObject DamageHandlerHeart::explode
	GameObject_t1113636619 * ___explode_5;
	// UnityEngine.GameObject DamageHandlerHeart::expsound
	GameObject_t1113636619 * ___expsound_6;
	// UnityEngine.UI.Slider DamageHandlerHeart::hpSlider
	Slider_t3903728902 * ___hpSlider_7;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(DamageHandlerHeart_t1292438650, ___health_4)); }
	inline int32_t get_health_4() const { return ___health_4; }
	inline int32_t* get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(int32_t value)
	{
		___health_4 = value;
	}

	inline static int32_t get_offset_of_explode_5() { return static_cast<int32_t>(offsetof(DamageHandlerHeart_t1292438650, ___explode_5)); }
	inline GameObject_t1113636619 * get_explode_5() const { return ___explode_5; }
	inline GameObject_t1113636619 ** get_address_of_explode_5() { return &___explode_5; }
	inline void set_explode_5(GameObject_t1113636619 * value)
	{
		___explode_5 = value;
		Il2CppCodeGenWriteBarrier((&___explode_5), value);
	}

	inline static int32_t get_offset_of_expsound_6() { return static_cast<int32_t>(offsetof(DamageHandlerHeart_t1292438650, ___expsound_6)); }
	inline GameObject_t1113636619 * get_expsound_6() const { return ___expsound_6; }
	inline GameObject_t1113636619 ** get_address_of_expsound_6() { return &___expsound_6; }
	inline void set_expsound_6(GameObject_t1113636619 * value)
	{
		___expsound_6 = value;
		Il2CppCodeGenWriteBarrier((&___expsound_6), value);
	}

	inline static int32_t get_offset_of_hpSlider_7() { return static_cast<int32_t>(offsetof(DamageHandlerHeart_t1292438650, ___hpSlider_7)); }
	inline Slider_t3903728902 * get_hpSlider_7() const { return ___hpSlider_7; }
	inline Slider_t3903728902 ** get_address_of_hpSlider_7() { return &___hpSlider_7; }
	inline void set_hpSlider_7(Slider_t3903728902 * value)
	{
		___hpSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___hpSlider_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEHANDLERHEART_T1292438650_H
#ifndef DRAGHANDLER_T4187724030_H
#define DRAGHANDLER_T4187724030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragHandler
struct  DragHandler_t4187724030  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 DragHandler::startPosition
	Vector3_t3722313464  ___startPosition_5;
	// UnityEngine.Transform DragHandler::startParent
	Transform_t3600365921 * ___startParent_6;
	// UnityEngine.GameObject DragHandler::EquipSound
	GameObject_t1113636619 * ___EquipSound_7;
	// UnityEngine.Transform DragHandler::player
	Transform_t3600365921 * ___player_8;
	// UnityEngine.Transform DragHandler::transform0
	Transform_t3600365921 * ___transform0_9;
	// UnityEngine.Transform DragHandler::transform1
	Transform_t3600365921 * ___transform1_10;
	// UnityEngine.Transform DragHandler::transform2
	Transform_t3600365921 * ___transform2_11;
	// UnityEngine.Transform DragHandler::transform3
	Transform_t3600365921 * ___transform3_12;
	// UnityEngine.Vector2 DragHandler::WorldPoint
	Vector2_t2156229523  ___WorldPoint_13;
	// UnityEngine.Camera DragHandler::cam
	Camera_t4157153871 * ___cam_14;
	// System.Single DragHandler::a
	float ___a_15;
	// System.Single DragHandler::b
	float ___b_16;
	// System.Single DragHandler::c
	float ___c_17;
	// System.Single DragHandler::d
	float ___d_18;
	// System.Single DragHandler::min
	float ___min_19;

public:
	inline static int32_t get_offset_of_startPosition_5() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___startPosition_5)); }
	inline Vector3_t3722313464  get_startPosition_5() const { return ___startPosition_5; }
	inline Vector3_t3722313464 * get_address_of_startPosition_5() { return &___startPosition_5; }
	inline void set_startPosition_5(Vector3_t3722313464  value)
	{
		___startPosition_5 = value;
	}

	inline static int32_t get_offset_of_startParent_6() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___startParent_6)); }
	inline Transform_t3600365921 * get_startParent_6() const { return ___startParent_6; }
	inline Transform_t3600365921 ** get_address_of_startParent_6() { return &___startParent_6; }
	inline void set_startParent_6(Transform_t3600365921 * value)
	{
		___startParent_6 = value;
		Il2CppCodeGenWriteBarrier((&___startParent_6), value);
	}

	inline static int32_t get_offset_of_EquipSound_7() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___EquipSound_7)); }
	inline GameObject_t1113636619 * get_EquipSound_7() const { return ___EquipSound_7; }
	inline GameObject_t1113636619 ** get_address_of_EquipSound_7() { return &___EquipSound_7; }
	inline void set_EquipSound_7(GameObject_t1113636619 * value)
	{
		___EquipSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___EquipSound_7), value);
	}

	inline static int32_t get_offset_of_player_8() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___player_8)); }
	inline Transform_t3600365921 * get_player_8() const { return ___player_8; }
	inline Transform_t3600365921 ** get_address_of_player_8() { return &___player_8; }
	inline void set_player_8(Transform_t3600365921 * value)
	{
		___player_8 = value;
		Il2CppCodeGenWriteBarrier((&___player_8), value);
	}

	inline static int32_t get_offset_of_transform0_9() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___transform0_9)); }
	inline Transform_t3600365921 * get_transform0_9() const { return ___transform0_9; }
	inline Transform_t3600365921 ** get_address_of_transform0_9() { return &___transform0_9; }
	inline void set_transform0_9(Transform_t3600365921 * value)
	{
		___transform0_9 = value;
		Il2CppCodeGenWriteBarrier((&___transform0_9), value);
	}

	inline static int32_t get_offset_of_transform1_10() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___transform1_10)); }
	inline Transform_t3600365921 * get_transform1_10() const { return ___transform1_10; }
	inline Transform_t3600365921 ** get_address_of_transform1_10() { return &___transform1_10; }
	inline void set_transform1_10(Transform_t3600365921 * value)
	{
		___transform1_10 = value;
		Il2CppCodeGenWriteBarrier((&___transform1_10), value);
	}

	inline static int32_t get_offset_of_transform2_11() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___transform2_11)); }
	inline Transform_t3600365921 * get_transform2_11() const { return ___transform2_11; }
	inline Transform_t3600365921 ** get_address_of_transform2_11() { return &___transform2_11; }
	inline void set_transform2_11(Transform_t3600365921 * value)
	{
		___transform2_11 = value;
		Il2CppCodeGenWriteBarrier((&___transform2_11), value);
	}

	inline static int32_t get_offset_of_transform3_12() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___transform3_12)); }
	inline Transform_t3600365921 * get_transform3_12() const { return ___transform3_12; }
	inline Transform_t3600365921 ** get_address_of_transform3_12() { return &___transform3_12; }
	inline void set_transform3_12(Transform_t3600365921 * value)
	{
		___transform3_12 = value;
		Il2CppCodeGenWriteBarrier((&___transform3_12), value);
	}

	inline static int32_t get_offset_of_WorldPoint_13() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___WorldPoint_13)); }
	inline Vector2_t2156229523  get_WorldPoint_13() const { return ___WorldPoint_13; }
	inline Vector2_t2156229523 * get_address_of_WorldPoint_13() { return &___WorldPoint_13; }
	inline void set_WorldPoint_13(Vector2_t2156229523  value)
	{
		___WorldPoint_13 = value;
	}

	inline static int32_t get_offset_of_cam_14() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___cam_14)); }
	inline Camera_t4157153871 * get_cam_14() const { return ___cam_14; }
	inline Camera_t4157153871 ** get_address_of_cam_14() { return &___cam_14; }
	inline void set_cam_14(Camera_t4157153871 * value)
	{
		___cam_14 = value;
		Il2CppCodeGenWriteBarrier((&___cam_14), value);
	}

	inline static int32_t get_offset_of_a_15() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___a_15)); }
	inline float get_a_15() const { return ___a_15; }
	inline float* get_address_of_a_15() { return &___a_15; }
	inline void set_a_15(float value)
	{
		___a_15 = value;
	}

	inline static int32_t get_offset_of_b_16() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___b_16)); }
	inline float get_b_16() const { return ___b_16; }
	inline float* get_address_of_b_16() { return &___b_16; }
	inline void set_b_16(float value)
	{
		___b_16 = value;
	}

	inline static int32_t get_offset_of_c_17() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___c_17)); }
	inline float get_c_17() const { return ___c_17; }
	inline float* get_address_of_c_17() { return &___c_17; }
	inline void set_c_17(float value)
	{
		___c_17 = value;
	}

	inline static int32_t get_offset_of_d_18() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___d_18)); }
	inline float get_d_18() const { return ___d_18; }
	inline float* get_address_of_d_18() { return &___d_18; }
	inline void set_d_18(float value)
	{
		___d_18 = value;
	}

	inline static int32_t get_offset_of_min_19() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030, ___min_19)); }
	inline float get_min_19() const { return ___min_19; }
	inline float* get_address_of_min_19() { return &___min_19; }
	inline void set_min_19(float value)
	{
		___min_19 = value;
	}
};

struct DragHandler_t4187724030_StaticFields
{
public:
	// UnityEngine.GameObject DragHandler::itemBeingDragged
	GameObject_t1113636619 * ___itemBeingDragged_4;

public:
	inline static int32_t get_offset_of_itemBeingDragged_4() { return static_cast<int32_t>(offsetof(DragHandler_t4187724030_StaticFields, ___itemBeingDragged_4)); }
	inline GameObject_t1113636619 * get_itemBeingDragged_4() const { return ___itemBeingDragged_4; }
	inline GameObject_t1113636619 ** get_address_of_itemBeingDragged_4() { return &___itemBeingDragged_4; }
	inline void set_itemBeingDragged_4(GameObject_t1113636619 * value)
	{
		___itemBeingDragged_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemBeingDragged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGHANDLER_T4187724030_H
#ifndef ENEMYMOVE_T2177327048_H
#define ENEMYMOVE_T2177327048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyMove
struct  EnemyMove_t2177327048  : public MonoBehaviour_t3962482529
{
public:
	// System.Single EnemyMove::maxSpeed
	float ___maxSpeed_4;

public:
	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(EnemyMove_t2177327048, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYMOVE_T2177327048_H
#ifndef ENEMYSHOOTING_T2912033273_H
#define ENEMYSHOOTING_T2912033273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyShooting
struct  EnemyShooting_t2912033273  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject EnemyShooting::bulletPrefab
	GameObject_t1113636619 * ___bulletPrefab_4;
	// System.Single EnemyShooting::fireDelay
	float ___fireDelay_5;
	// System.Single EnemyShooting::Distance
	float ___Distance_6;
	// System.Single EnemyShooting::distance
	float ___distance_7;
	// System.Single EnemyShooting::cooldownTimer
	float ___cooldownTimer_8;

public:
	inline static int32_t get_offset_of_bulletPrefab_4() { return static_cast<int32_t>(offsetof(EnemyShooting_t2912033273, ___bulletPrefab_4)); }
	inline GameObject_t1113636619 * get_bulletPrefab_4() const { return ___bulletPrefab_4; }
	inline GameObject_t1113636619 ** get_address_of_bulletPrefab_4() { return &___bulletPrefab_4; }
	inline void set_bulletPrefab_4(GameObject_t1113636619 * value)
	{
		___bulletPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_4), value);
	}

	inline static int32_t get_offset_of_fireDelay_5() { return static_cast<int32_t>(offsetof(EnemyShooting_t2912033273, ___fireDelay_5)); }
	inline float get_fireDelay_5() const { return ___fireDelay_5; }
	inline float* get_address_of_fireDelay_5() { return &___fireDelay_5; }
	inline void set_fireDelay_5(float value)
	{
		___fireDelay_5 = value;
	}

	inline static int32_t get_offset_of_Distance_6() { return static_cast<int32_t>(offsetof(EnemyShooting_t2912033273, ___Distance_6)); }
	inline float get_Distance_6() const { return ___Distance_6; }
	inline float* get_address_of_Distance_6() { return &___Distance_6; }
	inline void set_Distance_6(float value)
	{
		___Distance_6 = value;
	}

	inline static int32_t get_offset_of_distance_7() { return static_cast<int32_t>(offsetof(EnemyShooting_t2912033273, ___distance_7)); }
	inline float get_distance_7() const { return ___distance_7; }
	inline float* get_address_of_distance_7() { return &___distance_7; }
	inline void set_distance_7(float value)
	{
		___distance_7 = value;
	}

	inline static int32_t get_offset_of_cooldownTimer_8() { return static_cast<int32_t>(offsetof(EnemyShooting_t2912033273, ___cooldownTimer_8)); }
	inline float get_cooldownTimer_8() const { return ___cooldownTimer_8; }
	inline float* get_address_of_cooldownTimer_8() { return &___cooldownTimer_8; }
	inline void set_cooldownTimer_8(float value)
	{
		___cooldownTimer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSHOOTING_T2912033273_H
#ifndef ENEMYSPAWNER_T2006493939_H
#define ENEMYSPAWNER_T2006493939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemySpawner
struct  EnemySpawner_t2006493939  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] EnemySpawner::enemyPrefab
	GameObjectU5BU5D_t3328599146* ___enemyPrefab_4;
	// System.Single EnemySpawner::spawnDistance
	float ___spawnDistance_5;
	// System.Single EnemySpawner::enemyRate
	float ___enemyRate_6;
	// System.Single EnemySpawner::nextEnemy
	float ___nextEnemy_7;
	// System.Single EnemySpawner::n
	float ___n_8;
	// System.Int32 EnemySpawner::i
	int32_t ___i_9;
	// System.Single EnemySpawner::timer
	float ___timer_10;
	// UnityEngine.Vector3 EnemySpawner::offset
	Vector3_t3722313464  ___offset_11;

public:
	inline static int32_t get_offset_of_enemyPrefab_4() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___enemyPrefab_4)); }
	inline GameObjectU5BU5D_t3328599146* get_enemyPrefab_4() const { return ___enemyPrefab_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_enemyPrefab_4() { return &___enemyPrefab_4; }
	inline void set_enemyPrefab_4(GameObjectU5BU5D_t3328599146* value)
	{
		___enemyPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___enemyPrefab_4), value);
	}

	inline static int32_t get_offset_of_spawnDistance_5() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___spawnDistance_5)); }
	inline float get_spawnDistance_5() const { return ___spawnDistance_5; }
	inline float* get_address_of_spawnDistance_5() { return &___spawnDistance_5; }
	inline void set_spawnDistance_5(float value)
	{
		___spawnDistance_5 = value;
	}

	inline static int32_t get_offset_of_enemyRate_6() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___enemyRate_6)); }
	inline float get_enemyRate_6() const { return ___enemyRate_6; }
	inline float* get_address_of_enemyRate_6() { return &___enemyRate_6; }
	inline void set_enemyRate_6(float value)
	{
		___enemyRate_6 = value;
	}

	inline static int32_t get_offset_of_nextEnemy_7() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___nextEnemy_7)); }
	inline float get_nextEnemy_7() const { return ___nextEnemy_7; }
	inline float* get_address_of_nextEnemy_7() { return &___nextEnemy_7; }
	inline void set_nextEnemy_7(float value)
	{
		___nextEnemy_7 = value;
	}

	inline static int32_t get_offset_of_n_8() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___n_8)); }
	inline float get_n_8() const { return ___n_8; }
	inline float* get_address_of_n_8() { return &___n_8; }
	inline void set_n_8(float value)
	{
		___n_8 = value;
	}

	inline static int32_t get_offset_of_i_9() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___i_9)); }
	inline int32_t get_i_9() const { return ___i_9; }
	inline int32_t* get_address_of_i_9() { return &___i_9; }
	inline void set_i_9(int32_t value)
	{
		___i_9 = value;
	}

	inline static int32_t get_offset_of_timer_10() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___timer_10)); }
	inline float get_timer_10() const { return ___timer_10; }
	inline float* get_address_of_timer_10() { return &___timer_10; }
	inline void set_timer_10(float value)
	{
		___timer_10 = value;
	}

	inline static int32_t get_offset_of_offset_11() { return static_cast<int32_t>(offsetof(EnemySpawner_t2006493939, ___offset_11)); }
	inline Vector3_t3722313464  get_offset_11() const { return ___offset_11; }
	inline Vector3_t3722313464 * get_address_of_offset_11() { return &___offset_11; }
	inline void set_offset_11(Vector3_t3722313464  value)
	{
		___offset_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSPAWNER_T2006493939_H
#ifndef FACESENEMY_T493038826_H
#define FACESENEMY_T493038826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacesEnemy
struct  FacesEnemy_t493038826  : public MonoBehaviour_t3962482529
{
public:
	// System.String FacesEnemy::ttag
	String_t* ___ttag_4;
	// UnityEngine.Transform FacesEnemy::target
	Transform_t3600365921 * ___target_5;
	// System.Single FacesEnemy::targetdistance
	float ___targetdistance_6;
	// System.Single FacesEnemy::dist
	float ___dist_7;
	// UnityEngine.GameObject FacesEnemy::Player
	GameObject_t1113636619 * ___Player_8;
	// System.Single FacesEnemy::rotSpeed
	float ___rotSpeed_9;
	// UnityEngine.Transform FacesEnemy::Enemy
	Transform_t3600365921 * ___Enemy_10;
	// UnityEngine.GameObject FacesEnemy::finder
	GameObject_t1113636619 * ___finder_11;

public:
	inline static int32_t get_offset_of_ttag_4() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___ttag_4)); }
	inline String_t* get_ttag_4() const { return ___ttag_4; }
	inline String_t** get_address_of_ttag_4() { return &___ttag_4; }
	inline void set_ttag_4(String_t* value)
	{
		___ttag_4 = value;
		Il2CppCodeGenWriteBarrier((&___ttag_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___target_5)); }
	inline Transform_t3600365921 * get_target_5() const { return ___target_5; }
	inline Transform_t3600365921 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Transform_t3600365921 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_targetdistance_6() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___targetdistance_6)); }
	inline float get_targetdistance_6() const { return ___targetdistance_6; }
	inline float* get_address_of_targetdistance_6() { return &___targetdistance_6; }
	inline void set_targetdistance_6(float value)
	{
		___targetdistance_6 = value;
	}

	inline static int32_t get_offset_of_dist_7() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___dist_7)); }
	inline float get_dist_7() const { return ___dist_7; }
	inline float* get_address_of_dist_7() { return &___dist_7; }
	inline void set_dist_7(float value)
	{
		___dist_7 = value;
	}

	inline static int32_t get_offset_of_Player_8() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___Player_8)); }
	inline GameObject_t1113636619 * get_Player_8() const { return ___Player_8; }
	inline GameObject_t1113636619 ** get_address_of_Player_8() { return &___Player_8; }
	inline void set_Player_8(GameObject_t1113636619 * value)
	{
		___Player_8 = value;
		Il2CppCodeGenWriteBarrier((&___Player_8), value);
	}

	inline static int32_t get_offset_of_rotSpeed_9() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___rotSpeed_9)); }
	inline float get_rotSpeed_9() const { return ___rotSpeed_9; }
	inline float* get_address_of_rotSpeed_9() { return &___rotSpeed_9; }
	inline void set_rotSpeed_9(float value)
	{
		___rotSpeed_9 = value;
	}

	inline static int32_t get_offset_of_Enemy_10() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___Enemy_10)); }
	inline Transform_t3600365921 * get_Enemy_10() const { return ___Enemy_10; }
	inline Transform_t3600365921 ** get_address_of_Enemy_10() { return &___Enemy_10; }
	inline void set_Enemy_10(Transform_t3600365921 * value)
	{
		___Enemy_10 = value;
		Il2CppCodeGenWriteBarrier((&___Enemy_10), value);
	}

	inline static int32_t get_offset_of_finder_11() { return static_cast<int32_t>(offsetof(FacesEnemy_t493038826, ___finder_11)); }
	inline GameObject_t1113636619 * get_finder_11() const { return ___finder_11; }
	inline GameObject_t1113636619 ** get_address_of_finder_11() { return &___finder_11; }
	inline void set_finder_11(GameObject_t1113636619 * value)
	{
		___finder_11 = value;
		Il2CppCodeGenWriteBarrier((&___finder_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACESENEMY_T493038826_H
#ifndef FOLLOWPLAYER_T2788059413_H
#define FOLLOWPLAYER_T2788059413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowPlayer
struct  FollowPlayer_t2788059413  : public MonoBehaviour_t3962482529
{
public:
	// System.Single FollowPlayer::rotSpeed
	float ___rotSpeed_4;
	// System.Single FollowPlayer::maxSpeed
	float ___maxSpeed_5;
	// UnityEngine.Transform FollowPlayer::player
	Transform_t3600365921 * ___player_6;
	// System.Single FollowPlayer::Distance
	float ___Distance_7;
	// System.Single FollowPlayer::disM
	float ___disM_8;
	// System.Single FollowPlayer::dism
	float ___dism_9;
	// System.Single FollowPlayer::zAngle
	float ___zAngle_10;
	// UnityEngine.Vector3 FollowPlayer::pos
	Vector3_t3722313464  ___pos_11;
	// UnityEngine.Vector3 FollowPlayer::velocity
	Vector3_t3722313464  ___velocity_12;
	// UnityEngine.GameObject FollowPlayer::go
	GameObject_t1113636619 * ___go_13;
	// UnityEngine.Vector2 FollowPlayer::dir
	Vector2_t2156229523  ___dir_14;
	// UnityEngine.Quaternion FollowPlayer::desiredRot
	Quaternion_t2301928331  ___desiredRot_15;

public:
	inline static int32_t get_offset_of_rotSpeed_4() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___rotSpeed_4)); }
	inline float get_rotSpeed_4() const { return ___rotSpeed_4; }
	inline float* get_address_of_rotSpeed_4() { return &___rotSpeed_4; }
	inline void set_rotSpeed_4(float value)
	{
		___rotSpeed_4 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_5() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___maxSpeed_5)); }
	inline float get_maxSpeed_5() const { return ___maxSpeed_5; }
	inline float* get_address_of_maxSpeed_5() { return &___maxSpeed_5; }
	inline void set_maxSpeed_5(float value)
	{
		___maxSpeed_5 = value;
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___player_6)); }
	inline Transform_t3600365921 * get_player_6() const { return ___player_6; }
	inline Transform_t3600365921 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(Transform_t3600365921 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_Distance_7() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___Distance_7)); }
	inline float get_Distance_7() const { return ___Distance_7; }
	inline float* get_address_of_Distance_7() { return &___Distance_7; }
	inline void set_Distance_7(float value)
	{
		___Distance_7 = value;
	}

	inline static int32_t get_offset_of_disM_8() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___disM_8)); }
	inline float get_disM_8() const { return ___disM_8; }
	inline float* get_address_of_disM_8() { return &___disM_8; }
	inline void set_disM_8(float value)
	{
		___disM_8 = value;
	}

	inline static int32_t get_offset_of_dism_9() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___dism_9)); }
	inline float get_dism_9() const { return ___dism_9; }
	inline float* get_address_of_dism_9() { return &___dism_9; }
	inline void set_dism_9(float value)
	{
		___dism_9 = value;
	}

	inline static int32_t get_offset_of_zAngle_10() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___zAngle_10)); }
	inline float get_zAngle_10() const { return ___zAngle_10; }
	inline float* get_address_of_zAngle_10() { return &___zAngle_10; }
	inline void set_zAngle_10(float value)
	{
		___zAngle_10 = value;
	}

	inline static int32_t get_offset_of_pos_11() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___pos_11)); }
	inline Vector3_t3722313464  get_pos_11() const { return ___pos_11; }
	inline Vector3_t3722313464 * get_address_of_pos_11() { return &___pos_11; }
	inline void set_pos_11(Vector3_t3722313464  value)
	{
		___pos_11 = value;
	}

	inline static int32_t get_offset_of_velocity_12() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___velocity_12)); }
	inline Vector3_t3722313464  get_velocity_12() const { return ___velocity_12; }
	inline Vector3_t3722313464 * get_address_of_velocity_12() { return &___velocity_12; }
	inline void set_velocity_12(Vector3_t3722313464  value)
	{
		___velocity_12 = value;
	}

	inline static int32_t get_offset_of_go_13() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___go_13)); }
	inline GameObject_t1113636619 * get_go_13() const { return ___go_13; }
	inline GameObject_t1113636619 ** get_address_of_go_13() { return &___go_13; }
	inline void set_go_13(GameObject_t1113636619 * value)
	{
		___go_13 = value;
		Il2CppCodeGenWriteBarrier((&___go_13), value);
	}

	inline static int32_t get_offset_of_dir_14() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___dir_14)); }
	inline Vector2_t2156229523  get_dir_14() const { return ___dir_14; }
	inline Vector2_t2156229523 * get_address_of_dir_14() { return &___dir_14; }
	inline void set_dir_14(Vector2_t2156229523  value)
	{
		___dir_14 = value;
	}

	inline static int32_t get_offset_of_desiredRot_15() { return static_cast<int32_t>(offsetof(FollowPlayer_t2788059413, ___desiredRot_15)); }
	inline Quaternion_t2301928331  get_desiredRot_15() const { return ___desiredRot_15; }
	inline Quaternion_t2301928331 * get_address_of_desiredRot_15() { return &___desiredRot_15; }
	inline void set_desiredRot_15(Quaternion_t2301928331  value)
	{
		___desiredRot_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWPLAYER_T2788059413_H
#ifndef INVENTORY_T1050226016_H
#define INVENTORY_T1050226016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory
struct  Inventory_t1050226016  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean[] Inventory::isFull
	BooleanU5BU5D_t2897418192* ___isFull_4;
	// UnityEngine.GameObject[] Inventory::slots
	GameObjectU5BU5D_t3328599146* ___slots_5;

public:
	inline static int32_t get_offset_of_isFull_4() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___isFull_4)); }
	inline BooleanU5BU5D_t2897418192* get_isFull_4() const { return ___isFull_4; }
	inline BooleanU5BU5D_t2897418192** get_address_of_isFull_4() { return &___isFull_4; }
	inline void set_isFull_4(BooleanU5BU5D_t2897418192* value)
	{
		___isFull_4 = value;
		Il2CppCodeGenWriteBarrier((&___isFull_4), value);
	}

	inline static int32_t get_offset_of_slots_5() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___slots_5)); }
	inline GameObjectU5BU5D_t3328599146* get_slots_5() const { return ___slots_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_slots_5() { return &___slots_5; }
	inline void set_slots_5(GameObjectU5BU5D_t3328599146* value)
	{
		___slots_5 = value;
		Il2CppCodeGenWriteBarrier((&___slots_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T1050226016_H
#ifndef JOYSTICK_T2364599988_H
#define JOYSTICK_T2364599988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoyStick
struct  JoyStick_t2364599988  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image JoyStick::bgImg
	Image_t2670269651 * ___bgImg_4;
	// UnityEngine.UI.Image JoyStick::joystickImg
	Image_t2670269651 * ___joystickImg_5;
	// UnityEngine.Vector3 JoyStick::inputVector
	Vector3_t3722313464  ___inputVector_6;

public:
	inline static int32_t get_offset_of_bgImg_4() { return static_cast<int32_t>(offsetof(JoyStick_t2364599988, ___bgImg_4)); }
	inline Image_t2670269651 * get_bgImg_4() const { return ___bgImg_4; }
	inline Image_t2670269651 ** get_address_of_bgImg_4() { return &___bgImg_4; }
	inline void set_bgImg_4(Image_t2670269651 * value)
	{
		___bgImg_4 = value;
		Il2CppCodeGenWriteBarrier((&___bgImg_4), value);
	}

	inline static int32_t get_offset_of_joystickImg_5() { return static_cast<int32_t>(offsetof(JoyStick_t2364599988, ___joystickImg_5)); }
	inline Image_t2670269651 * get_joystickImg_5() const { return ___joystickImg_5; }
	inline Image_t2670269651 ** get_address_of_joystickImg_5() { return &___joystickImg_5; }
	inline void set_joystickImg_5(Image_t2670269651 * value)
	{
		___joystickImg_5 = value;
		Il2CppCodeGenWriteBarrier((&___joystickImg_5), value);
	}

	inline static int32_t get_offset_of_inputVector_6() { return static_cast<int32_t>(offsetof(JoyStick_t2364599988, ___inputVector_6)); }
	inline Vector3_t3722313464  get_inputVector_6() const { return ___inputVector_6; }
	inline Vector3_t3722313464 * get_address_of_inputVector_6() { return &___inputVector_6; }
	inline void set_inputVector_6(Vector3_t3722313464  value)
	{
		___inputVector_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T2364599988_H
#ifndef JOYSTICK_T9498292_H
#define JOYSTICK_T9498292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t9498292  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Joystick::handleLimit
	float ___handleLimit_4;
	// JoystickMode Joystick::joystickMode
	int32_t ___joystickMode_5;
	// UnityEngine.Vector2 Joystick::inputVector
	Vector2_t2156229523  ___inputVector_6;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t3704657025 * ___background_7;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t3704657025 * ___handle_8;

public:
	inline static int32_t get_offset_of_handleLimit_4() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___handleLimit_4)); }
	inline float get_handleLimit_4() const { return ___handleLimit_4; }
	inline float* get_address_of_handleLimit_4() { return &___handleLimit_4; }
	inline void set_handleLimit_4(float value)
	{
		___handleLimit_4 = value;
	}

	inline static int32_t get_offset_of_joystickMode_5() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___joystickMode_5)); }
	inline int32_t get_joystickMode_5() const { return ___joystickMode_5; }
	inline int32_t* get_address_of_joystickMode_5() { return &___joystickMode_5; }
	inline void set_joystickMode_5(int32_t value)
	{
		___joystickMode_5 = value;
	}

	inline static int32_t get_offset_of_inputVector_6() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___inputVector_6)); }
	inline Vector2_t2156229523  get_inputVector_6() const { return ___inputVector_6; }
	inline Vector2_t2156229523 * get_address_of_inputVector_6() { return &___inputVector_6; }
	inline void set_inputVector_6(Vector2_t2156229523  value)
	{
		___inputVector_6 = value;
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___background_7)); }
	inline RectTransform_t3704657025 * get_background_7() const { return ___background_7; }
	inline RectTransform_t3704657025 ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(RectTransform_t3704657025 * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((&___background_7), value);
	}

	inline static int32_t get_offset_of_handle_8() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___handle_8)); }
	inline RectTransform_t3704657025 * get_handle_8() const { return ___handle_8; }
	inline RectTransform_t3704657025 ** get_address_of_handle_8() { return &___handle_8; }
	inline void set_handle_8(RectTransform_t3704657025 * value)
	{
		___handle_8 = value;
		Il2CppCodeGenWriteBarrier((&___handle_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T9498292_H
#ifndef MAINMENU_T3798339593_H
#define MAINMENU_T3798339593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t3798339593  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T3798339593_H
#ifndef MOVEFORWARD_T2564727450_H
#define MOVEFORWARD_T2564727450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveForward
struct  MoveForward_t2564727450  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MoveForward::maxSpeed
	float ___maxSpeed_4;
	// UnityEngine.Vector3 MoveForward::pos
	Vector3_t3722313464  ___pos_5;
	// UnityEngine.Vector3 MoveForward::velocity
	Vector3_t3722313464  ___velocity_6;

public:
	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(MoveForward_t2564727450, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_pos_5() { return static_cast<int32_t>(offsetof(MoveForward_t2564727450, ___pos_5)); }
	inline Vector3_t3722313464  get_pos_5() const { return ___pos_5; }
	inline Vector3_t3722313464 * get_address_of_pos_5() { return &___pos_5; }
	inline void set_pos_5(Vector3_t3722313464  value)
	{
		___pos_5 = value;
	}

	inline static int32_t get_offset_of_velocity_6() { return static_cast<int32_t>(offsetof(MoveForward_t2564727450, ___velocity_6)); }
	inline Vector3_t3722313464  get_velocity_6() const { return ___velocity_6; }
	inline Vector3_t3722313464 * get_address_of_velocity_6() { return &___velocity_6; }
	inline void set_velocity_6(Vector3_t3722313464  value)
	{
		___velocity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEFORWARD_T2564727450_H
#ifndef PAUSEMENU_T3916167947_H
#define PAUSEMENU_T3916167947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_t3916167947  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PauseMenu::pauseMenuUI
	GameObject_t1113636619 * ___pauseMenuUI_5;
	// UnityEngine.GameObject PauseMenu::playScreen
	GameObject_t1113636619 * ___playScreen_6;
	// UnityEngine.GameObject PauseMenu::pauseButton
	GameObject_t1113636619 * ___pauseButton_7;
	// UnityEngine.GameObject PauseMenu::ButtonSound
	GameObject_t1113636619 * ___ButtonSound_8;

public:
	inline static int32_t get_offset_of_pauseMenuUI_5() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___pauseMenuUI_5)); }
	inline GameObject_t1113636619 * get_pauseMenuUI_5() const { return ___pauseMenuUI_5; }
	inline GameObject_t1113636619 ** get_address_of_pauseMenuUI_5() { return &___pauseMenuUI_5; }
	inline void set_pauseMenuUI_5(GameObject_t1113636619 * value)
	{
		___pauseMenuUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___pauseMenuUI_5), value);
	}

	inline static int32_t get_offset_of_playScreen_6() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___playScreen_6)); }
	inline GameObject_t1113636619 * get_playScreen_6() const { return ___playScreen_6; }
	inline GameObject_t1113636619 ** get_address_of_playScreen_6() { return &___playScreen_6; }
	inline void set_playScreen_6(GameObject_t1113636619 * value)
	{
		___playScreen_6 = value;
		Il2CppCodeGenWriteBarrier((&___playScreen_6), value);
	}

	inline static int32_t get_offset_of_pauseButton_7() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___pauseButton_7)); }
	inline GameObject_t1113636619 * get_pauseButton_7() const { return ___pauseButton_7; }
	inline GameObject_t1113636619 ** get_address_of_pauseButton_7() { return &___pauseButton_7; }
	inline void set_pauseButton_7(GameObject_t1113636619 * value)
	{
		___pauseButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___pauseButton_7), value);
	}

	inline static int32_t get_offset_of_ButtonSound_8() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___ButtonSound_8)); }
	inline GameObject_t1113636619 * get_ButtonSound_8() const { return ___ButtonSound_8; }
	inline GameObject_t1113636619 ** get_address_of_ButtonSound_8() { return &___ButtonSound_8; }
	inline void set_ButtonSound_8(GameObject_t1113636619 * value)
	{
		___ButtonSound_8 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonSound_8), value);
	}
};

struct PauseMenu_t3916167947_StaticFields
{
public:
	// System.Boolean PauseMenu::GameIsPaused
	bool ___GameIsPaused_4;

public:
	inline static int32_t get_offset_of_GameIsPaused_4() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947_StaticFields, ___GameIsPaused_4)); }
	inline bool get_GameIsPaused_4() const { return ___GameIsPaused_4; }
	inline bool* get_address_of_GameIsPaused_4() { return &___GameIsPaused_4; }
	inline void set_GameIsPaused_4(bool value)
	{
		___GameIsPaused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T3916167947_H
#ifndef PICKUP_T999226966_H
#define PICKUP_T999226966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pickup
struct  Pickup_t999226966  : public MonoBehaviour_t3962482529
{
public:
	// Inventory Pickup::inventory
	Inventory_t1050226016 * ___inventory_4;
	// UnityEngine.GameObject Pickup::itemButton
	GameObject_t1113636619 * ___itemButton_5;
	// UnityEngine.GameObject Pickup::pickup
	GameObject_t1113636619 * ___pickup_6;

public:
	inline static int32_t get_offset_of_inventory_4() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___inventory_4)); }
	inline Inventory_t1050226016 * get_inventory_4() const { return ___inventory_4; }
	inline Inventory_t1050226016 ** get_address_of_inventory_4() { return &___inventory_4; }
	inline void set_inventory_4(Inventory_t1050226016 * value)
	{
		___inventory_4 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_4), value);
	}

	inline static int32_t get_offset_of_itemButton_5() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___itemButton_5)); }
	inline GameObject_t1113636619 * get_itemButton_5() const { return ___itemButton_5; }
	inline GameObject_t1113636619 ** get_address_of_itemButton_5() { return &___itemButton_5; }
	inline void set_itemButton_5(GameObject_t1113636619 * value)
	{
		___itemButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___itemButton_5), value);
	}

	inline static int32_t get_offset_of_pickup_6() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___pickup_6)); }
	inline GameObject_t1113636619 * get_pickup_6() const { return ___pickup_6; }
	inline GameObject_t1113636619 ** get_address_of_pickup_6() { return &___pickup_6; }
	inline void set_pickup_6(GameObject_t1113636619 * value)
	{
		___pickup_6 = value;
		Il2CppCodeGenWriteBarrier((&___pickup_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUP_T999226966_H
#ifndef PLAYERCONTROLLER_T2064355688_H
#define PLAYERCONTROLLER_T2064355688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t2064355688  : public MonoBehaviour_t3962482529
{
public:
	// FloatingJoystick PlayerController::joystick
	FloatingJoystick_t3402721920 * ___joystick_4;

public:
	inline static int32_t get_offset_of_joystick_4() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___joystick_4)); }
	inline FloatingJoystick_t3402721920 * get_joystick_4() const { return ___joystick_4; }
	inline FloatingJoystick_t3402721920 ** get_address_of_joystick_4() { return &___joystick_4; }
	inline void set_joystick_4(FloatingJoystick_t3402721920 * value)
	{
		___joystick_4 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2064355688_H
#ifndef PLAYERMOVEMENT_T2731566919_H
#define PLAYERMOVEMENT_T2731566919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovement
struct  PlayerMovement_t2731566919  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PlayerMovement::MoveSpeed
	float ___MoveSpeed_4;
	// System.Single PlayerMovement::h
	float ___h_5;
	// System.Single PlayerMovement::v
	float ___v_6;

public:
	inline static int32_t get_offset_of_MoveSpeed_4() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___MoveSpeed_4)); }
	inline float get_MoveSpeed_4() const { return ___MoveSpeed_4; }
	inline float* get_address_of_MoveSpeed_4() { return &___MoveSpeed_4; }
	inline void set_MoveSpeed_4(float value)
	{
		___MoveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_h_5() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___h_5)); }
	inline float get_h_5() const { return ___h_5; }
	inline float* get_address_of_h_5() { return &___h_5; }
	inline void set_h_5(float value)
	{
		___h_5 = value;
	}

	inline static int32_t get_offset_of_v_6() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___v_6)); }
	inline float get_v_6() const { return ___v_6; }
	inline float* get_address_of_v_6() { return &___v_6; }
	inline void set_v_6(float value)
	{
		___v_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEMENT_T2731566919_H
#ifndef PLAYERSHOOTING_T3504758017_H
#define PLAYERSHOOTING_T3504758017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerShooting
struct  PlayerShooting_t3504758017  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerShooting::shootSound
	GameObject_t1113636619 * ___shootSound_4;
	// UnityEngine.GameObject PlayerShooting::bulletPrefab
	GameObject_t1113636619 * ___bulletPrefab_5;
	// System.String PlayerShooting::ttag
	String_t* ___ttag_6;
	// UnityEngine.Transform PlayerShooting::target
	Transform_t3600365921 * ___target_7;
	// System.Single PlayerShooting::targetdistance
	float ___targetdistance_8;
	// System.Single PlayerShooting::dist
	float ___dist_9;
	// System.Single PlayerShooting::fireDelay
	float ___fireDelay_10;
	// System.Single PlayerShooting::Distance
	float ___Distance_11;
	// UnityEngine.Transform PlayerShooting::Enemy
	Transform_t3600365921 * ___Enemy_12;
	// System.Single PlayerShooting::cooldownTimer
	float ___cooldownTimer_13;

public:
	inline static int32_t get_offset_of_shootSound_4() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___shootSound_4)); }
	inline GameObject_t1113636619 * get_shootSound_4() const { return ___shootSound_4; }
	inline GameObject_t1113636619 ** get_address_of_shootSound_4() { return &___shootSound_4; }
	inline void set_shootSound_4(GameObject_t1113636619 * value)
	{
		___shootSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___shootSound_4), value);
	}

	inline static int32_t get_offset_of_bulletPrefab_5() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___bulletPrefab_5)); }
	inline GameObject_t1113636619 * get_bulletPrefab_5() const { return ___bulletPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_bulletPrefab_5() { return &___bulletPrefab_5; }
	inline void set_bulletPrefab_5(GameObject_t1113636619 * value)
	{
		___bulletPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_5), value);
	}

	inline static int32_t get_offset_of_ttag_6() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___ttag_6)); }
	inline String_t* get_ttag_6() const { return ___ttag_6; }
	inline String_t** get_address_of_ttag_6() { return &___ttag_6; }
	inline void set_ttag_6(String_t* value)
	{
		___ttag_6 = value;
		Il2CppCodeGenWriteBarrier((&___ttag_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___target_7)); }
	inline Transform_t3600365921 * get_target_7() const { return ___target_7; }
	inline Transform_t3600365921 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Transform_t3600365921 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_targetdistance_8() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___targetdistance_8)); }
	inline float get_targetdistance_8() const { return ___targetdistance_8; }
	inline float* get_address_of_targetdistance_8() { return &___targetdistance_8; }
	inline void set_targetdistance_8(float value)
	{
		___targetdistance_8 = value;
	}

	inline static int32_t get_offset_of_dist_9() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___dist_9)); }
	inline float get_dist_9() const { return ___dist_9; }
	inline float* get_address_of_dist_9() { return &___dist_9; }
	inline void set_dist_9(float value)
	{
		___dist_9 = value;
	}

	inline static int32_t get_offset_of_fireDelay_10() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___fireDelay_10)); }
	inline float get_fireDelay_10() const { return ___fireDelay_10; }
	inline float* get_address_of_fireDelay_10() { return &___fireDelay_10; }
	inline void set_fireDelay_10(float value)
	{
		___fireDelay_10 = value;
	}

	inline static int32_t get_offset_of_Distance_11() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___Distance_11)); }
	inline float get_Distance_11() const { return ___Distance_11; }
	inline float* get_address_of_Distance_11() { return &___Distance_11; }
	inline void set_Distance_11(float value)
	{
		___Distance_11 = value;
	}

	inline static int32_t get_offset_of_Enemy_12() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___Enemy_12)); }
	inline Transform_t3600365921 * get_Enemy_12() const { return ___Enemy_12; }
	inline Transform_t3600365921 ** get_address_of_Enemy_12() { return &___Enemy_12; }
	inline void set_Enemy_12(Transform_t3600365921 * value)
	{
		___Enemy_12 = value;
		Il2CppCodeGenWriteBarrier((&___Enemy_12), value);
	}

	inline static int32_t get_offset_of_cooldownTimer_13() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___cooldownTimer_13)); }
	inline float get_cooldownTimer_13() const { return ___cooldownTimer_13; }
	inline float* get_address_of_cooldownTimer_13() { return &___cooldownTimer_13; }
	inline void set_cooldownTimer_13(float value)
	{
		___cooldownTimer_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSHOOTING_T3504758017_H
#ifndef SECONDSLOT_T2904157877_H
#define SECONDSLOT_T2904157877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondSlot
struct  SecondSlot_t2904157877  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECONDSLOT_T2904157877_H
#ifndef SELFDESTRUCT_T1181332799_H
#define SELFDESTRUCT_T1181332799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelfDestruct
struct  SelfDestruct_t1181332799  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SelfDestruct::timer
	float ___timer_4;

public:
	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(SelfDestruct_t1181332799, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFDESTRUCT_T1181332799_H
#ifndef SLOT_T3333057830_H
#define SLOT_T3333057830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slot
struct  Slot_t3333057830  : public MonoBehaviour_t3962482529
{
public:
	// Inventory Slot::inventory
	Inventory_t1050226016 * ___inventory_4;
	// System.Int32 Slot::i
	int32_t ___i_5;
	// UnityEngine.GameObject Slot::deleteSound
	GameObject_t1113636619 * ___deleteSound_6;

public:
	inline static int32_t get_offset_of_inventory_4() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___inventory_4)); }
	inline Inventory_t1050226016 * get_inventory_4() const { return ___inventory_4; }
	inline Inventory_t1050226016 ** get_address_of_inventory_4() { return &___inventory_4; }
	inline void set_inventory_4(Inventory_t1050226016 * value)
	{
		___inventory_4 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_4), value);
	}

	inline static int32_t get_offset_of_i_5() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___i_5)); }
	inline int32_t get_i_5() const { return ___i_5; }
	inline int32_t* get_address_of_i_5() { return &___i_5; }
	inline void set_i_5(int32_t value)
	{
		___i_5 = value;
	}

	inline static int32_t get_offset_of_deleteSound_6() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___deleteSound_6)); }
	inline GameObject_t1113636619 * get_deleteSound_6() const { return ___deleteSound_6; }
	inline GameObject_t1113636619 ** get_address_of_deleteSound_6() { return &___deleteSound_6; }
	inline void set_deleteSound_6(GameObject_t1113636619 * value)
	{
		___deleteSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___deleteSound_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3333057830_H
#ifndef SPAWN_T617419884_H
#define SPAWN_T617419884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawn
struct  Spawn_t617419884  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Spawn::item
	GameObject_t1113636619 * ___item_4;
	// UnityEngine.GameObject Spawn::ThreeDirection
	GameObject_t1113636619 * ___ThreeDirection_5;

public:
	inline static int32_t get_offset_of_item_4() { return static_cast<int32_t>(offsetof(Spawn_t617419884, ___item_4)); }
	inline GameObject_t1113636619 * get_item_4() const { return ___item_4; }
	inline GameObject_t1113636619 ** get_address_of_item_4() { return &___item_4; }
	inline void set_item_4(GameObject_t1113636619 * value)
	{
		___item_4 = value;
		Il2CppCodeGenWriteBarrier((&___item_4), value);
	}

	inline static int32_t get_offset_of_ThreeDirection_5() { return static_cast<int32_t>(offsetof(Spawn_t617419884, ___ThreeDirection_5)); }
	inline GameObject_t1113636619 * get_ThreeDirection_5() const { return ___ThreeDirection_5; }
	inline GameObject_t1113636619 ** get_address_of_ThreeDirection_5() { return &___ThreeDirection_5; }
	inline void set_ThreeDirection_5(GameObject_t1113636619 * value)
	{
		___ThreeDirection_5 = value;
		Il2CppCodeGenWriteBarrier((&___ThreeDirection_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWN_T617419884_H
#ifndef TOUCHEVENT_T1418210080_H
#define TOUCHEVENT_T1418210080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchEvent
struct  TouchEvent_t1418210080  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 TouchEvent::WorldPoint
	Vector2_t2156229523  ___WorldPoint_4;
	// UnityEngine.Camera TouchEvent::cam
	Camera_t4157153871 * ___cam_5;
	// UnityEngine.GameObject TouchEvent::SpawnItem
	GameObject_t1113636619 * ___SpawnItem_6;
	// UnityEngine.GameObject TouchEvent::UnequipSound
	GameObject_t1113636619 * ___UnequipSound_7;

public:
	inline static int32_t get_offset_of_WorldPoint_4() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___WorldPoint_4)); }
	inline Vector2_t2156229523  get_WorldPoint_4() const { return ___WorldPoint_4; }
	inline Vector2_t2156229523 * get_address_of_WorldPoint_4() { return &___WorldPoint_4; }
	inline void set_WorldPoint_4(Vector2_t2156229523  value)
	{
		___WorldPoint_4 = value;
	}

	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___cam_5)); }
	inline Camera_t4157153871 * get_cam_5() const { return ___cam_5; }
	inline Camera_t4157153871 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t4157153871 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___cam_5), value);
	}

	inline static int32_t get_offset_of_SpawnItem_6() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___SpawnItem_6)); }
	inline GameObject_t1113636619 * get_SpawnItem_6() const { return ___SpawnItem_6; }
	inline GameObject_t1113636619 ** get_address_of_SpawnItem_6() { return &___SpawnItem_6; }
	inline void set_SpawnItem_6(GameObject_t1113636619 * value)
	{
		___SpawnItem_6 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnItem_6), value);
	}

	inline static int32_t get_offset_of_UnequipSound_7() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___UnequipSound_7)); }
	inline GameObject_t1113636619 * get_UnequipSound_7() const { return ___UnequipSound_7; }
	inline GameObject_t1113636619 ** get_address_of_UnequipSound_7() { return &___UnequipSound_7; }
	inline void set_UnequipSound_7(GameObject_t1113636619 * value)
	{
		___UnequipSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___UnequipSound_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENT_T1418210080_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_4;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_5;

public:
	inline static int32_t get_offset_of_spatializerExtension_4() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_4)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_4() const { return ___spatializerExtension_4; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_4() { return &___spatializerExtension_4; }
	inline void set_spatializerExtension_4(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_4), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_5() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_5)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_5() const { return ___ambisonicExtension_5; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_5() { return &___ambisonicExtension_5; }
	inline void set_ambisonicExtension_5(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef FIXEDJOYSTICK_T2618381211_H
#define FIXEDJOYSTICK_T2618381211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixedJoystick
struct  FixedJoystick_t2618381211  : public Joystick_t9498292
{
public:
	// UnityEngine.Vector2 FixedJoystick::joystickPosition
	Vector2_t2156229523  ___joystickPosition_9;
	// UnityEngine.Camera FixedJoystick::cam
	Camera_t4157153871 * ___cam_10;

public:
	inline static int32_t get_offset_of_joystickPosition_9() { return static_cast<int32_t>(offsetof(FixedJoystick_t2618381211, ___joystickPosition_9)); }
	inline Vector2_t2156229523  get_joystickPosition_9() const { return ___joystickPosition_9; }
	inline Vector2_t2156229523 * get_address_of_joystickPosition_9() { return &___joystickPosition_9; }
	inline void set_joystickPosition_9(Vector2_t2156229523  value)
	{
		___joystickPosition_9 = value;
	}

	inline static int32_t get_offset_of_cam_10() { return static_cast<int32_t>(offsetof(FixedJoystick_t2618381211, ___cam_10)); }
	inline Camera_t4157153871 * get_cam_10() const { return ___cam_10; }
	inline Camera_t4157153871 ** get_address_of_cam_10() { return &___cam_10; }
	inline void set_cam_10(Camera_t4157153871 * value)
	{
		___cam_10 = value;
		Il2CppCodeGenWriteBarrier((&___cam_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDJOYSTICK_T2618381211_H
#ifndef FLOATINGJOYSTICK_T3402721920_H
#define FLOATINGJOYSTICK_T3402721920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatingJoystick
struct  FloatingJoystick_t3402721920  : public Joystick_t9498292
{
public:
	// UnityEngine.Vector2 FloatingJoystick::joystickCenter
	Vector2_t2156229523  ___joystickCenter_9;

public:
	inline static int32_t get_offset_of_joystickCenter_9() { return static_cast<int32_t>(offsetof(FloatingJoystick_t3402721920, ___joystickCenter_9)); }
	inline Vector2_t2156229523  get_joystickCenter_9() const { return ___joystickCenter_9; }
	inline Vector2_t2156229523 * get_address_of_joystickCenter_9() { return &___joystickCenter_9; }
	inline void set_joystickCenter_9(Vector2_t2156229523  value)
	{
		___joystickCenter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINGJOYSTICK_T3402721920_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef VARIABLEJOYSTICK_T2643911586_H
#define VARIABLEJOYSTICK_T2643911586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VariableJoystick
struct  VariableJoystick_t2643911586  : public Joystick_t9498292
{
public:
	// System.Boolean VariableJoystick::isFixed
	bool ___isFixed_9;
	// UnityEngine.Vector2 VariableJoystick::fixedScreenPosition
	Vector2_t2156229523  ___fixedScreenPosition_10;
	// UnityEngine.Vector2 VariableJoystick::joystickCenter
	Vector2_t2156229523  ___joystickCenter_11;

public:
	inline static int32_t get_offset_of_isFixed_9() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___isFixed_9)); }
	inline bool get_isFixed_9() const { return ___isFixed_9; }
	inline bool* get_address_of_isFixed_9() { return &___isFixed_9; }
	inline void set_isFixed_9(bool value)
	{
		___isFixed_9 = value;
	}

	inline static int32_t get_offset_of_fixedScreenPosition_10() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___fixedScreenPosition_10)); }
	inline Vector2_t2156229523  get_fixedScreenPosition_10() const { return ___fixedScreenPosition_10; }
	inline Vector2_t2156229523 * get_address_of_fixedScreenPosition_10() { return &___fixedScreenPosition_10; }
	inline void set_fixedScreenPosition_10(Vector2_t2156229523  value)
	{
		___fixedScreenPosition_10 = value;
	}

	inline static int32_t get_offset_of_joystickCenter_11() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___joystickCenter_11)); }
	inline Vector2_t2156229523  get_joystickCenter_11() const { return ___joystickCenter_11; }
	inline Vector2_t2156229523 * get_address_of_joystickCenter_11() { return &___joystickCenter_11; }
	inline void set_joystickCenter_11(Vector2_t2156229523  value)
	{
		___joystickCenter_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEJOYSTICK_T2643911586_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef SLIDER_T3903728902_H
#define SLIDER_T3903728902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t3903728902  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t3704657025 * ___m_FillRect_18;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t3704657025 * ___m_HandleRect_19;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_20;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_21;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_22;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_23;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_24;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t3180273144 * ___m_OnValueChanged_25;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t2670269651 * ___m_FillImage_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_t3600365921 * ___m_FillTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t3704657025 * ___m_FillContainerRect_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_t3600365921 * ___m_HandleTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t3704657025 * ___m_HandleContainerRect_30;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_t2156229523  ___m_Offset_31;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_32;

public:
	inline static int32_t get_offset_of_m_FillRect_18() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillRect_18)); }
	inline RectTransform_t3704657025 * get_m_FillRect_18() const { return ___m_FillRect_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillRect_18() { return &___m_FillRect_18; }
	inline void set_m_FillRect_18(RectTransform_t3704657025 * value)
	{
		___m_FillRect_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_18), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_19() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleRect_19)); }
	inline RectTransform_t3704657025 * get_m_HandleRect_19() const { return ___m_HandleRect_19; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleRect_19() { return &___m_HandleRect_19; }
	inline void set_m_HandleRect_19(RectTransform_t3704657025 * value)
	{
		___m_HandleRect_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_19), value);
	}

	inline static int32_t get_offset_of_m_Direction_20() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Direction_20)); }
	inline int32_t get_m_Direction_20() const { return ___m_Direction_20; }
	inline int32_t* get_address_of_m_Direction_20() { return &___m_Direction_20; }
	inline void set_m_Direction_20(int32_t value)
	{
		___m_Direction_20 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_21() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MinValue_21)); }
	inline float get_m_MinValue_21() const { return ___m_MinValue_21; }
	inline float* get_address_of_m_MinValue_21() { return &___m_MinValue_21; }
	inline void set_m_MinValue_21(float value)
	{
		___m_MinValue_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_22() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_MaxValue_22)); }
	inline float get_m_MaxValue_22() const { return ___m_MaxValue_22; }
	inline float* get_address_of_m_MaxValue_22() { return &___m_MaxValue_22; }
	inline void set_m_MaxValue_22(float value)
	{
		___m_MaxValue_22 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_23() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_WholeNumbers_23)); }
	inline bool get_m_WholeNumbers_23() const { return ___m_WholeNumbers_23; }
	inline bool* get_address_of_m_WholeNumbers_23() { return &___m_WholeNumbers_23; }
	inline void set_m_WholeNumbers_23(bool value)
	{
		___m_WholeNumbers_23 = value;
	}

	inline static int32_t get_offset_of_m_Value_24() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Value_24)); }
	inline float get_m_Value_24() const { return ___m_Value_24; }
	inline float* get_address_of_m_Value_24() { return &___m_Value_24; }
	inline void set_m_Value_24(float value)
	{
		___m_Value_24 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_OnValueChanged_25)); }
	inline SliderEvent_t3180273144 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline SliderEvent_t3180273144 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(SliderEvent_t3180273144 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_FillImage_26() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillImage_26)); }
	inline Image_t2670269651 * get_m_FillImage_26() const { return ___m_FillImage_26; }
	inline Image_t2670269651 ** get_address_of_m_FillImage_26() { return &___m_FillImage_26; }
	inline void set_m_FillImage_26(Image_t2670269651 * value)
	{
		___m_FillImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_26), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_27() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillTransform_27)); }
	inline Transform_t3600365921 * get_m_FillTransform_27() const { return ___m_FillTransform_27; }
	inline Transform_t3600365921 ** get_address_of_m_FillTransform_27() { return &___m_FillTransform_27; }
	inline void set_m_FillTransform_27(Transform_t3600365921 * value)
	{
		___m_FillTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_27), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_FillContainerRect_28)); }
	inline RectTransform_t3704657025 * get_m_FillContainerRect_28() const { return ___m_FillContainerRect_28; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillContainerRect_28() { return &___m_FillContainerRect_28; }
	inline void set_m_FillContainerRect_28(RectTransform_t3704657025 * value)
	{
		___m_FillContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_29() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleTransform_29)); }
	inline Transform_t3600365921 * get_m_HandleTransform_29() const { return ___m_HandleTransform_29; }
	inline Transform_t3600365921 ** get_address_of_m_HandleTransform_29() { return &___m_HandleTransform_29; }
	inline void set_m_HandleTransform_29(Transform_t3600365921 * value)
	{
		___m_HandleTransform_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_29), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_HandleContainerRect_30)); }
	inline RectTransform_t3704657025 * get_m_HandleContainerRect_30() const { return ___m_HandleContainerRect_30; }
	inline RectTransform_t3704657025 ** get_address_of_m_HandleContainerRect_30() { return &___m_HandleContainerRect_30; }
	inline void set_m_HandleContainerRect_30(RectTransform_t3704657025 * value)
	{
		___m_HandleContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_30), value);
	}

	inline static int32_t get_offset_of_m_Offset_31() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Offset_31)); }
	inline Vector2_t2156229523  get_m_Offset_31() const { return ___m_Offset_31; }
	inline Vector2_t2156229523 * get_address_of_m_Offset_31() { return &___m_Offset_31; }
	inline void set_m_Offset_31(Vector2_t2156229523  value)
	{
		___m_Offset_31 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_32() { return static_cast<int32_t>(offsetof(Slider_t3903728902, ___m_Tracker_32)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_32() const { return ___m_Tracker_32; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_32() { return &___m_Tracker_32; }
	inline void set_m_Tracker_32(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T3903728902_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_31;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_32;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_33;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_34;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_35;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_36;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_37;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_38;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_39;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_40;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_41;

public:
	inline static int32_t get_offset_of_m_Sprite_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_31)); }
	inline Sprite_t280657092 * get_m_Sprite_31() const { return ___m_Sprite_31; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_31() { return &___m_Sprite_31; }
	inline void set_m_Sprite_31(Sprite_t280657092 * value)
	{
		___m_Sprite_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_31), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_32)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_32() const { return ___m_OverrideSprite_32; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_32() { return &___m_OverrideSprite_32; }
	inline void set_m_OverrideSprite_32(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_32), value);
	}

	inline static int32_t get_offset_of_m_Type_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_33)); }
	inline int32_t get_m_Type_33() const { return ___m_Type_33; }
	inline int32_t* get_address_of_m_Type_33() { return &___m_Type_33; }
	inline void set_m_Type_33(int32_t value)
	{
		___m_Type_33 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_34)); }
	inline bool get_m_PreserveAspect_34() const { return ___m_PreserveAspect_34; }
	inline bool* get_address_of_m_PreserveAspect_34() { return &___m_PreserveAspect_34; }
	inline void set_m_PreserveAspect_34(bool value)
	{
		___m_PreserveAspect_34 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_35)); }
	inline bool get_m_FillCenter_35() const { return ___m_FillCenter_35; }
	inline bool* get_address_of_m_FillCenter_35() { return &___m_FillCenter_35; }
	inline void set_m_FillCenter_35(bool value)
	{
		___m_FillCenter_35 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_36)); }
	inline int32_t get_m_FillMethod_36() const { return ___m_FillMethod_36; }
	inline int32_t* get_address_of_m_FillMethod_36() { return &___m_FillMethod_36; }
	inline void set_m_FillMethod_36(int32_t value)
	{
		___m_FillMethod_36 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_37)); }
	inline float get_m_FillAmount_37() const { return ___m_FillAmount_37; }
	inline float* get_address_of_m_FillAmount_37() { return &___m_FillAmount_37; }
	inline void set_m_FillAmount_37(float value)
	{
		___m_FillAmount_37 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_38)); }
	inline bool get_m_FillClockwise_38() const { return ___m_FillClockwise_38; }
	inline bool* get_address_of_m_FillClockwise_38() { return &___m_FillClockwise_38; }
	inline void set_m_FillClockwise_38(bool value)
	{
		___m_FillClockwise_38 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_39() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_39)); }
	inline int32_t get_m_FillOrigin_39() const { return ___m_FillOrigin_39; }
	inline int32_t* get_address_of_m_FillOrigin_39() { return &___m_FillOrigin_39; }
	inline void set_m_FillOrigin_39(int32_t value)
	{
		___m_FillOrigin_39 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_40() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_40)); }
	inline float get_m_AlphaHitTestMinimumThreshold_40() const { return ___m_AlphaHitTestMinimumThreshold_40; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_40() { return &___m_AlphaHitTestMinimumThreshold_40; }
	inline void set_m_AlphaHitTestMinimumThreshold_40(float value)
	{
		___m_AlphaHitTestMinimumThreshold_40 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_41() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Tracked_41)); }
	inline bool get_m_Tracked_41() const { return ___m_Tracked_41; }
	inline bool* get_address_of_m_Tracked_41() { return &___m_Tracked_41; }
	inline void set_m_Tracked_41(bool value)
	{
		___m_Tracked_41 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_30;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_42;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_43;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_44;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_45;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t4142344393 * ___m_TrackedTexturelessImages_46;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_47;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.UI.Image::<>f__mg$cache0
	Action_1_t819399007 * ___U3CU3Ef__mgU24cache0_48;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_30() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_30)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_30() const { return ___s_ETC1DefaultUI_30; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_30() { return &___s_ETC1DefaultUI_30; }
	inline void set_s_ETC1DefaultUI_30(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_30), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_42)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_42() const { return ___s_VertScratch_42; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_42() { return &___s_VertScratch_42; }
	inline void set_s_VertScratch_42(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_42), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_43() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_43)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_43() const { return ___s_UVScratch_43; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_43() { return &___s_UVScratch_43; }
	inline void set_s_UVScratch_43(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_43 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_43), value);
	}

	inline static int32_t get_offset_of_s_Xy_44() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_44)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_44() const { return ___s_Xy_44; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_44() { return &___s_Xy_44; }
	inline void set_s_Xy_44(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_44 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_44), value);
	}

	inline static int32_t get_offset_of_s_Uv_45() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_45)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_45() const { return ___s_Uv_45; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_45() { return &___s_Uv_45; }
	inline void set_s_Uv_45(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_45 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_45), value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_46() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___m_TrackedTexturelessImages_46)); }
	inline List_1_t4142344393 * get_m_TrackedTexturelessImages_46() const { return ___m_TrackedTexturelessImages_46; }
	inline List_1_t4142344393 ** get_address_of_m_TrackedTexturelessImages_46() { return &___m_TrackedTexturelessImages_46; }
	inline void set_m_TrackedTexturelessImages_46(List_1_t4142344393 * value)
	{
		___m_TrackedTexturelessImages_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackedTexturelessImages_46), value);
	}

	inline static int32_t get_offset_of_s_Initialized_47() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Initialized_47)); }
	inline bool get_s_Initialized_47() const { return ___s_Initialized_47; }
	inline bool* get_address_of_s_Initialized_47() { return &___s_Initialized_47; }
	inline void set_s_Initialized_47(bool value)
	{
		___s_Initialized_47 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_48() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___U3CU3Ef__mgU24cache0_48)); }
	inline Action_1_t819399007 * get_U3CU3Ef__mgU24cache0_48() const { return ___U3CU3Ef__mgU24cache0_48; }
	inline Action_1_t819399007 ** get_address_of_U3CU3Ef__mgU24cache0_48() { return &___U3CU3Ef__mgU24cache0_48; }
	inline void set_U3CU3Ef__mgU24cache0_48(Action_1_t819399007 * value)
	{
		___U3CU3Ef__mgU24cache0_48 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_48), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t2897418192  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m2446893047_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m1858812370_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Transform_t3600365921 * p1, bool p2, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_set_volume_m1273312851 (AudioSource_t3935305588 * __this, float p0, const RuntimeMethod* method);
// System.Single Joystick::get_Horizontal()
extern "C" IL2CPP_METHOD_ATTR float Joystick_get_Horizontal_m3614099306 (Joystick_t9498292 * __this, const RuntimeMethod* method);
// System.Single Joystick::get_Vertical()
extern "C" IL2CPP_METHOD_ATTR float Joystick_get_Vertical_m3105283820 (Joystick_t9498292 * __this, const RuntimeMethod* method);
// System.Void ControllerScript::Move()
extern "C" IL2CPP_METHOD_ATTR void ControllerScript_Move_m929746044 (ControllerScript_t326548708 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Component_CompareTag_m1328479619 (Component_t1923634451 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void DamageHandler::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandler_Die_m1148011466 (DamageHandler_t2022420323 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_t1113636619 * Object_Instantiate_TisGameObject_t1113636619_m3006960551 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m3118546832 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, float p1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void DamageHandlerEnemy::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy_Die_m3879310850 (DamageHandlerEnemy_t1319748984 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Slider_set_maxValue_m2251404465 (Slider_t3903728902 * __this, float p0, const RuntimeMethod* method);
// System.Void DamageHandlerHeart::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart_Die_m3960869623 (DamageHandlerHeart_t1292438650 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CanvasGroup>()
inline CanvasGroup_t4083511760 * Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  CanvasGroup_t4083511760 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void CanvasGroup_set_blocksRaycasts_m3675023212 (CanvasGroup_t4083511760 * __this, bool p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" IL2CPP_METHOD_ATTR bool Application_get_isEditor_m857789090 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Camera_ScreenToWorldPoint_m3978588570 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Void DragHandler::Recursive(UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_Recursive_m2344347231 (DragHandler_t4187724030 * __this, Transform_t3600365921 * ___trans0, Vector2_t2156229523  ___WorldPoint1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_m3145433196 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Spawn>()
inline Spawn_t617419884 * Component_GetComponent_TisSpawn_t617419884_m3338007022 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Spawn_t617419884 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void Spawn::SpawnDroppedItem(UnityEngine.Transform,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Spawn_SpawnDroppedItem_m1584144246 (Spawn_t617419884 * __this, Transform_t3600365921 * ___trans0, int32_t ___i1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
inline GameObject_t1113636619 * Object_Instantiate_TisGameObject_t1113636619_m4070250708 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * p0, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m2446893047_gshared)(__this /* static, unused */, p0, method);
}
// System.Void DragHandler::Recursive2(UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_Recursive2_m1786376718 (DragHandler_t4187724030 * __this, Transform_t3600365921 * ___trans0, Vector2_t2156229523  ___WorldPoint1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Random_get_onUnitSphere_m1840191398 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_InvokeRepeating_m650519629 (MonoBehaviour_t3962482529 * __this, String_t* p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_FindWithTag_m981614592 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
extern "C" IL2CPP_METHOD_ATTR void Vector3_Normalize_m914904454 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_Euler_m3049309462 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_RotateTowards_m3102912458 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Camera__ctor_m741555041 (Camera_t4157153871 * __this, const RuntimeMethod* method);
// System.Void Joystick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Joystick__ctor_m1272315817 (Joystick_t9498292 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RectTransformUtility_WorldToScreenPoint_m3914148572 (RuntimeObject * __this /* static, unused */, Camera_t4157153871 * p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  PointerEventData_get_position_m437660275 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  RectTransform_get_sizeDelta_m2183112744 (RectTransform_t3704657025 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Division_m132623573 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// System.Void Joystick::ClampJoystick()
extern "C" IL2CPP_METHOD_ATTR void Joystick_ClampJoystick_m2501349361 (Joystick_t9498292 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::Normalize()
extern "C" IL2CPP_METHOD_ATTR void Vector2_Normalize_m1906922873 (Vector2_t2156229523 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t2670269651 * Component_GetComponent_TisImage_t2670269651_m980647750 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C" IL2CPP_METHOD_ATTR RectTransform_t3704657025 * Graphic_get_rectTransform_m1167152468 (Graphic_t1660335611 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.EventSystems.PointerEventData::get_pressEventCamera()
extern "C" IL2CPP_METHOD_ATTR Camera_t4157153871 * PointerEventData_get_pressEventCamera_m2613974917 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C" IL2CPP_METHOD_ATTR bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, Camera_t4157153871 * p2, Vector2_t2156229523 * p3, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C" IL2CPP_METHOD_ATTR Scene_t2348375561  SceneManager_GetActiveScene_m1825203488 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_m270272723 (Scene_t2348375561 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m3463216446 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
extern "C" IL2CPP_METHOD_ATTR void Application_Quit_m470877999 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void PauseMenu::Resume()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Resume_m690786508 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method);
// System.Void PauseMenu::Pause()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Pause_m890285182 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Time_set_timeScale_m1127545364 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Inventory>()
inline Inventory_t1050226016 * GameObject_GetComponent_TisInventory_t1050226016_m3648803814 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Inventory_t1050226016 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform,System.Boolean)
inline GameObject_t1113636619 * Object_Instantiate_TisGameObject_t1113636619_m4130575780 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * p0, Transform_t3600365921 * p1, bool p2, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Transform_t3600365921 *, bool, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1858812370_gshared)(__this /* static, unused */, p0, p1, p2, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m315980366 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_LookRotation_m3197602968 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C" IL2CPP_METHOD_ATTR float Input_GetAxis_m4009438427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void PlayerMovement::Move()
extern "C" IL2CPP_METHOD_ATTR void PlayerMovement_Move_m167645979 (PlayerMovement_t2731566919 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject SecondSlot::get_Item()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * SecondSlot_get_Item_m1488879554 (SecondSlot_t2904157877 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_SetParent_m381167889 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_m2717073726 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" IL2CPP_METHOD_ATTR int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// System.Void VariableJoystick::OnFixed()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnFixed_m4141959043 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method);
// System.Void VariableJoystick::OnFloat()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnFloat_m165815926 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraFollow::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow__ctor_m2357661391 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFollow::Update()
extern "C" IL2CPP_METHOD_ATTR void CameraFollow_Update_m2790158597 (CameraFollow_t129522575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraFollow_Update_m2790158597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3600365921 * L_0 = __this->get_myTarget_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		Transform_t3600365921 * L_2 = __this->get_myTarget_4();
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_z_4();
		(&V_0)->set_z_4(L_6);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = V_0;
		NullCheck(L_7);
		Transform_set_position_m3387557959(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChangeMusicVolume::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ChangeMusicVolume__ctor_m1263614524 (ChangeMusicVolume_t2599693512 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChangeMusicVolume::Start()
extern "C" IL2CPP_METHOD_ATTR void ChangeMusicVolume_Start_m3352508894 (ChangeMusicVolume_t2599693512 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ChangeMusicVolume::Update()
extern "C" IL2CPP_METHOD_ATTR void ChangeMusicVolume_Update_m2253957616 (ChangeMusicVolume_t2599693512 * __this, const RuntimeMethod* method)
{
	{
		AudioSource_t3935305588 * L_0 = __this->get_myMusic_5();
		Slider_t3903728902 * L_1 = __this->get_Volume_4();
		NullCheck(L_1);
		float L_2 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		NullCheck(L_0);
		AudioSource_set_volume_m1273312851(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ControllerScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ControllerScript__ctor_m1970338229 (ControllerScript_t326548708 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ControllerScript::Update()
extern "C" IL2CPP_METHOD_ATTR void ControllerScript_Update_m4293247761 (ControllerScript_t326548708 * __this, const RuntimeMethod* method)
{
	{
		FloatingJoystick_t3402721920 * L_0 = __this->get_joystick_4();
		NullCheck(L_0);
		float L_1 = Joystick_get_Horizontal_m3614099306(L_0, /*hidden argument*/NULL);
		__this->set_h_6(L_1);
		FloatingJoystick_t3402721920 * L_2 = __this->get_joystick_4();
		NullCheck(L_2);
		float L_3 = Joystick_get_Vertical_m3105283820(L_2, /*hidden argument*/NULL);
		__this->set_v_7(L_3);
		return;
	}
}
// System.Void ControllerScript::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void ControllerScript_FixedUpdate_m1902522150 (ControllerScript_t326548708 * __this, const RuntimeMethod* method)
{
	{
		ControllerScript_Move_m929746044(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ControllerScript::Move()
extern "C" IL2CPP_METHOD_ATTR void ControllerScript_Move_m929746044 (ControllerScript_t326548708 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464 * L_2 = (&V_0);
		float L_3 = L_2->get_y_3();
		float L_4 = __this->get_v_7();
		float L_5 = __this->get_MoveSpeed_5();
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_2->set_y_3(((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), (float)L_6)))));
		Vector3_t3722313464 * L_7 = (&V_0);
		float L_8 = L_7->get_x_2();
		float L_9 = __this->get_h_6();
		float L_10 = __this->get_MoveSpeed_5();
		float L_11 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_7->set_x_2(((float)il2cpp_codegen_add((float)L_8, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), (float)L_11)))));
		float L_12 = (&V_0)->get_y_3();
		if ((!(((float)L_12) > ((float)(40.0f)))))
		{
			goto IL_0070;
		}
	}
	{
		(&V_0)->set_y_3((40.0f));
		goto IL_0092;
	}

IL_0070:
	{
		float L_13 = (&V_0)->get_y_3();
		if ((!(((float)L_13) < ((float)(-40.0f)))))
		{
			goto IL_0092;
		}
	}
	{
		(&V_0)->set_y_3((-40.0f));
		goto IL_0092;
	}

IL_0092:
	{
		float L_14 = (&V_0)->get_x_2();
		if ((!(((float)L_14) > ((float)(40.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		(&V_0)->set_x_2((40.0f));
		goto IL_00d6;
	}

IL_00b4:
	{
		float L_15 = (&V_0)->get_x_2();
		if ((!(((float)L_15) < ((float)(-40.0f)))))
		{
			goto IL_00d6;
		}
	}
	{
		(&V_0)->set_x_2((-40.0f));
		goto IL_00d6;
	}

IL_00d6:
	{
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = V_0;
		NullCheck(L_16);
		Transform_set_position_m3387557959(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DamageHandler::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DamageHandler__ctor_m509107705 (DamageHandler_t2022420323 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_4(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DamageHandler::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void DamageHandler_OnTriggerEnter2D_m1389998856 (DamageHandler_t2022420323 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandler_OnTriggerEnter2D_m1389998856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2804170526, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Collider2D_t2806799626 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m1328479619(L_2, _stringLiteral2176724968, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_4 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)));
		goto IL_00ad;
	}

IL_0034:
	{
		Collider2D_t2806799626 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = Component_CompareTag_m1328479619(L_5, _stringLiteral4200480571, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_7 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)4)));
		goto IL_00ad;
	}

IL_0057:
	{
		Collider2D_t2806799626 * L_8 = ___other0;
		NullCheck(L_8);
		bool L_9 = Component_CompareTag_m1328479619(L_8, _stringLiteral2769116279, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_10 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)20))));
		goto IL_00ad;
	}

IL_007b:
	{
		Collider2D_t2806799626 * L_11 = ___other0;
		NullCheck(L_11);
		bool L_12 = Component_CompareTag_m1328479619(L_11, _stringLiteral3567541360, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_13 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)((int32_t)100))));
		goto IL_00ad;
	}

IL_009f:
	{
		int32_t L_14 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1)));
	}

IL_00ad:
	{
		return;
	}
}
// System.Void DamageHandler::Start()
extern "C" IL2CPP_METHOD_ATTR void DamageHandler_Start_m1242061931 (DamageHandler_t2022420323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandler_Start_m1242061931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2670505167, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3605365887, /*hidden argument*/NULL);
		__this->set_explode_5(L_0);
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral811387782, /*hidden argument*/NULL);
		__this->set_expsound_6(L_1);
		return;
	}
}
// System.Void DamageHandler::Update()
extern "C" IL2CPP_METHOD_ATTR void DamageHandler_Update_m2857232938 (DamageHandler_t2022420323 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_health_4();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		DamageHandler_Die_m1148011466(__this, /*hidden argument*/NULL);
		goto IL_0018;
	}

IL_0017:
	{
		return;
	}

IL_0018:
	{
		return;
	}
}
// System.Void DamageHandler::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandler_Die_m1148011466 (DamageHandler_t2022420323 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandler_Die_m1148011466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_explode_5();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_4 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_0 = L_4;
		GameObject_t1113636619 * L_5 = __this->get_expsound_6();
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_5, L_7, L_8, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_1 = L_9;
		GameObject_t1113636619 * L_10 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_10, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_11, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DamageHandlerEnemy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy__ctor_m2257483237 (DamageHandlerEnemy_t1319748984 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_5(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DamageHandlerEnemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy_OnTriggerEnter2D_m2407517138 (DamageHandlerEnemy_t1319748984 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerEnemy_OnTriggerEnter2D_m2407517138_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2804170526, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Collider2D_t2806799626 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = Component_CompareTag_m1328479619(L_2, _stringLiteral2176724968, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_4 = __this->get_health_5();
		__this->set_health_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)));
		goto IL_00ad;
	}

IL_0034:
	{
		Collider2D_t2806799626 * L_5 = ___other0;
		NullCheck(L_5);
		bool L_6 = Component_CompareTag_m1328479619(L_5, _stringLiteral4200480571, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_7 = __this->get_health_5();
		__this->set_health_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)4)));
		goto IL_00ad;
	}

IL_0057:
	{
		Collider2D_t2806799626 * L_8 = ___other0;
		NullCheck(L_8);
		bool L_9 = Component_CompareTag_m1328479619(L_8, _stringLiteral2769116279, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_10 = __this->get_health_5();
		__this->set_health_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)((int32_t)20))));
		goto IL_00ad;
	}

IL_007b:
	{
		Collider2D_t2806799626 * L_11 = ___other0;
		NullCheck(L_11);
		bool L_12 = Component_CompareTag_m1328479619(L_11, _stringLiteral3567541360, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_009f;
		}
	}
	{
		int32_t L_13 = __this->get_health_5();
		__this->set_health_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)((int32_t)100))));
		goto IL_00ad;
	}

IL_009f:
	{
		int32_t L_14 = __this->get_health_5();
		__this->set_health_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)1)));
	}

IL_00ad:
	{
		return;
	}
}
// System.Void DamageHandlerEnemy::Start()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy_Start_m408010825 (DamageHandlerEnemy_t1319748984 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerEnemy_Start_m408010825_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2670505167, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3605365887, /*hidden argument*/NULL);
		__this->set_explode_6(L_0);
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral811387782, /*hidden argument*/NULL);
		__this->set_expsound_7(L_1);
		return;
	}
}
// System.Void DamageHandlerEnemy::Update()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy_Update_m3999026071 (DamageHandlerEnemy_t1319748984 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerEnemy_Update_m3999026071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_health_5();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		DamageHandlerEnemy_Die_m3879310850(__this, /*hidden argument*/NULL);
		goto IL_0038;
	}

IL_0017:
	{
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2670505167, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		DamageHandlerEnemy_Die_m3879310850(__this, /*hidden argument*/NULL);
		goto IL_0038;
	}

IL_0037:
	{
		return;
	}

IL_0038:
	{
		return;
	}
}
// System.Void DamageHandlerEnemy::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerEnemy_Die_m3879310850 (DamageHandlerEnemy_t1319748984 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerEnemy_Die_m3879310850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_explode_6();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_4 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_0 = L_4;
		GameObject_t1113636619 * L_5 = __this->get_expsound_7();
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_5, L_7, L_8, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_1 = L_9;
		GameObject_t1113636619 * L_10 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_10, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_11, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = __this->get_SpawnItem_4();
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Transform_get_position_m36019626(L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Quaternion_t2301928331  L_16 = Transform_get_rotation_m3502953881(L_15, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_12, L_14, L_16, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_t3600365921 * L_18 = Transform_get_parent_m835071599(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_t1113636619 * L_19 = Component_get_gameObject_m442555142(L_18, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DamageHandlerHeart::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart__ctor_m342847104 (DamageHandlerHeart_t1292438650 * __this, const RuntimeMethod* method)
{
	{
		__this->set_health_4(((int32_t)10));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DamageHandlerHeart::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart_OnTriggerEnter2D_m3333993096 (DamageHandlerHeart_t1292438650 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerHeart_OnTriggerEnter2D_m3333993096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2804170526, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		int32_t L_2 = __this->get_health_4();
		__this->set_health_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)));
		return;
	}
}
// System.Void DamageHandlerHeart::Start()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart_Start_m1905279168 (DamageHandlerHeart_t1292438650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerHeart_Start_m1905279168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2670505167, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3605365887, /*hidden argument*/NULL);
		__this->set_explode_5(L_0);
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral811387782, /*hidden argument*/NULL);
		__this->set_expsound_6(L_1);
		Slider_t3903728902 * L_2 = __this->get_hpSlider_7();
		int32_t L_3 = __this->get_health_4();
		NullCheck(L_2);
		Slider_set_maxValue_m2251404465(L_2, (((float)((float)L_3))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DamageHandlerHeart::Update()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart_Update_m2296818230 (DamageHandlerHeart_t1292438650 * __this, const RuntimeMethod* method)
{
	{
		Slider_t3903728902 * L_0 = __this->get_hpSlider_7();
		int32_t L_1 = __this->get_health_4();
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, (((float)((float)L_1))));
		int32_t L_2 = __this->get_health_4();
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		DamageHandlerHeart_Die_m3960869623(__this, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_0029:
	{
		return;
	}

IL_002a:
	{
		return;
	}
}
// System.Void DamageHandlerHeart::Die()
extern "C" IL2CPP_METHOD_ATTR void DamageHandlerHeart_Die_m3960869623 (DamageHandlerHeart_t1292438650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageHandlerHeart_Die_m3960869623_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = __this->get_explode_5();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_4 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_0 = L_4;
		GameObject_t1113636619 * L_5 = __this->get_expsound_6();
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_5, L_7, L_8, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_1 = L_9;
		GameObject_t1113636619 * L_10 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_10, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_11 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_11, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1555075383, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DragHandler::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DragHandler__ctor_m2623419429 (DragHandler_t4187724030 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragHandler::Start()
extern "C" IL2CPP_METHOD_ATTR void DragHandler_Start_m866278565 (DragHandler_t4187724030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_Start_m866278565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cam_14(L_0);
		GameObject_t1113636619 * L_1 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		__this->set_player_8(L_2);
		GameObject_t1113636619 * L_3 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3914663099, /*hidden argument*/NULL);
		__this->set_EquipSound_7(L_3);
		return;
	}
}
// System.Void DragHandler::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_OnBeginDrag_m3151166853 (DragHandler_t4187724030 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_OnBeginDrag_m3151166853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		((DragHandler_t4187724030_StaticFields*)il2cpp_codegen_static_fields_for(DragHandler_t4187724030_il2cpp_TypeInfo_var))->set_itemBeingDragged_4(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		__this->set_startPosition_5(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Transform_get_parent_m835071599(L_3, /*hidden argument*/NULL);
		__this->set_startParent_6(L_4);
		CanvasGroup_t4083511760 * L_5 = Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026_RuntimeMethod_var);
		NullCheck(L_5);
		CanvasGroup_set_blocksRaycasts_m3675023212(L_5, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DragHandler::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_OnDrag_m2523973970 (DragHandler_t4187724030 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_OnDrag_m2523973970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m3387557959(L_1, L_2, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_001f:
	{
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_4 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2156229523  L_5 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3387557959(L_3, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void DragHandler::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_OnEndDrag_m2610952961 (DragHandler_t4187724030 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_OnEndDrag_m2610952961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		__this->set_a_15((0.0f));
		__this->set_b_16((0.0f));
		__this->set_c_17((0.0f));
		__this->set_d_18((0.0f));
		__this->set_min_19((0.0f));
		((DragHandler_t4187724030_StaticFields*)il2cpp_codegen_static_fields_for(DragHandler_t4187724030_il2cpp_TypeInfo_var))->set_itemBeingDragged_4((GameObject_t1113636619 *)NULL);
		CanvasGroup_t4083511760 * L_0 = Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026(__this, /*hidden argument*/Component_GetComponent_TisCanvasGroup_t4083511760_m1030459026_RuntimeMethod_var);
		NullCheck(L_0);
		CanvasGroup_set_blocksRaycasts_m3675023212(L_0, (bool)1, /*hidden argument*/NULL);
		bool L_1 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0073;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_cam_14();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_4 = Camera_ScreenToWorldPoint_m3978588570(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_WorldPoint_13(L_5);
		goto IL_009c;
	}

IL_0073:
	{
		Camera_t4157153871 * L_6 = __this->get_cam_14();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_7 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector2_t2156229523  L_8 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_10 = Camera_ScreenToWorldPoint_m3978588570(L_6, L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_WorldPoint_13(L_11);
	}

IL_009c:
	{
		Transform_t3600365921 * L_12 = __this->get_player_8();
		Vector2_t2156229523  L_13 = __this->get_WorldPoint_13();
		DragHandler_Recursive_m2344347231(__this, L_12, L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = Transform_get_parent_m835071599(L_14, /*hidden argument*/NULL);
		Transform_t3600365921 * L_16 = __this->get_startParent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00da;
		}
	}
	{
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = __this->get_startPosition_5();
		NullCheck(L_18);
		Transform_set_position_m3387557959(L_18, L_19, /*hidden argument*/NULL);
	}

IL_00da:
	{
		return;
	}
}
// System.Void DragHandler::Recursive(UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_Recursive_m2344347231 (DragHandler_t4187724030 * __this, Transform_t3600365921 * ___trans0, Vector2_t2156229523  ___WorldPoint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_Recursive_m2344347231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	GameObject_t1113636619 * V_3 = NULL;
	{
		__this->set_a_15((0.0f));
		__this->set_b_16((0.0f));
		__this->set_c_17((0.0f));
		__this->set_d_18((0.0f));
		__this->set_min_19((0.0f));
		Transform_t3600365921 * L_0 = ___trans0;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_GetChild_m1092972975(L_0, 0, /*hidden argument*/NULL);
		__this->set_transform0_9(L_1);
		Transform_t3600365921 * L_2 = ___trans0;
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_GetChild_m1092972975(L_2, 1, /*hidden argument*/NULL);
		__this->set_transform1_10(L_3);
		Transform_t3600365921 * L_4 = ___trans0;
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Transform_GetChild_m1092972975(L_4, 2, /*hidden argument*/NULL);
		__this->set_transform2_11(L_5);
		Transform_t3600365921 * L_6 = ___trans0;
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = Transform_GetChild_m1092972975(L_6, 3, /*hidden argument*/NULL);
		__this->set_transform3_12(L_7);
		Transform_t3600365921 * L_8 = __this->get_transform0_9();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_10 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = ___WorldPoint1;
		float L_12 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		__this->set_a_15(L_12);
		Transform_t3600365921 * L_13 = __this->get_transform1_10();
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Transform_get_position_m36019626(L_13, /*hidden argument*/NULL);
		Vector2_t2156229523  L_15 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = ___WorldPoint1;
		float L_17 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		__this->set_b_16(L_17);
		Transform_t3600365921 * L_18 = __this->get_transform2_11();
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21 = ___WorldPoint1;
		float L_22 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		__this->set_c_17(L_22);
		Transform_t3600365921 * L_23 = __this->get_transform3_12();
		NullCheck(L_23);
		Vector3_t3722313464  L_24 = Transform_get_position_m36019626(L_23, /*hidden argument*/NULL);
		Vector2_t2156229523  L_25 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector2_t2156229523  L_26 = ___WorldPoint1;
		float L_27 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		__this->set_d_18(L_27);
		float L_28 = __this->get_a_15();
		__this->set_min_19(L_28);
		float L_29 = __this->get_min_19();
		float L_30 = __this->get_b_16();
		if ((!(((float)L_29) > ((float)L_30))))
		{
			goto IL_0104;
		}
	}
	{
		float L_31 = __this->get_b_16();
		__this->set_min_19(L_31);
	}

IL_0104:
	{
		float L_32 = __this->get_min_19();
		float L_33 = __this->get_c_17();
		if ((!(((float)L_32) > ((float)L_33))))
		{
			goto IL_0121;
		}
	}
	{
		float L_34 = __this->get_c_17();
		__this->set_min_19(L_34);
	}

IL_0121:
	{
		float L_35 = __this->get_min_19();
		float L_36 = __this->get_d_18();
		if ((!(((float)L_35) > ((float)L_36))))
		{
			goto IL_013e;
		}
	}
	{
		float L_37 = __this->get_d_18();
		__this->set_min_19(L_37);
	}

IL_013e:
	{
		float L_38 = __this->get_min_19();
		float L_39 = __this->get_a_15();
		float L_40 = fabsf(((float)il2cpp_codegen_subtract((float)L_38, (float)L_39)));
		if ((!(((double)(((double)((double)L_40)))) < ((double)(0.01)))))
		{
			goto IL_0366;
		}
	}
	{
		float L_41 = __this->get_a_15();
		if ((!(((double)(((double)((double)L_41)))) <= ((double)(0.4)))))
		{
			goto IL_01c4;
		}
	}
	{
		Transform_t3600365921 * L_42 = __this->get_transform0_9();
		NullCheck(L_42);
		int32_t L_43 = Transform_get_childCount_m3145433196(L_42, /*hidden argument*/NULL);
		if ((((int32_t)L_43) > ((int32_t)0)))
		{
			goto IL_01be;
		}
	}
	{
		Spawn_t617419884 * L_44 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_45 = __this->get_transform0_9();
		NullCheck(L_44);
		Spawn_SpawnDroppedItem_m1584144246(L_44, L_45, 0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_46 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_47 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_46, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_0 = L_47;
		GameObject_t1113636619 * L_48 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_48, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_49 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		goto IL_01bf;
	}

IL_01be:
	{
		return;
	}

IL_01bf:
	{
		goto IL_0361;
	}

IL_01c4:
	{
		Transform_t3600365921 * L_50 = __this->get_transform0_9();
		NullCheck(L_50);
		int32_t L_51 = Transform_get_childCount_m3145433196(L_50, /*hidden argument*/NULL);
		if ((((int32_t)L_51) <= ((int32_t)0)))
		{
			goto IL_02bb;
		}
	}
	{
		Transform_t3600365921 * L_52 = __this->get_transform0_9();
		NullCheck(L_52);
		Transform_t3600365921 * L_53 = Transform_GetChild_m1092972975(L_52, 0, /*hidden argument*/NULL);
		NullCheck(L_53);
		Transform_t3600365921 * L_54 = Transform_GetChild_m1092972975(L_53, 0, /*hidden argument*/NULL);
		NullCheck(L_54);
		int32_t L_55 = Transform_get_childCount_m3145433196(L_54, /*hidden argument*/NULL);
		if ((((int32_t)L_55) == ((int32_t)4)))
		{
			goto IL_029d;
		}
	}
	{
		float L_56 = __this->get_b_16();
		float L_57 = __this->get_c_17();
		if ((!(((float)L_56) < ((float)L_57))))
		{
			goto IL_0250;
		}
	}
	{
		Transform_t3600365921 * L_58 = __this->get_transform1_10();
		NullCheck(L_58);
		int32_t L_59 = Transform_get_childCount_m3145433196(L_58, /*hidden argument*/NULL);
		if ((((int32_t)L_59) <= ((int32_t)0)))
		{
			goto IL_024b;
		}
	}
	{
		Transform_t3600365921 * L_60 = __this->get_transform1_10();
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = Transform_GetChild_m1092972975(L_60, 0, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_t3600365921 * L_62 = Transform_GetChild_m1092972975(L_61, 0, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = Transform_get_childCount_m3145433196(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_63) == ((int32_t)4)))
		{
			goto IL_0232;
		}
	}
	{
		return;
	}

IL_0232:
	{
		Transform_t3600365921 * L_64 = __this->get_transform1_10();
		NullCheck(L_64);
		Transform_t3600365921 * L_65 = Transform_GetChild_m1092972975(L_64, 0, /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_t3600365921 * L_66 = Transform_GetChild_m1092972975(L_65, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_67 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_66, L_67, /*hidden argument*/NULL);
	}

IL_024b:
	{
		goto IL_0298;
	}

IL_0250:
	{
		Transform_t3600365921 * L_68 = __this->get_transform2_11();
		NullCheck(L_68);
		int32_t L_69 = Transform_get_childCount_m3145433196(L_68, /*hidden argument*/NULL);
		if ((((int32_t)L_69) <= ((int32_t)0)))
		{
			goto IL_0298;
		}
	}
	{
		Transform_t3600365921 * L_70 = __this->get_transform2_11();
		NullCheck(L_70);
		Transform_t3600365921 * L_71 = Transform_GetChild_m1092972975(L_70, 0, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t3600365921 * L_72 = Transform_GetChild_m1092972975(L_71, 0, /*hidden argument*/NULL);
		NullCheck(L_72);
		int32_t L_73 = Transform_get_childCount_m3145433196(L_72, /*hidden argument*/NULL);
		if ((((int32_t)L_73) == ((int32_t)4)))
		{
			goto IL_027f;
		}
	}
	{
		return;
	}

IL_027f:
	{
		Transform_t3600365921 * L_74 = __this->get_transform2_11();
		NullCheck(L_74);
		Transform_t3600365921 * L_75 = Transform_GetChild_m1092972975(L_74, 0, /*hidden argument*/NULL);
		NullCheck(L_75);
		Transform_t3600365921 * L_76 = Transform_GetChild_m1092972975(L_75, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_77 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_76, L_77, /*hidden argument*/NULL);
	}

IL_0298:
	{
		goto IL_02b6;
	}

IL_029d:
	{
		Transform_t3600365921 * L_78 = __this->get_transform0_9();
		NullCheck(L_78);
		Transform_t3600365921 * L_79 = Transform_GetChild_m1092972975(L_78, 0, /*hidden argument*/NULL);
		NullCheck(L_79);
		Transform_t3600365921 * L_80 = Transform_GetChild_m1092972975(L_79, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_81 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_80, L_81, /*hidden argument*/NULL);
	}

IL_02b6:
	{
		goto IL_0361;
	}

IL_02bb:
	{
		float L_82 = __this->get_b_16();
		float L_83 = __this->get_c_17();
		if ((!(((float)L_82) < ((float)L_83))))
		{
			goto IL_0319;
		}
	}
	{
		Transform_t3600365921 * L_84 = __this->get_transform1_10();
		NullCheck(L_84);
		int32_t L_85 = Transform_get_childCount_m3145433196(L_84, /*hidden argument*/NULL);
		if ((((int32_t)L_85) <= ((int32_t)0)))
		{
			goto IL_0314;
		}
	}
	{
		Transform_t3600365921 * L_86 = __this->get_transform1_10();
		NullCheck(L_86);
		Transform_t3600365921 * L_87 = Transform_GetChild_m1092972975(L_86, 0, /*hidden argument*/NULL);
		NullCheck(L_87);
		Transform_t3600365921 * L_88 = Transform_GetChild_m1092972975(L_87, 0, /*hidden argument*/NULL);
		NullCheck(L_88);
		int32_t L_89 = Transform_get_childCount_m3145433196(L_88, /*hidden argument*/NULL);
		if ((((int32_t)L_89) == ((int32_t)4)))
		{
			goto IL_02fb;
		}
	}
	{
		return;
	}

IL_02fb:
	{
		Transform_t3600365921 * L_90 = __this->get_transform1_10();
		NullCheck(L_90);
		Transform_t3600365921 * L_91 = Transform_GetChild_m1092972975(L_90, 0, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_t3600365921 * L_92 = Transform_GetChild_m1092972975(L_91, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_93 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_92, L_93, /*hidden argument*/NULL);
	}

IL_0314:
	{
		goto IL_0361;
	}

IL_0319:
	{
		Transform_t3600365921 * L_94 = __this->get_transform2_11();
		NullCheck(L_94);
		int32_t L_95 = Transform_get_childCount_m3145433196(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) <= ((int32_t)0)))
		{
			goto IL_0361;
		}
	}
	{
		Transform_t3600365921 * L_96 = __this->get_transform2_11();
		NullCheck(L_96);
		Transform_t3600365921 * L_97 = Transform_GetChild_m1092972975(L_96, 0, /*hidden argument*/NULL);
		NullCheck(L_97);
		Transform_t3600365921 * L_98 = Transform_GetChild_m1092972975(L_97, 0, /*hidden argument*/NULL);
		NullCheck(L_98);
		int32_t L_99 = Transform_get_childCount_m3145433196(L_98, /*hidden argument*/NULL);
		if ((((int32_t)L_99) == ((int32_t)4)))
		{
			goto IL_0348;
		}
	}
	{
		return;
	}

IL_0348:
	{
		Transform_t3600365921 * L_100 = __this->get_transform2_11();
		NullCheck(L_100);
		Transform_t3600365921 * L_101 = Transform_GetChild_m1092972975(L_100, 0, /*hidden argument*/NULL);
		NullCheck(L_101);
		Transform_t3600365921 * L_102 = Transform_GetChild_m1092972975(L_101, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_103 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_102, L_103, /*hidden argument*/NULL);
	}

IL_0361:
	{
		goto IL_09df;
	}

IL_0366:
	{
		float L_104 = __this->get_min_19();
		float L_105 = __this->get_b_16();
		float L_106 = fabsf(((float)il2cpp_codegen_subtract((float)L_104, (float)L_105)));
		if ((!(((double)(((double)((double)L_106)))) < ((double)(0.01)))))
		{
			goto IL_058e;
		}
	}
	{
		float L_107 = __this->get_b_16();
		if ((!(((double)(((double)((double)L_107)))) <= ((double)(0.4)))))
		{
			goto IL_03ec;
		}
	}
	{
		Transform_t3600365921 * L_108 = __this->get_transform1_10();
		NullCheck(L_108);
		int32_t L_109 = Transform_get_childCount_m3145433196(L_108, /*hidden argument*/NULL);
		if ((((int32_t)L_109) > ((int32_t)0)))
		{
			goto IL_03e6;
		}
	}
	{
		Spawn_t617419884 * L_110 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_111 = __this->get_transform1_10();
		NullCheck(L_110);
		Spawn_SpawnDroppedItem_m1584144246(L_110, L_111, 1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_112 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_113 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_112, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_1 = L_113;
		GameObject_t1113636619 * L_114 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_114, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_115 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		goto IL_03e7;
	}

IL_03e6:
	{
		return;
	}

IL_03e7:
	{
		goto IL_0589;
	}

IL_03ec:
	{
		Transform_t3600365921 * L_116 = __this->get_transform1_10();
		NullCheck(L_116);
		int32_t L_117 = Transform_get_childCount_m3145433196(L_116, /*hidden argument*/NULL);
		if ((((int32_t)L_117) <= ((int32_t)0)))
		{
			goto IL_04e3;
		}
	}
	{
		Transform_t3600365921 * L_118 = __this->get_transform1_10();
		NullCheck(L_118);
		Transform_t3600365921 * L_119 = Transform_GetChild_m1092972975(L_118, 0, /*hidden argument*/NULL);
		NullCheck(L_119);
		Transform_t3600365921 * L_120 = Transform_GetChild_m1092972975(L_119, 0, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = Transform_get_childCount_m3145433196(L_120, /*hidden argument*/NULL);
		if ((((int32_t)L_121) == ((int32_t)4)))
		{
			goto IL_04c5;
		}
	}
	{
		float L_122 = __this->get_a_15();
		float L_123 = __this->get_d_18();
		if ((!(((float)L_122) < ((float)L_123))))
		{
			goto IL_0478;
		}
	}
	{
		Transform_t3600365921 * L_124 = __this->get_transform0_9();
		NullCheck(L_124);
		int32_t L_125 = Transform_get_childCount_m3145433196(L_124, /*hidden argument*/NULL);
		if ((((int32_t)L_125) <= ((int32_t)0)))
		{
			goto IL_0473;
		}
	}
	{
		Transform_t3600365921 * L_126 = __this->get_transform0_9();
		NullCheck(L_126);
		Transform_t3600365921 * L_127 = Transform_GetChild_m1092972975(L_126, 0, /*hidden argument*/NULL);
		NullCheck(L_127);
		Transform_t3600365921 * L_128 = Transform_GetChild_m1092972975(L_127, 0, /*hidden argument*/NULL);
		NullCheck(L_128);
		int32_t L_129 = Transform_get_childCount_m3145433196(L_128, /*hidden argument*/NULL);
		if ((((int32_t)L_129) == ((int32_t)4)))
		{
			goto IL_045a;
		}
	}
	{
		return;
	}

IL_045a:
	{
		Transform_t3600365921 * L_130 = __this->get_transform0_9();
		NullCheck(L_130);
		Transform_t3600365921 * L_131 = Transform_GetChild_m1092972975(L_130, 0, /*hidden argument*/NULL);
		NullCheck(L_131);
		Transform_t3600365921 * L_132 = Transform_GetChild_m1092972975(L_131, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_133 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_132, L_133, /*hidden argument*/NULL);
	}

IL_0473:
	{
		goto IL_04c0;
	}

IL_0478:
	{
		Transform_t3600365921 * L_134 = __this->get_transform3_12();
		NullCheck(L_134);
		int32_t L_135 = Transform_get_childCount_m3145433196(L_134, /*hidden argument*/NULL);
		if ((((int32_t)L_135) <= ((int32_t)0)))
		{
			goto IL_04c0;
		}
	}
	{
		Transform_t3600365921 * L_136 = __this->get_transform3_12();
		NullCheck(L_136);
		Transform_t3600365921 * L_137 = Transform_GetChild_m1092972975(L_136, 0, /*hidden argument*/NULL);
		NullCheck(L_137);
		Transform_t3600365921 * L_138 = Transform_GetChild_m1092972975(L_137, 0, /*hidden argument*/NULL);
		NullCheck(L_138);
		int32_t L_139 = Transform_get_childCount_m3145433196(L_138, /*hidden argument*/NULL);
		if ((((int32_t)L_139) == ((int32_t)4)))
		{
			goto IL_04a7;
		}
	}
	{
		return;
	}

IL_04a7:
	{
		Transform_t3600365921 * L_140 = __this->get_transform3_12();
		NullCheck(L_140);
		Transform_t3600365921 * L_141 = Transform_GetChild_m1092972975(L_140, 0, /*hidden argument*/NULL);
		NullCheck(L_141);
		Transform_t3600365921 * L_142 = Transform_GetChild_m1092972975(L_141, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_143 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_142, L_143, /*hidden argument*/NULL);
	}

IL_04c0:
	{
		goto IL_04de;
	}

IL_04c5:
	{
		Transform_t3600365921 * L_144 = __this->get_transform1_10();
		NullCheck(L_144);
		Transform_t3600365921 * L_145 = Transform_GetChild_m1092972975(L_144, 0, /*hidden argument*/NULL);
		NullCheck(L_145);
		Transform_t3600365921 * L_146 = Transform_GetChild_m1092972975(L_145, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_147 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_146, L_147, /*hidden argument*/NULL);
	}

IL_04de:
	{
		goto IL_0589;
	}

IL_04e3:
	{
		float L_148 = __this->get_a_15();
		float L_149 = __this->get_d_18();
		if ((!(((float)L_148) < ((float)L_149))))
		{
			goto IL_0541;
		}
	}
	{
		Transform_t3600365921 * L_150 = __this->get_transform0_9();
		NullCheck(L_150);
		int32_t L_151 = Transform_get_childCount_m3145433196(L_150, /*hidden argument*/NULL);
		if ((((int32_t)L_151) <= ((int32_t)0)))
		{
			goto IL_053c;
		}
	}
	{
		Transform_t3600365921 * L_152 = __this->get_transform0_9();
		NullCheck(L_152);
		Transform_t3600365921 * L_153 = Transform_GetChild_m1092972975(L_152, 0, /*hidden argument*/NULL);
		NullCheck(L_153);
		Transform_t3600365921 * L_154 = Transform_GetChild_m1092972975(L_153, 0, /*hidden argument*/NULL);
		NullCheck(L_154);
		int32_t L_155 = Transform_get_childCount_m3145433196(L_154, /*hidden argument*/NULL);
		if ((((int32_t)L_155) == ((int32_t)4)))
		{
			goto IL_0523;
		}
	}
	{
		return;
	}

IL_0523:
	{
		Transform_t3600365921 * L_156 = __this->get_transform0_9();
		NullCheck(L_156);
		Transform_t3600365921 * L_157 = Transform_GetChild_m1092972975(L_156, 0, /*hidden argument*/NULL);
		NullCheck(L_157);
		Transform_t3600365921 * L_158 = Transform_GetChild_m1092972975(L_157, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_159 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_158, L_159, /*hidden argument*/NULL);
	}

IL_053c:
	{
		goto IL_0589;
	}

IL_0541:
	{
		Transform_t3600365921 * L_160 = __this->get_transform3_12();
		NullCheck(L_160);
		int32_t L_161 = Transform_get_childCount_m3145433196(L_160, /*hidden argument*/NULL);
		if ((((int32_t)L_161) <= ((int32_t)0)))
		{
			goto IL_0589;
		}
	}
	{
		Transform_t3600365921 * L_162 = __this->get_transform3_12();
		NullCheck(L_162);
		Transform_t3600365921 * L_163 = Transform_GetChild_m1092972975(L_162, 0, /*hidden argument*/NULL);
		NullCheck(L_163);
		Transform_t3600365921 * L_164 = Transform_GetChild_m1092972975(L_163, 0, /*hidden argument*/NULL);
		NullCheck(L_164);
		int32_t L_165 = Transform_get_childCount_m3145433196(L_164, /*hidden argument*/NULL);
		if ((((int32_t)L_165) == ((int32_t)4)))
		{
			goto IL_0570;
		}
	}
	{
		return;
	}

IL_0570:
	{
		Transform_t3600365921 * L_166 = __this->get_transform3_12();
		NullCheck(L_166);
		Transform_t3600365921 * L_167 = Transform_GetChild_m1092972975(L_166, 0, /*hidden argument*/NULL);
		NullCheck(L_167);
		Transform_t3600365921 * L_168 = Transform_GetChild_m1092972975(L_167, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_169 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_168, L_169, /*hidden argument*/NULL);
	}

IL_0589:
	{
		goto IL_09df;
	}

IL_058e:
	{
		float L_170 = __this->get_min_19();
		float L_171 = __this->get_c_17();
		float L_172 = fabsf(((float)il2cpp_codegen_subtract((float)L_170, (float)L_171)));
		if ((!(((double)(((double)((double)L_172)))) < ((double)(0.01)))))
		{
			goto IL_07b6;
		}
	}
	{
		float L_173 = __this->get_c_17();
		if ((!(((double)(((double)((double)L_173)))) <= ((double)(0.4)))))
		{
			goto IL_0614;
		}
	}
	{
		Transform_t3600365921 * L_174 = __this->get_transform2_11();
		NullCheck(L_174);
		int32_t L_175 = Transform_get_childCount_m3145433196(L_174, /*hidden argument*/NULL);
		if ((((int32_t)L_175) > ((int32_t)0)))
		{
			goto IL_060e;
		}
	}
	{
		Spawn_t617419884 * L_176 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_177 = __this->get_transform2_11();
		NullCheck(L_176);
		Spawn_SpawnDroppedItem_m1584144246(L_176, L_177, 2, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_178 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_179 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_178, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_2 = L_179;
		GameObject_t1113636619 * L_180 = V_2;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_180, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_181 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_181, /*hidden argument*/NULL);
		goto IL_060f;
	}

IL_060e:
	{
		return;
	}

IL_060f:
	{
		goto IL_07b1;
	}

IL_0614:
	{
		Transform_t3600365921 * L_182 = __this->get_transform2_11();
		NullCheck(L_182);
		int32_t L_183 = Transform_get_childCount_m3145433196(L_182, /*hidden argument*/NULL);
		if ((((int32_t)L_183) <= ((int32_t)0)))
		{
			goto IL_070b;
		}
	}
	{
		Transform_t3600365921 * L_184 = __this->get_transform2_11();
		NullCheck(L_184);
		Transform_t3600365921 * L_185 = Transform_GetChild_m1092972975(L_184, 0, /*hidden argument*/NULL);
		NullCheck(L_185);
		Transform_t3600365921 * L_186 = Transform_GetChild_m1092972975(L_185, 0, /*hidden argument*/NULL);
		NullCheck(L_186);
		int32_t L_187 = Transform_get_childCount_m3145433196(L_186, /*hidden argument*/NULL);
		if ((((int32_t)L_187) == ((int32_t)4)))
		{
			goto IL_06ed;
		}
	}
	{
		float L_188 = __this->get_a_15();
		float L_189 = __this->get_d_18();
		if ((!(((float)L_188) < ((float)L_189))))
		{
			goto IL_06a0;
		}
	}
	{
		Transform_t3600365921 * L_190 = __this->get_transform0_9();
		NullCheck(L_190);
		int32_t L_191 = Transform_get_childCount_m3145433196(L_190, /*hidden argument*/NULL);
		if ((((int32_t)L_191) <= ((int32_t)0)))
		{
			goto IL_069b;
		}
	}
	{
		Transform_t3600365921 * L_192 = __this->get_transform0_9();
		NullCheck(L_192);
		Transform_t3600365921 * L_193 = Transform_GetChild_m1092972975(L_192, 0, /*hidden argument*/NULL);
		NullCheck(L_193);
		Transform_t3600365921 * L_194 = Transform_GetChild_m1092972975(L_193, 0, /*hidden argument*/NULL);
		NullCheck(L_194);
		int32_t L_195 = Transform_get_childCount_m3145433196(L_194, /*hidden argument*/NULL);
		if ((((int32_t)L_195) == ((int32_t)4)))
		{
			goto IL_0682;
		}
	}
	{
		return;
	}

IL_0682:
	{
		Transform_t3600365921 * L_196 = __this->get_transform0_9();
		NullCheck(L_196);
		Transform_t3600365921 * L_197 = Transform_GetChild_m1092972975(L_196, 0, /*hidden argument*/NULL);
		NullCheck(L_197);
		Transform_t3600365921 * L_198 = Transform_GetChild_m1092972975(L_197, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_199 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_198, L_199, /*hidden argument*/NULL);
	}

IL_069b:
	{
		goto IL_06e8;
	}

IL_06a0:
	{
		Transform_t3600365921 * L_200 = __this->get_transform3_12();
		NullCheck(L_200);
		int32_t L_201 = Transform_get_childCount_m3145433196(L_200, /*hidden argument*/NULL);
		if ((((int32_t)L_201) <= ((int32_t)0)))
		{
			goto IL_06e8;
		}
	}
	{
		Transform_t3600365921 * L_202 = __this->get_transform3_12();
		NullCheck(L_202);
		Transform_t3600365921 * L_203 = Transform_GetChild_m1092972975(L_202, 0, /*hidden argument*/NULL);
		NullCheck(L_203);
		Transform_t3600365921 * L_204 = Transform_GetChild_m1092972975(L_203, 0, /*hidden argument*/NULL);
		NullCheck(L_204);
		int32_t L_205 = Transform_get_childCount_m3145433196(L_204, /*hidden argument*/NULL);
		if ((((int32_t)L_205) == ((int32_t)4)))
		{
			goto IL_06cf;
		}
	}
	{
		return;
	}

IL_06cf:
	{
		Transform_t3600365921 * L_206 = __this->get_transform3_12();
		NullCheck(L_206);
		Transform_t3600365921 * L_207 = Transform_GetChild_m1092972975(L_206, 0, /*hidden argument*/NULL);
		NullCheck(L_207);
		Transform_t3600365921 * L_208 = Transform_GetChild_m1092972975(L_207, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_209 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_208, L_209, /*hidden argument*/NULL);
	}

IL_06e8:
	{
		goto IL_0706;
	}

IL_06ed:
	{
		Transform_t3600365921 * L_210 = __this->get_transform2_11();
		NullCheck(L_210);
		Transform_t3600365921 * L_211 = Transform_GetChild_m1092972975(L_210, 0, /*hidden argument*/NULL);
		NullCheck(L_211);
		Transform_t3600365921 * L_212 = Transform_GetChild_m1092972975(L_211, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_213 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_212, L_213, /*hidden argument*/NULL);
	}

IL_0706:
	{
		goto IL_07b1;
	}

IL_070b:
	{
		float L_214 = __this->get_a_15();
		float L_215 = __this->get_d_18();
		if ((!(((float)L_214) < ((float)L_215))))
		{
			goto IL_0769;
		}
	}
	{
		Transform_t3600365921 * L_216 = __this->get_transform0_9();
		NullCheck(L_216);
		int32_t L_217 = Transform_get_childCount_m3145433196(L_216, /*hidden argument*/NULL);
		if ((((int32_t)L_217) <= ((int32_t)0)))
		{
			goto IL_0764;
		}
	}
	{
		Transform_t3600365921 * L_218 = __this->get_transform0_9();
		NullCheck(L_218);
		Transform_t3600365921 * L_219 = Transform_GetChild_m1092972975(L_218, 0, /*hidden argument*/NULL);
		NullCheck(L_219);
		Transform_t3600365921 * L_220 = Transform_GetChild_m1092972975(L_219, 0, /*hidden argument*/NULL);
		NullCheck(L_220);
		int32_t L_221 = Transform_get_childCount_m3145433196(L_220, /*hidden argument*/NULL);
		if ((((int32_t)L_221) == ((int32_t)4)))
		{
			goto IL_074b;
		}
	}
	{
		return;
	}

IL_074b:
	{
		Transform_t3600365921 * L_222 = __this->get_transform0_9();
		NullCheck(L_222);
		Transform_t3600365921 * L_223 = Transform_GetChild_m1092972975(L_222, 0, /*hidden argument*/NULL);
		NullCheck(L_223);
		Transform_t3600365921 * L_224 = Transform_GetChild_m1092972975(L_223, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_225 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_224, L_225, /*hidden argument*/NULL);
	}

IL_0764:
	{
		goto IL_07b1;
	}

IL_0769:
	{
		Transform_t3600365921 * L_226 = __this->get_transform3_12();
		NullCheck(L_226);
		int32_t L_227 = Transform_get_childCount_m3145433196(L_226, /*hidden argument*/NULL);
		if ((((int32_t)L_227) <= ((int32_t)0)))
		{
			goto IL_07b1;
		}
	}
	{
		Transform_t3600365921 * L_228 = __this->get_transform3_12();
		NullCheck(L_228);
		Transform_t3600365921 * L_229 = Transform_GetChild_m1092972975(L_228, 0, /*hidden argument*/NULL);
		NullCheck(L_229);
		Transform_t3600365921 * L_230 = Transform_GetChild_m1092972975(L_229, 0, /*hidden argument*/NULL);
		NullCheck(L_230);
		int32_t L_231 = Transform_get_childCount_m3145433196(L_230, /*hidden argument*/NULL);
		if ((((int32_t)L_231) == ((int32_t)4)))
		{
			goto IL_0798;
		}
	}
	{
		return;
	}

IL_0798:
	{
		Transform_t3600365921 * L_232 = __this->get_transform3_12();
		NullCheck(L_232);
		Transform_t3600365921 * L_233 = Transform_GetChild_m1092972975(L_232, 0, /*hidden argument*/NULL);
		NullCheck(L_233);
		Transform_t3600365921 * L_234 = Transform_GetChild_m1092972975(L_233, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_235 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_234, L_235, /*hidden argument*/NULL);
	}

IL_07b1:
	{
		goto IL_09df;
	}

IL_07b6:
	{
		float L_236 = __this->get_min_19();
		float L_237 = __this->get_d_18();
		float L_238 = fabsf(((float)il2cpp_codegen_subtract((float)L_236, (float)L_237)));
		if ((!(((double)(((double)((double)L_238)))) < ((double)(0.01)))))
		{
			goto IL_09de;
		}
	}
	{
		float L_239 = __this->get_d_18();
		if ((!(((double)(((double)((double)L_239)))) <= ((double)(0.4)))))
		{
			goto IL_083c;
		}
	}
	{
		Transform_t3600365921 * L_240 = __this->get_transform3_12();
		NullCheck(L_240);
		int32_t L_241 = Transform_get_childCount_m3145433196(L_240, /*hidden argument*/NULL);
		if ((((int32_t)L_241) > ((int32_t)0)))
		{
			goto IL_0836;
		}
	}
	{
		Spawn_t617419884 * L_242 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_243 = __this->get_transform3_12();
		NullCheck(L_242);
		Spawn_SpawnDroppedItem_m1584144246(L_242, L_243, 3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_244 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_245 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_244, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_3 = L_245;
		GameObject_t1113636619 * L_246 = V_3;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_246, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_247 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_247, /*hidden argument*/NULL);
		goto IL_0837;
	}

IL_0836:
	{
		return;
	}

IL_0837:
	{
		goto IL_09d9;
	}

IL_083c:
	{
		Transform_t3600365921 * L_248 = __this->get_transform3_12();
		NullCheck(L_248);
		int32_t L_249 = Transform_get_childCount_m3145433196(L_248, /*hidden argument*/NULL);
		if ((((int32_t)L_249) <= ((int32_t)0)))
		{
			goto IL_0933;
		}
	}
	{
		Transform_t3600365921 * L_250 = __this->get_transform3_12();
		NullCheck(L_250);
		Transform_t3600365921 * L_251 = Transform_GetChild_m1092972975(L_250, 0, /*hidden argument*/NULL);
		NullCheck(L_251);
		Transform_t3600365921 * L_252 = Transform_GetChild_m1092972975(L_251, 0, /*hidden argument*/NULL);
		NullCheck(L_252);
		int32_t L_253 = Transform_get_childCount_m3145433196(L_252, /*hidden argument*/NULL);
		if ((((int32_t)L_253) == ((int32_t)4)))
		{
			goto IL_0915;
		}
	}
	{
		float L_254 = __this->get_b_16();
		float L_255 = __this->get_c_17();
		if ((!(((float)L_254) < ((float)L_255))))
		{
			goto IL_08c8;
		}
	}
	{
		Transform_t3600365921 * L_256 = __this->get_transform1_10();
		NullCheck(L_256);
		int32_t L_257 = Transform_get_childCount_m3145433196(L_256, /*hidden argument*/NULL);
		if ((((int32_t)L_257) <= ((int32_t)0)))
		{
			goto IL_08c3;
		}
	}
	{
		Transform_t3600365921 * L_258 = __this->get_transform1_10();
		NullCheck(L_258);
		Transform_t3600365921 * L_259 = Transform_GetChild_m1092972975(L_258, 0, /*hidden argument*/NULL);
		NullCheck(L_259);
		Transform_t3600365921 * L_260 = Transform_GetChild_m1092972975(L_259, 0, /*hidden argument*/NULL);
		NullCheck(L_260);
		int32_t L_261 = Transform_get_childCount_m3145433196(L_260, /*hidden argument*/NULL);
		if ((((int32_t)L_261) == ((int32_t)4)))
		{
			goto IL_08aa;
		}
	}
	{
		return;
	}

IL_08aa:
	{
		Transform_t3600365921 * L_262 = __this->get_transform1_10();
		NullCheck(L_262);
		Transform_t3600365921 * L_263 = Transform_GetChild_m1092972975(L_262, 0, /*hidden argument*/NULL);
		NullCheck(L_263);
		Transform_t3600365921 * L_264 = Transform_GetChild_m1092972975(L_263, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_265 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_264, L_265, /*hidden argument*/NULL);
	}

IL_08c3:
	{
		goto IL_0910;
	}

IL_08c8:
	{
		Transform_t3600365921 * L_266 = __this->get_transform2_11();
		NullCheck(L_266);
		int32_t L_267 = Transform_get_childCount_m3145433196(L_266, /*hidden argument*/NULL);
		if ((((int32_t)L_267) <= ((int32_t)0)))
		{
			goto IL_0910;
		}
	}
	{
		Transform_t3600365921 * L_268 = __this->get_transform2_11();
		NullCheck(L_268);
		Transform_t3600365921 * L_269 = Transform_GetChild_m1092972975(L_268, 0, /*hidden argument*/NULL);
		NullCheck(L_269);
		Transform_t3600365921 * L_270 = Transform_GetChild_m1092972975(L_269, 0, /*hidden argument*/NULL);
		NullCheck(L_270);
		int32_t L_271 = Transform_get_childCount_m3145433196(L_270, /*hidden argument*/NULL);
		if ((((int32_t)L_271) == ((int32_t)4)))
		{
			goto IL_08f7;
		}
	}
	{
		return;
	}

IL_08f7:
	{
		Transform_t3600365921 * L_272 = __this->get_transform2_11();
		NullCheck(L_272);
		Transform_t3600365921 * L_273 = Transform_GetChild_m1092972975(L_272, 0, /*hidden argument*/NULL);
		NullCheck(L_273);
		Transform_t3600365921 * L_274 = Transform_GetChild_m1092972975(L_273, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_275 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_274, L_275, /*hidden argument*/NULL);
	}

IL_0910:
	{
		goto IL_092e;
	}

IL_0915:
	{
		Transform_t3600365921 * L_276 = __this->get_transform3_12();
		NullCheck(L_276);
		Transform_t3600365921 * L_277 = Transform_GetChild_m1092972975(L_276, 0, /*hidden argument*/NULL);
		NullCheck(L_277);
		Transform_t3600365921 * L_278 = Transform_GetChild_m1092972975(L_277, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_279 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_278, L_279, /*hidden argument*/NULL);
	}

IL_092e:
	{
		goto IL_09d9;
	}

IL_0933:
	{
		float L_280 = __this->get_b_16();
		float L_281 = __this->get_c_17();
		if ((!(((float)L_280) < ((float)L_281))))
		{
			goto IL_0991;
		}
	}
	{
		Transform_t3600365921 * L_282 = __this->get_transform1_10();
		NullCheck(L_282);
		int32_t L_283 = Transform_get_childCount_m3145433196(L_282, /*hidden argument*/NULL);
		if ((((int32_t)L_283) <= ((int32_t)0)))
		{
			goto IL_098c;
		}
	}
	{
		Transform_t3600365921 * L_284 = __this->get_transform1_10();
		NullCheck(L_284);
		Transform_t3600365921 * L_285 = Transform_GetChild_m1092972975(L_284, 0, /*hidden argument*/NULL);
		NullCheck(L_285);
		Transform_t3600365921 * L_286 = Transform_GetChild_m1092972975(L_285, 0, /*hidden argument*/NULL);
		NullCheck(L_286);
		int32_t L_287 = Transform_get_childCount_m3145433196(L_286, /*hidden argument*/NULL);
		if ((((int32_t)L_287) == ((int32_t)4)))
		{
			goto IL_0973;
		}
	}
	{
		return;
	}

IL_0973:
	{
		Transform_t3600365921 * L_288 = __this->get_transform1_10();
		NullCheck(L_288);
		Transform_t3600365921 * L_289 = Transform_GetChild_m1092972975(L_288, 0, /*hidden argument*/NULL);
		NullCheck(L_289);
		Transform_t3600365921 * L_290 = Transform_GetChild_m1092972975(L_289, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_291 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_290, L_291, /*hidden argument*/NULL);
	}

IL_098c:
	{
		goto IL_09d9;
	}

IL_0991:
	{
		Transform_t3600365921 * L_292 = __this->get_transform2_11();
		NullCheck(L_292);
		int32_t L_293 = Transform_get_childCount_m3145433196(L_292, /*hidden argument*/NULL);
		if ((((int32_t)L_293) <= ((int32_t)0)))
		{
			goto IL_09d9;
		}
	}
	{
		Transform_t3600365921 * L_294 = __this->get_transform2_11();
		NullCheck(L_294);
		Transform_t3600365921 * L_295 = Transform_GetChild_m1092972975(L_294, 0, /*hidden argument*/NULL);
		NullCheck(L_295);
		Transform_t3600365921 * L_296 = Transform_GetChild_m1092972975(L_295, 0, /*hidden argument*/NULL);
		NullCheck(L_296);
		int32_t L_297 = Transform_get_childCount_m3145433196(L_296, /*hidden argument*/NULL);
		if ((((int32_t)L_297) == ((int32_t)4)))
		{
			goto IL_09c0;
		}
	}
	{
		return;
	}

IL_09c0:
	{
		Transform_t3600365921 * L_298 = __this->get_transform2_11();
		NullCheck(L_298);
		Transform_t3600365921 * L_299 = Transform_GetChild_m1092972975(L_298, 0, /*hidden argument*/NULL);
		NullCheck(L_299);
		Transform_t3600365921 * L_300 = Transform_GetChild_m1092972975(L_299, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_301 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_300, L_301, /*hidden argument*/NULL);
	}

IL_09d9:
	{
		goto IL_09df;
	}

IL_09de:
	{
		return;
	}

IL_09df:
	{
		return;
	}
}
// System.Void DragHandler::Recursive2(UnityEngine.Transform,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void DragHandler_Recursive2_m1786376718 (DragHandler_t4187724030 * __this, Transform_t3600365921 * ___trans0, Vector2_t2156229523  ___WorldPoint1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DragHandler_Recursive2_m1786376718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		__this->set_a_15((0.0f));
		__this->set_b_16((0.0f));
		__this->set_c_17((0.0f));
		__this->set_min_19((0.0f));
		Transform_t3600365921 * L_0 = ___trans0;
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Transform_GetChild_m1092972975(L_0, 0, /*hidden argument*/NULL);
		__this->set_transform0_9(L_1);
		Transform_t3600365921 * L_2 = ___trans0;
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_GetChild_m1092972975(L_2, 1, /*hidden argument*/NULL);
		__this->set_transform1_10(L_3);
		Transform_t3600365921 * L_4 = ___trans0;
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Transform_GetChild_m1092972975(L_4, 2, /*hidden argument*/NULL);
		__this->set_transform2_11(L_5);
		Transform_t3600365921 * L_6 = __this->get_transform0_9();
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_8 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = ___WorldPoint1;
		float L_10 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		__this->set_a_15(L_10);
		Transform_t3600365921 * L_11 = __this->get_transform1_10();
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = ___WorldPoint1;
		float L_15 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		__this->set_b_16(L_15);
		Transform_t3600365921 * L_16 = __this->get_transform2_11();
		NullCheck(L_16);
		Vector3_t3722313464  L_17 = Transform_get_position_m36019626(L_16, /*hidden argument*/NULL);
		Vector2_t2156229523  L_18 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Vector2_t2156229523  L_19 = ___WorldPoint1;
		float L_20 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		__this->set_c_17(L_20);
		float L_21 = __this->get_a_15();
		__this->set_min_19(L_21);
		float L_22 = __this->get_min_19();
		float L_23 = __this->get_b_16();
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_00d0;
		}
	}
	{
		float L_24 = __this->get_b_16();
		__this->set_min_19(L_24);
	}

IL_00d0:
	{
		float L_25 = __this->get_min_19();
		float L_26 = __this->get_c_17();
		if ((!(((float)L_25) > ((float)L_26))))
		{
			goto IL_00ed;
		}
	}
	{
		float L_27 = __this->get_c_17();
		__this->set_min_19(L_27);
	}

IL_00ed:
	{
		float L_28 = __this->get_min_19();
		float L_29 = __this->get_a_15();
		float L_30 = fabsf(((float)il2cpp_codegen_subtract((float)L_28, (float)L_29)));
		if ((!(((double)(((double)((double)L_30)))) < ((double)(0.01)))))
		{
			goto IL_0361;
		}
	}
	{
		float L_31 = __this->get_a_15();
		if ((!(((double)(((double)((double)L_31)))) <= ((double)(0.4)))))
		{
			goto IL_0173;
		}
	}
	{
		Transform_t3600365921 * L_32 = __this->get_transform0_9();
		NullCheck(L_32);
		int32_t L_33 = Transform_get_childCount_m3145433196(L_32, /*hidden argument*/NULL);
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_016d;
		}
	}
	{
		Spawn_t617419884 * L_34 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_35 = __this->get_transform0_9();
		NullCheck(L_34);
		Spawn_SpawnDroppedItem_m1584144246(L_34, L_35, 0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_36 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_37 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_36, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_0 = L_37;
		GameObject_t1113636619 * L_38 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_38, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_39 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		goto IL_016e;
	}

IL_016d:
	{
		return;
	}

IL_016e:
	{
		goto IL_035c;
	}

IL_0173:
	{
		Transform_t3600365921 * L_40 = __this->get_transform0_9();
		NullCheck(L_40);
		int32_t L_41 = Transform_get_childCount_m3145433196(L_40, /*hidden argument*/NULL);
		if ((((int32_t)L_41) <= ((int32_t)0)))
		{
			goto IL_026a;
		}
	}
	{
		Transform_t3600365921 * L_42 = __this->get_transform0_9();
		NullCheck(L_42);
		Transform_t3600365921 * L_43 = Transform_GetChild_m1092972975(L_42, 0, /*hidden argument*/NULL);
		NullCheck(L_43);
		Transform_t3600365921 * L_44 = Transform_GetChild_m1092972975(L_43, 0, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = Transform_get_childCount_m3145433196(L_44, /*hidden argument*/NULL);
		if ((((int32_t)L_45) == ((int32_t)4)))
		{
			goto IL_024c;
		}
	}
	{
		float L_46 = __this->get_b_16();
		float L_47 = __this->get_c_17();
		if ((!(((float)L_46) < ((float)L_47))))
		{
			goto IL_01ff;
		}
	}
	{
		Transform_t3600365921 * L_48 = __this->get_transform1_10();
		NullCheck(L_48);
		int32_t L_49 = Transform_get_childCount_m3145433196(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_01fa;
		}
	}
	{
		Transform_t3600365921 * L_50 = __this->get_transform1_10();
		NullCheck(L_50);
		Transform_t3600365921 * L_51 = Transform_GetChild_m1092972975(L_50, 0, /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_t3600365921 * L_52 = Transform_GetChild_m1092972975(L_51, 0, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = Transform_get_childCount_m3145433196(L_52, /*hidden argument*/NULL);
		if ((((int32_t)L_53) == ((int32_t)4)))
		{
			goto IL_01e1;
		}
	}
	{
		return;
	}

IL_01e1:
	{
		Transform_t3600365921 * L_54 = __this->get_transform1_10();
		NullCheck(L_54);
		Transform_t3600365921 * L_55 = Transform_GetChild_m1092972975(L_54, 0, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_t3600365921 * L_56 = Transform_GetChild_m1092972975(L_55, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_57 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_56, L_57, /*hidden argument*/NULL);
	}

IL_01fa:
	{
		goto IL_0247;
	}

IL_01ff:
	{
		Transform_t3600365921 * L_58 = __this->get_transform2_11();
		NullCheck(L_58);
		int32_t L_59 = Transform_get_childCount_m3145433196(L_58, /*hidden argument*/NULL);
		if ((((int32_t)L_59) <= ((int32_t)0)))
		{
			goto IL_0247;
		}
	}
	{
		Transform_t3600365921 * L_60 = __this->get_transform2_11();
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = Transform_GetChild_m1092972975(L_60, 0, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_t3600365921 * L_62 = Transform_GetChild_m1092972975(L_61, 0, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = Transform_get_childCount_m3145433196(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_63) == ((int32_t)4)))
		{
			goto IL_022e;
		}
	}
	{
		return;
	}

IL_022e:
	{
		Transform_t3600365921 * L_64 = __this->get_transform2_11();
		NullCheck(L_64);
		Transform_t3600365921 * L_65 = Transform_GetChild_m1092972975(L_64, 0, /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_t3600365921 * L_66 = Transform_GetChild_m1092972975(L_65, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_67 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_66, L_67, /*hidden argument*/NULL);
	}

IL_0247:
	{
		goto IL_0265;
	}

IL_024c:
	{
		Transform_t3600365921 * L_68 = __this->get_transform0_9();
		NullCheck(L_68);
		Transform_t3600365921 * L_69 = Transform_GetChild_m1092972975(L_68, 0, /*hidden argument*/NULL);
		NullCheck(L_69);
		Transform_t3600365921 * L_70 = Transform_GetChild_m1092972975(L_69, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_71 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_70, L_71, /*hidden argument*/NULL);
	}

IL_0265:
	{
		goto IL_035c;
	}

IL_026a:
	{
		float L_72 = __this->get_b_16();
		float L_73 = __this->get_c_17();
		if ((!(((float)L_72) < ((float)L_73))))
		{
			goto IL_0314;
		}
	}
	{
		Transform_t3600365921 * L_74 = __this->get_transform1_10();
		NullCheck(L_74);
		int32_t L_75 = Transform_get_childCount_m3145433196(L_74, /*hidden argument*/NULL);
		if ((((int32_t)L_75) <= ((int32_t)0)))
		{
			goto IL_030f;
		}
	}
	{
		Transform_t3600365921 * L_76 = __this->get_transform1_10();
		NullCheck(L_76);
		Transform_t3600365921 * L_77 = Transform_GetChild_m1092972975(L_76, 0, /*hidden argument*/NULL);
		NullCheck(L_77);
		Transform_t3600365921 * L_78 = Transform_GetChild_m1092972975(L_77, 0, /*hidden argument*/NULL);
		NullCheck(L_78);
		int32_t L_79 = Transform_get_childCount_m3145433196(L_78, /*hidden argument*/NULL);
		if ((((int32_t)L_79) == ((int32_t)4)))
		{
			goto IL_02f6;
		}
	}
	{
		Transform_t3600365921 * L_80 = __this->get_transform2_11();
		NullCheck(L_80);
		int32_t L_81 = Transform_get_childCount_m3145433196(L_80, /*hidden argument*/NULL);
		if ((((int32_t)L_81) <= ((int32_t)0)))
		{
			goto IL_02f1;
		}
	}
	{
		Transform_t3600365921 * L_82 = __this->get_transform2_11();
		NullCheck(L_82);
		Transform_t3600365921 * L_83 = Transform_GetChild_m1092972975(L_82, 0, /*hidden argument*/NULL);
		NullCheck(L_83);
		Transform_t3600365921 * L_84 = Transform_GetChild_m1092972975(L_83, 0, /*hidden argument*/NULL);
		NullCheck(L_84);
		int32_t L_85 = Transform_get_childCount_m3145433196(L_84, /*hidden argument*/NULL);
		if ((((int32_t)L_85) == ((int32_t)4)))
		{
			goto IL_02d8;
		}
	}
	{
		return;
	}

IL_02d8:
	{
		Transform_t3600365921 * L_86 = __this->get_transform2_11();
		NullCheck(L_86);
		Transform_t3600365921 * L_87 = Transform_GetChild_m1092972975(L_86, 0, /*hidden argument*/NULL);
		NullCheck(L_87);
		Transform_t3600365921 * L_88 = Transform_GetChild_m1092972975(L_87, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_89 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_88, L_89, /*hidden argument*/NULL);
	}

IL_02f1:
	{
		goto IL_030f;
	}

IL_02f6:
	{
		Transform_t3600365921 * L_90 = __this->get_transform1_10();
		NullCheck(L_90);
		Transform_t3600365921 * L_91 = Transform_GetChild_m1092972975(L_90, 0, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_t3600365921 * L_92 = Transform_GetChild_m1092972975(L_91, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_93 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_92, L_93, /*hidden argument*/NULL);
	}

IL_030f:
	{
		goto IL_035c;
	}

IL_0314:
	{
		Transform_t3600365921 * L_94 = __this->get_transform2_11();
		NullCheck(L_94);
		int32_t L_95 = Transform_get_childCount_m3145433196(L_94, /*hidden argument*/NULL);
		if ((((int32_t)L_95) <= ((int32_t)0)))
		{
			goto IL_035c;
		}
	}
	{
		Transform_t3600365921 * L_96 = __this->get_transform2_11();
		NullCheck(L_96);
		Transform_t3600365921 * L_97 = Transform_GetChild_m1092972975(L_96, 0, /*hidden argument*/NULL);
		NullCheck(L_97);
		Transform_t3600365921 * L_98 = Transform_GetChild_m1092972975(L_97, 0, /*hidden argument*/NULL);
		NullCheck(L_98);
		int32_t L_99 = Transform_get_childCount_m3145433196(L_98, /*hidden argument*/NULL);
		if ((((int32_t)L_99) == ((int32_t)4)))
		{
			goto IL_0343;
		}
	}
	{
		return;
	}

IL_0343:
	{
		Transform_t3600365921 * L_100 = __this->get_transform2_11();
		NullCheck(L_100);
		Transform_t3600365921 * L_101 = Transform_GetChild_m1092972975(L_100, 0, /*hidden argument*/NULL);
		NullCheck(L_101);
		Transform_t3600365921 * L_102 = Transform_GetChild_m1092972975(L_101, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_103 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_102, L_103, /*hidden argument*/NULL);
	}

IL_035c:
	{
		goto IL_067e;
	}

IL_0361:
	{
		float L_104 = __this->get_min_19();
		float L_105 = __this->get_b_16();
		float L_106 = fabsf(((float)il2cpp_codegen_subtract((float)L_104, (float)L_105)));
		if ((!(((double)(((double)((double)L_106)))) < ((double)(0.01)))))
		{
			goto IL_04ef;
		}
	}
	{
		float L_107 = __this->get_b_16();
		if ((!(((double)(((double)((double)L_107)))) <= ((double)(0.4)))))
		{
			goto IL_03e7;
		}
	}
	{
		Transform_t3600365921 * L_108 = __this->get_transform1_10();
		NullCheck(L_108);
		int32_t L_109 = Transform_get_childCount_m3145433196(L_108, /*hidden argument*/NULL);
		if ((((int32_t)L_109) > ((int32_t)0)))
		{
			goto IL_03e1;
		}
	}
	{
		Spawn_t617419884 * L_110 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_111 = __this->get_transform1_10();
		NullCheck(L_110);
		Spawn_SpawnDroppedItem_m1584144246(L_110, L_111, 1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_112 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_113 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_112, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_1 = L_113;
		GameObject_t1113636619 * L_114 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_114, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_115 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		goto IL_03e2;
	}

IL_03e1:
	{
		return;
	}

IL_03e2:
	{
		goto IL_04ea;
	}

IL_03e7:
	{
		Transform_t3600365921 * L_116 = __this->get_transform1_10();
		NullCheck(L_116);
		int32_t L_117 = Transform_get_childCount_m3145433196(L_116, /*hidden argument*/NULL);
		if ((((int32_t)L_117) <= ((int32_t)0)))
		{
			goto IL_0491;
		}
	}
	{
		Transform_t3600365921 * L_118 = __this->get_transform1_10();
		NullCheck(L_118);
		Transform_t3600365921 * L_119 = Transform_GetChild_m1092972975(L_118, 0, /*hidden argument*/NULL);
		NullCheck(L_119);
		Transform_t3600365921 * L_120 = Transform_GetChild_m1092972975(L_119, 0, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = Transform_get_childCount_m3145433196(L_120, /*hidden argument*/NULL);
		if ((((int32_t)L_121) == ((int32_t)4)))
		{
			goto IL_0473;
		}
	}
	{
		float L_122 = __this->get_a_15();
		float L_123 = __this->get_c_17();
		if ((!(((float)L_122) < ((float)L_123))))
		{
			goto IL_046e;
		}
	}
	{
		Transform_t3600365921 * L_124 = __this->get_transform0_9();
		NullCheck(L_124);
		int32_t L_125 = Transform_get_childCount_m3145433196(L_124, /*hidden argument*/NULL);
		if ((((int32_t)L_125) <= ((int32_t)0)))
		{
			goto IL_046e;
		}
	}
	{
		Transform_t3600365921 * L_126 = __this->get_transform0_9();
		NullCheck(L_126);
		Transform_t3600365921 * L_127 = Transform_GetChild_m1092972975(L_126, 0, /*hidden argument*/NULL);
		NullCheck(L_127);
		Transform_t3600365921 * L_128 = Transform_GetChild_m1092972975(L_127, 0, /*hidden argument*/NULL);
		NullCheck(L_128);
		int32_t L_129 = Transform_get_childCount_m3145433196(L_128, /*hidden argument*/NULL);
		if ((((int32_t)L_129) == ((int32_t)4)))
		{
			goto IL_0455;
		}
	}
	{
		return;
	}

IL_0455:
	{
		Transform_t3600365921 * L_130 = __this->get_transform0_9();
		NullCheck(L_130);
		Transform_t3600365921 * L_131 = Transform_GetChild_m1092972975(L_130, 0, /*hidden argument*/NULL);
		NullCheck(L_131);
		Transform_t3600365921 * L_132 = Transform_GetChild_m1092972975(L_131, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_133 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_132, L_133, /*hidden argument*/NULL);
	}

IL_046e:
	{
		goto IL_048c;
	}

IL_0473:
	{
		Transform_t3600365921 * L_134 = __this->get_transform1_10();
		NullCheck(L_134);
		Transform_t3600365921 * L_135 = Transform_GetChild_m1092972975(L_134, 0, /*hidden argument*/NULL);
		NullCheck(L_135);
		Transform_t3600365921 * L_136 = Transform_GetChild_m1092972975(L_135, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_137 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_136, L_137, /*hidden argument*/NULL);
	}

IL_048c:
	{
		goto IL_04ea;
	}

IL_0491:
	{
		float L_138 = __this->get_a_15();
		float L_139 = __this->get_c_17();
		if ((!(((float)L_138) < ((float)L_139))))
		{
			goto IL_04ea;
		}
	}
	{
		Transform_t3600365921 * L_140 = __this->get_transform0_9();
		NullCheck(L_140);
		int32_t L_141 = Transform_get_childCount_m3145433196(L_140, /*hidden argument*/NULL);
		if ((((int32_t)L_141) <= ((int32_t)0)))
		{
			goto IL_04ea;
		}
	}
	{
		Transform_t3600365921 * L_142 = __this->get_transform0_9();
		NullCheck(L_142);
		Transform_t3600365921 * L_143 = Transform_GetChild_m1092972975(L_142, 0, /*hidden argument*/NULL);
		NullCheck(L_143);
		Transform_t3600365921 * L_144 = Transform_GetChild_m1092972975(L_143, 0, /*hidden argument*/NULL);
		NullCheck(L_144);
		int32_t L_145 = Transform_get_childCount_m3145433196(L_144, /*hidden argument*/NULL);
		if ((((int32_t)L_145) == ((int32_t)4)))
		{
			goto IL_04d1;
		}
	}
	{
		return;
	}

IL_04d1:
	{
		Transform_t3600365921 * L_146 = __this->get_transform0_9();
		NullCheck(L_146);
		Transform_t3600365921 * L_147 = Transform_GetChild_m1092972975(L_146, 0, /*hidden argument*/NULL);
		NullCheck(L_147);
		Transform_t3600365921 * L_148 = Transform_GetChild_m1092972975(L_147, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_149 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_148, L_149, /*hidden argument*/NULL);
	}

IL_04ea:
	{
		goto IL_067e;
	}

IL_04ef:
	{
		float L_150 = __this->get_min_19();
		float L_151 = __this->get_c_17();
		float L_152 = fabsf(((float)il2cpp_codegen_subtract((float)L_150, (float)L_151)));
		if ((!(((double)(((double)((double)L_152)))) < ((double)(0.01)))))
		{
			goto IL_067d;
		}
	}
	{
		float L_153 = __this->get_c_17();
		if ((!(((double)(((double)((double)L_153)))) <= ((double)(0.4)))))
		{
			goto IL_0575;
		}
	}
	{
		Transform_t3600365921 * L_154 = __this->get_transform2_11();
		NullCheck(L_154);
		int32_t L_155 = Transform_get_childCount_m3145433196(L_154, /*hidden argument*/NULL);
		if ((((int32_t)L_155) > ((int32_t)0)))
		{
			goto IL_056f;
		}
	}
	{
		Spawn_t617419884 * L_156 = Component_GetComponent_TisSpawn_t617419884_m3338007022(__this, /*hidden argument*/Component_GetComponent_TisSpawn_t617419884_m3338007022_RuntimeMethod_var);
		Transform_t3600365921 * L_157 = __this->get_transform2_11();
		NullCheck(L_156);
		Spawn_SpawnDroppedItem_m1584144246(L_156, L_157, 2, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_158 = __this->get_EquipSound_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_159 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_158, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_2 = L_159;
		GameObject_t1113636619 * L_160 = V_2;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_160, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_161 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_161, /*hidden argument*/NULL);
		goto IL_0570;
	}

IL_056f:
	{
		return;
	}

IL_0570:
	{
		goto IL_0678;
	}

IL_0575:
	{
		Transform_t3600365921 * L_162 = __this->get_transform2_11();
		NullCheck(L_162);
		int32_t L_163 = Transform_get_childCount_m3145433196(L_162, /*hidden argument*/NULL);
		if ((((int32_t)L_163) <= ((int32_t)0)))
		{
			goto IL_061f;
		}
	}
	{
		Transform_t3600365921 * L_164 = __this->get_transform2_11();
		NullCheck(L_164);
		Transform_t3600365921 * L_165 = Transform_GetChild_m1092972975(L_164, 0, /*hidden argument*/NULL);
		NullCheck(L_165);
		Transform_t3600365921 * L_166 = Transform_GetChild_m1092972975(L_165, 0, /*hidden argument*/NULL);
		NullCheck(L_166);
		int32_t L_167 = Transform_get_childCount_m3145433196(L_166, /*hidden argument*/NULL);
		if ((((int32_t)L_167) == ((int32_t)4)))
		{
			goto IL_0601;
		}
	}
	{
		float L_168 = __this->get_a_15();
		float L_169 = __this->get_b_16();
		if ((!(((float)L_168) < ((float)L_169))))
		{
			goto IL_05fc;
		}
	}
	{
		Transform_t3600365921 * L_170 = __this->get_transform0_9();
		NullCheck(L_170);
		int32_t L_171 = Transform_get_childCount_m3145433196(L_170, /*hidden argument*/NULL);
		if ((((int32_t)L_171) <= ((int32_t)0)))
		{
			goto IL_05fc;
		}
	}
	{
		Transform_t3600365921 * L_172 = __this->get_transform0_9();
		NullCheck(L_172);
		Transform_t3600365921 * L_173 = Transform_GetChild_m1092972975(L_172, 0, /*hidden argument*/NULL);
		NullCheck(L_173);
		Transform_t3600365921 * L_174 = Transform_GetChild_m1092972975(L_173, 0, /*hidden argument*/NULL);
		NullCheck(L_174);
		int32_t L_175 = Transform_get_childCount_m3145433196(L_174, /*hidden argument*/NULL);
		if ((((int32_t)L_175) == ((int32_t)4)))
		{
			goto IL_05e3;
		}
	}
	{
		return;
	}

IL_05e3:
	{
		Transform_t3600365921 * L_176 = __this->get_transform0_9();
		NullCheck(L_176);
		Transform_t3600365921 * L_177 = Transform_GetChild_m1092972975(L_176, 0, /*hidden argument*/NULL);
		NullCheck(L_177);
		Transform_t3600365921 * L_178 = Transform_GetChild_m1092972975(L_177, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_179 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_178, L_179, /*hidden argument*/NULL);
	}

IL_05fc:
	{
		goto IL_061a;
	}

IL_0601:
	{
		Transform_t3600365921 * L_180 = __this->get_transform2_11();
		NullCheck(L_180);
		Transform_t3600365921 * L_181 = Transform_GetChild_m1092972975(L_180, 0, /*hidden argument*/NULL);
		NullCheck(L_181);
		Transform_t3600365921 * L_182 = Transform_GetChild_m1092972975(L_181, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_183 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_182, L_183, /*hidden argument*/NULL);
	}

IL_061a:
	{
		goto IL_0678;
	}

IL_061f:
	{
		float L_184 = __this->get_a_15();
		float L_185 = __this->get_b_16();
		if ((!(((float)L_184) < ((float)L_185))))
		{
			goto IL_0678;
		}
	}
	{
		Transform_t3600365921 * L_186 = __this->get_transform0_9();
		NullCheck(L_186);
		int32_t L_187 = Transform_get_childCount_m3145433196(L_186, /*hidden argument*/NULL);
		if ((((int32_t)L_187) <= ((int32_t)0)))
		{
			goto IL_0678;
		}
	}
	{
		Transform_t3600365921 * L_188 = __this->get_transform0_9();
		NullCheck(L_188);
		Transform_t3600365921 * L_189 = Transform_GetChild_m1092972975(L_188, 0, /*hidden argument*/NULL);
		NullCheck(L_189);
		Transform_t3600365921 * L_190 = Transform_GetChild_m1092972975(L_189, 0, /*hidden argument*/NULL);
		NullCheck(L_190);
		int32_t L_191 = Transform_get_childCount_m3145433196(L_190, /*hidden argument*/NULL);
		if ((((int32_t)L_191) == ((int32_t)4)))
		{
			goto IL_065f;
		}
	}
	{
		return;
	}

IL_065f:
	{
		Transform_t3600365921 * L_192 = __this->get_transform0_9();
		NullCheck(L_192);
		Transform_t3600365921 * L_193 = Transform_GetChild_m1092972975(L_192, 0, /*hidden argument*/NULL);
		NullCheck(L_193);
		Transform_t3600365921 * L_194 = Transform_GetChild_m1092972975(L_193, 0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_195 = ___WorldPoint1;
		DragHandler_Recursive2_m1786376718(__this, L_194, L_195, /*hidden argument*/NULL);
	}

IL_0678:
	{
		goto IL_067e;
	}

IL_067d:
	{
		return;
	}

IL_067e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyMove::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnemyMove__ctor_m140768884 (EnemyMove_t2177327048 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyMove::Update()
extern "C" IL2CPP_METHOD_ATTR void EnemyMove_Update_m1644472081 (EnemyMove_t2177327048 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyMove_Update_m1644472081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = __this->get_maxSpeed_4();
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_1), (0.0f), ((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)), (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = V_0;
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = V_0;
		NullCheck(L_10);
		Transform_set_position_m3387557959(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyShooting::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnemyShooting__ctor_m240403928 (EnemyShooting_t2912033273 * __this, const RuntimeMethod* method)
{
	{
		__this->set_distance_7((6.0f));
		__this->set_cooldownTimer_8((0.3f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyShooting::Update()
extern "C" IL2CPP_METHOD_ATTR void EnemyShooting_Update_m2738480705 (EnemyShooting_t2912033273 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyShooting_Update_m2738480705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a3;
		}
	}
	{
		GameObject_t1113636619 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = GameObject_get_transform_m1369836730(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		__this->set_Distance_6(L_10);
		float L_11 = __this->get_Distance_6();
		float L_12 = __this->get_distance_7();
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_13 = __this->get_cooldownTimer_8();
		float L_14 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cooldownTimer_8(((float)il2cpp_codegen_subtract((float)L_13, (float)L_14)));
		float L_15 = __this->get_cooldownTimer_8();
		if ((!(((float)L_15) <= ((float)(0.0f)))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_16 = __this->get_fireDelay_5();
		__this->set_cooldownTimer_8(L_16);
		GameObject_t1113636619 * L_17 = __this->get_bulletPrefab_4();
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		Transform_t3600365921 * L_20 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Quaternion_t2301928331  L_21 = Transform_get_rotation_m3502953881(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_17, L_19, L_21, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
	}

IL_00a3:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemySpawner::.ctor()
extern "C" IL2CPP_METHOD_ATTR void EnemySpawner__ctor_m1783562082 (EnemySpawner_t2006493939 * __this, const RuntimeMethod* method)
{
	{
		__this->set_spawnDistance_5((3.0f));
		__this->set_nextEnemy_7((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemySpawner::Start()
extern "C" IL2CPP_METHOD_ATTR void EnemySpawner_Start_m1918682020 (EnemySpawner_t2006493939 * __this, const RuntimeMethod* method)
{
	{
		__this->set_timer_10((0.0f));
		__this->set_enemyRate_6((12.0f));
		return;
	}
}
// System.Void EnemySpawner::Update()
extern "C" IL2CPP_METHOD_ATTR void EnemySpawner_Update_m306438552 (EnemySpawner_t2006493939 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemySpawner_Update_m306438552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_nextEnemy_7();
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_nextEnemy_7(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		float L_2 = __this->get_timer_10();
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_10(((float)il2cpp_codegen_add((float)L_2, (float)L_3)));
		float L_4 = __this->get_nextEnemy_7();
		if ((!(((float)L_4) <= ((float)(0.0f)))))
		{
			goto IL_02f9;
		}
	}
	{
		float L_5 = __this->get_enemyRate_6();
		__this->set_nextEnemy_7(L_5);
		Vector3_t3722313464  L_6 = Random_get_onUnitSphere_m1840191398(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_offset_11(L_6);
		Vector3_t3722313464 * L_7 = __this->get_address_of_offset_11();
		L_7->set_z_4((0.0f));
		int32_t L_8 = Random_Range_m4054026115(NULL /*static, unused*/, 2, ((int32_t)12), /*hidden argument*/NULL);
		__this->set_n_8((((float)((float)L_8))));
		Vector3_t3722313464 * L_9 = __this->get_address_of_offset_11();
		Vector3_t3722313464  L_10 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_spawnDistance_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = __this->get_n_8();
		Vector3_t3722313464  L_14 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		__this->set_offset_11(L_14);
		float L_15 = __this->get_timer_10();
		if ((!(((float)L_15) < ((float)(180.0f)))))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_16 = Random_Range_m4054026115(NULL /*static, unused*/, 0, 6, /*hidden argument*/NULL);
		__this->set_i_9(L_16);
		goto IL_02cb;
	}

IL_00b3:
	{
		float L_17 = __this->get_timer_10();
		if ((!(((float)L_17) >= ((float)(180.0f)))))
		{
			goto IL_00e6;
		}
	}
	{
		float L_18 = __this->get_timer_10();
		if ((!(((float)L_18) < ((float)(360.0f)))))
		{
			goto IL_00e6;
		}
	}
	{
		int32_t L_19 = Random_Range_m4054026115(NULL /*static, unused*/, 6, ((int32_t)11), /*hidden argument*/NULL);
		__this->set_i_9(L_19);
		goto IL_02cb;
	}

IL_00e6:
	{
		float L_20 = __this->get_timer_10();
		if ((!(((float)L_20) >= ((float)(360.0f)))))
		{
			goto IL_011a;
		}
	}
	{
		float L_21 = __this->get_timer_10();
		if ((!(((float)L_21) < ((float)(540.0f)))))
		{
			goto IL_011a;
		}
	}
	{
		int32_t L_22 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)11), ((int32_t)16), /*hidden argument*/NULL);
		__this->set_i_9(L_22);
		goto IL_02cb;
	}

IL_011a:
	{
		float L_23 = __this->get_timer_10();
		if ((!(((float)L_23) >= ((float)(540.0f)))))
		{
			goto IL_014e;
		}
	}
	{
		float L_24 = __this->get_timer_10();
		if ((!(((float)L_24) < ((float)(720.0f)))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_25 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)16), ((int32_t)21), /*hidden argument*/NULL);
		__this->set_i_9(L_25);
		goto IL_02cb;
	}

IL_014e:
	{
		float L_26 = __this->get_timer_10();
		if ((!(((float)L_26) >= ((float)(720.0f)))))
		{
			goto IL_0182;
		}
	}
	{
		float L_27 = __this->get_timer_10();
		if ((!(((float)L_27) < ((float)(900.0f)))))
		{
			goto IL_0182;
		}
	}
	{
		int32_t L_28 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)21), ((int32_t)26), /*hidden argument*/NULL);
		__this->set_i_9(L_28);
		goto IL_02cb;
	}

IL_0182:
	{
		float L_29 = __this->get_timer_10();
		if ((!(((float)L_29) >= ((float)(900.0f)))))
		{
			goto IL_01b6;
		}
	}
	{
		float L_30 = __this->get_timer_10();
		if ((!(((float)L_30) < ((float)(1080.0f)))))
		{
			goto IL_01b6;
		}
	}
	{
		int32_t L_31 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)26), ((int32_t)31), /*hidden argument*/NULL);
		__this->set_i_9(L_31);
		goto IL_02cb;
	}

IL_01b6:
	{
		float L_32 = __this->get_timer_10();
		if ((!(((float)L_32) >= ((float)(1080.0f)))))
		{
			goto IL_01ea;
		}
	}
	{
		float L_33 = __this->get_timer_10();
		if ((!(((float)L_33) < ((float)(1260.0f)))))
		{
			goto IL_01ea;
		}
	}
	{
		int32_t L_34 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)31), ((int32_t)36), /*hidden argument*/NULL);
		__this->set_i_9(L_34);
		goto IL_02cb;
	}

IL_01ea:
	{
		float L_35 = __this->get_timer_10();
		if ((!(((float)L_35) >= ((float)(1260.0f)))))
		{
			goto IL_021e;
		}
	}
	{
		float L_36 = __this->get_timer_10();
		if ((!(((float)L_36) < ((float)(1440.0f)))))
		{
			goto IL_021e;
		}
	}
	{
		int32_t L_37 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)36), ((int32_t)41), /*hidden argument*/NULL);
		__this->set_i_9(L_37);
		goto IL_02cb;
	}

IL_021e:
	{
		float L_38 = __this->get_timer_10();
		if ((!(((float)L_38) >= ((float)(1440.0f)))))
		{
			goto IL_0252;
		}
	}
	{
		float L_39 = __this->get_timer_10();
		if ((!(((float)L_39) < ((float)(1620.0f)))))
		{
			goto IL_0252;
		}
	}
	{
		int32_t L_40 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)41), ((int32_t)46), /*hidden argument*/NULL);
		__this->set_i_9(L_40);
		goto IL_02cb;
	}

IL_0252:
	{
		float L_41 = __this->get_timer_10();
		if ((!(((float)L_41) >= ((float)(1620.0f)))))
		{
			goto IL_0286;
		}
	}
	{
		float L_42 = __this->get_timer_10();
		if ((!(((float)L_42) < ((float)(1800.0f)))))
		{
			goto IL_0286;
		}
	}
	{
		int32_t L_43 = Random_Range_m4054026115(NULL /*static, unused*/, ((int32_t)46), ((int32_t)52), /*hidden argument*/NULL);
		__this->set_i_9(L_43);
		goto IL_02cb;
	}

IL_0286:
	{
		float L_44 = __this->get_timer_10();
		if ((!(((float)L_44) >= ((float)(1800.0f)))))
		{
			goto IL_02cb;
		}
	}
	{
		GameObjectU5BU5D_t3328599146* L_45 = __this->get_enemyPrefab_4();
		NullCheck(L_45);
		int32_t L_46 = ((int32_t)53);
		GameObject_t1113636619 * L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		Transform_t3600365921 * L_48 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t3722313464  L_49 = Transform_get_position_m36019626(L_48, /*hidden argument*/NULL);
		Vector3_t3722313464  L_50 = __this->get_offset_11();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_51 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_52 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_47, L_51, L_52, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		GameObject_t1113636619 * L_53 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
	}

IL_02cb:
	{
		GameObjectU5BU5D_t3328599146* L_54 = __this->get_enemyPrefab_4();
		int32_t L_55 = __this->get_i_9();
		NullCheck(L_54);
		int32_t L_56 = L_55;
		GameObject_t1113636619 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		Transform_t3600365921 * L_58 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t3722313464  L_59 = Transform_get_position_m36019626(L_58, /*hidden argument*/NULL);
		Vector3_t3722313464  L_60 = __this->get_offset_11();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_61 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_62 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_57, L_61, L_62, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
	}

IL_02f9:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FacesEnemy::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FacesEnemy__ctor_m1348940205 (FacesEnemy_t493038826 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacesEnemy__ctor_m1348940205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_ttag_4(_stringLiteral760905195);
		__this->set_targetdistance_6((25.0f));
		__this->set_rotSpeed_9((90.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FacesEnemy::Start()
extern "C" IL2CPP_METHOD_ATTR void FacesEnemy_Start_m2796397225 (FacesEnemy_t493038826 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacesEnemy_Start_m2796397225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_InvokeRepeating_m650519629(__this, _stringLiteral3517658432, (0.0f), (0.5f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_FindWithTag_m981614592(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		__this->set_Player_8(L_0);
		GameObject_t1113636619 * L_1 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3956951295, /*hidden argument*/NULL);
		__this->set_finder_11(L_1);
		return;
	}
}
// System.Void FacesEnemy::Update()
extern "C" IL2CPP_METHOD_ATTR void FacesEnemy_Update_m577662581 (FacesEnemy_t493038826 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacesEnemy_Update_m577662581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t2301928331  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = __this->get_Player_8();
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3387557959(L_0, L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_target_5();
		__this->set_Enemy_10(L_4);
		Transform_t3600365921 * L_5 = __this->get_Enemy_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0045;
		}
	}
	{
		GameObject_t1113636619 * L_7 = __this->get_finder_11();
		NullCheck(L_7);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0045:
	{
		Transform_t3600365921 * L_8 = __this->get_Enemy_10();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_10 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		float L_14 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		if ((!(((float)L_14) > ((float)(7.0f)))))
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t1113636619 * L_15 = __this->get_finder_11();
		NullCheck(L_15);
		GameObject_SetActive_m796801857(L_15, (bool)1, /*hidden argument*/NULL);
		goto IL_0092;
	}

IL_0085:
	{
		GameObject_t1113636619 * L_16 = __this->get_finder_11();
		NullCheck(L_16);
		GameObject_SetActive_m796801857(L_16, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_0092:
	{
		Transform_t3600365921 * L_17 = __this->get_Enemy_10();
		NullCheck(L_17);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		Transform_t3600365921 * L_19 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		Vector3_Normalize_m914904454((Vector3_t3722313464 *)(&V_0), /*hidden argument*/NULL);
		float L_22 = (&V_0)->get_y_3();
		float L_23 = (&V_0)->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_24 = atan2f(L_22, L_23);
		V_1 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_24, (float)(57.29578f))), (float)(90.0f)));
		float L_25 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_26 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), (0.0f), L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Quaternion_t2301928331  L_29 = Transform_get_rotation_m3502953881(L_28, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_30 = V_2;
		float L_31 = __this->get_rotSpeed_9();
		float L_32 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_33 = Quaternion_RotateTowards_m3102912458(NULL /*static, unused*/, L_29, L_30, ((float)il2cpp_codegen_multiply((float)L_31, (float)L_32)), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_rotation_m3524318132(L_27, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FacesEnemy::GetClosestEnemy()
extern "C" IL2CPP_METHOD_ATTR void FacesEnemy_GetClosestEnemy_m4059716479 (FacesEnemy_t493038826 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacesEnemy_GetClosestEnemy_m4059716479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	float V_1 = 0.0f;
	Transform_t3600365921 * V_2 = NULL;
	GameObject_t1113636619 * V_3 = NULL;
	GameObjectU5BU5D_t3328599146* V_4 = NULL;
	int32_t V_5 = 0;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = __this->get_ttag_4();
		GameObjectU5BU5D_t3328599146* L_1 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (std::numeric_limits<float>::infinity());
		V_2 = (Transform_t3600365921 *)NULL;
		GameObjectU5BU5D_t3328599146* L_2 = V_0;
		V_4 = L_2;
		V_5 = 0;
		goto IL_0085;
	}

IL_001f:
	{
		GameObjectU5BU5D_t3328599146* L_3 = V_4;
		int32_t L_4 = V_5;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		GameObject_t1113636619 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		GameObject_t1113636619 * L_7 = V_3;
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		V_6 = L_9;
		Vector3_t3722313464  L_10 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		__this->set_dist_7(L_15);
		float L_16 = __this->get_dist_7();
		float L_17 = __this->get_targetdistance_6();
		if ((!(((float)L_16) < ((float)L_17))))
		{
			goto IL_007f;
		}
	}
	{
		float L_18 = __this->get_dist_7();
		float L_19 = V_1;
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_007f;
		}
	}
	{
		float L_20 = __this->get_dist_7();
		V_1 = L_20;
		GameObject_t1113636619 * L_21 = V_3;
		NullCheck(L_21);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
	}

IL_007f:
	{
		int32_t L_23 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0085:
	{
		int32_t L_24 = V_5;
		GameObjectU5BU5D_t3328599146* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		Transform_t3600365921 * L_26 = V_2;
		__this->set_target_5(L_26);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FixedJoystick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FixedJoystick__ctor_m3165509026 (FixedJoystick_t2618381211 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixedJoystick__ctor_m3165509026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_joystickPosition_9(L_0);
		Camera_t4157153871 * L_1 = (Camera_t4157153871 *)il2cpp_codegen_object_new(Camera_t4157153871_il2cpp_TypeInfo_var);
		Camera__ctor_m741555041(L_1, /*hidden argument*/NULL);
		__this->set_cam_10(L_1);
		Joystick__ctor_m1272315817(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixedJoystick::Start()
extern "C" IL2CPP_METHOD_ATTR void FixedJoystick_Start_m649713002 (FixedJoystick_t2618381211 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixedJoystick_Start_m649713002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = __this->get_cam_10();
		RectTransform_t3704657025 * L_1 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = RectTransformUtility_WorldToScreenPoint_m3914148572(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		__this->set_joystickPosition_9(L_3);
		return;
	}
}
// System.Void FixedJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FixedJoystick_OnDrag_m1042755941 (FixedJoystick_t2618381211 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixedJoystick_OnDrag_m1042755941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FixedJoystick_t2618381211 * G_B2_0 = NULL;
	FixedJoystick_t2618381211 * G_B1_0 = NULL;
	Vector2_t2156229523  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	FixedJoystick_t2618381211 * G_B3_1 = NULL;
	{
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = PointerEventData_get_position_m437660275(L_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = __this->get_joystickPosition_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_5);
		Vector2_t2156229523  L_6 = RectTransform_get_sizeDelta_m2183112744(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_0();
		G_B1_0 = __this;
		if ((!(((float)L_4) > ((float)((float)((float)L_7/(float)(2.0f)))))))
		{
			G_B2_0 = __this;
			goto IL_0044;
		}
	}
	{
		Vector2_t2156229523  L_8 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		goto IL_0063;
	}

IL_0044:
	{
		Vector2_t2156229523  L_9 = V_0;
		RectTransform_t3704657025 * L_10 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_10);
		Vector2_t2156229523  L_11 = RectTransform_get_sizeDelta_m2183112744(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_13 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_9, ((float)((float)L_12/(float)(2.0f))), /*hidden argument*/NULL);
		G_B3_0 = L_13;
		G_B3_1 = G_B2_0;
	}

IL_0063:
	{
		NullCheck(G_B3_1);
		((Joystick_t9498292 *)G_B3_1)->set_inputVector_6(G_B3_0);
		Joystick_ClampJoystick_m2501349361(__this, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_15 = ((Joystick_t9498292 *)__this)->get_inputVector_6();
		RectTransform_t3704657025 * L_16 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_16);
		Vector2_t2156229523  L_17 = RectTransform_get_sizeDelta_m2183112744(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_19 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_19, (2.0f), /*hidden argument*/NULL);
		float L_21 = ((Joystick_t9498292 *)__this)->get_handleLimit_4();
		Vector2_t2156229523  L_22 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchoredPosition_m4126691837(L_14, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FixedJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FixedJoystick_OnPointerDown_m3601786111 (FixedJoystick_t2618381211 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		VirtActionInvoker1< PointerEventData_t3807901092 * >::Invoke(7 /* System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_0);
		return;
	}
}
// System.Void FixedJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FixedJoystick_OnPointerUp_m763078753 (FixedJoystick_t2618381211 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FixedJoystick_OnPointerUp_m763078753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Joystick_t9498292 *)__this)->set_inputVector_6(L_0);
		RectTransform_t3704657025 * L_1 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		RectTransform_set_anchoredPosition_m4126691837(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FloatingJoystick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FloatingJoystick__ctor_m612783692 (FloatingJoystick_t3402721920 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FloatingJoystick__ctor_m612783692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_joystickCenter_9(L_0);
		Joystick__ctor_m1272315817(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FloatingJoystick::Start()
extern "C" IL2CPP_METHOD_ATTR void FloatingJoystick_Start_m290384880 (FloatingJoystick_t3402721920 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FloatingJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FloatingJoystick_OnDrag_m1849813291 (FloatingJoystick_t3402721920 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FloatingJoystick_OnDrag_m1849813291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FloatingJoystick_t3402721920 * G_B2_0 = NULL;
	FloatingJoystick_t3402721920 * G_B1_0 = NULL;
	Vector2_t2156229523  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	FloatingJoystick_t3402721920 * G_B3_1 = NULL;
	{
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = PointerEventData_get_position_m437660275(L_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = __this->get_joystickCenter_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_5);
		Vector2_t2156229523  L_6 = RectTransform_get_sizeDelta_m2183112744(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_0();
		G_B1_0 = __this;
		if ((!(((float)L_4) > ((float)((float)((float)L_7/(float)(2.0f)))))))
		{
			G_B2_0 = __this;
			goto IL_0044;
		}
	}
	{
		Vector2_t2156229523  L_8 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		goto IL_0063;
	}

IL_0044:
	{
		Vector2_t2156229523  L_9 = V_0;
		RectTransform_t3704657025 * L_10 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_10);
		Vector2_t2156229523  L_11 = RectTransform_get_sizeDelta_m2183112744(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_13 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_9, ((float)((float)L_12/(float)(2.0f))), /*hidden argument*/NULL);
		G_B3_0 = L_13;
		G_B3_1 = G_B2_0;
	}

IL_0063:
	{
		NullCheck(G_B3_1);
		((Joystick_t9498292 *)G_B3_1)->set_inputVector_6(G_B3_0);
		Joystick_ClampJoystick_m2501349361(__this, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_15 = ((Joystick_t9498292 *)__this)->get_inputVector_6();
		RectTransform_t3704657025 * L_16 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_16);
		Vector2_t2156229523  L_17 = RectTransform_get_sizeDelta_m2183112744(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_19 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_19, (2.0f), /*hidden argument*/NULL);
		float L_21 = ((Joystick_t9498292 *)__this)->get_handleLimit_4();
		Vector2_t2156229523  L_22 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchoredPosition_m4126691837(L_14, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FloatingJoystick_OnPointerDown_m3753834403 (FloatingJoystick_t3402721920 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FloatingJoystick_OnPointerDown_m3753834403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_2 = ((Joystick_t9498292 *)__this)->get_background_7();
		PointerEventData_t3807901092 * L_3 = ___eventData0;
		NullCheck(L_3);
		Vector2_t2156229523  L_4 = PointerEventData_get_position_m437660275(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3387557959(L_2, L_5, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_6 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_7 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectTransform_set_anchoredPosition_m4126691837(L_6, L_7, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_8 = ___eventData0;
		NullCheck(L_8);
		Vector2_t2156229523  L_9 = PointerEventData_get_position_m437660275(L_8, /*hidden argument*/NULL);
		__this->set_joystickCenter_9(L_9);
		return;
	}
}
// System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void FloatingJoystick_OnPointerUp_m2581807680 (FloatingJoystick_t3402721920 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FloatingJoystick_OnPointerUp_m2581807680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Joystick_t9498292 *)__this)->set_inputVector_6(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FollowPlayer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FollowPlayer__ctor_m1808261062 (FollowPlayer_t2788059413 * __this, const RuntimeMethod* method)
{
	{
		__this->set_rotSpeed_4((90.0f));
		__this->set_maxSpeed_5((2.0f));
		__this->set_disM_8((7.0f));
		__this->set_dism_9((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowPlayer::Update()
extern "C" IL2CPP_METHOD_ATTR void FollowPlayer_Update_m1580389478 (FollowPlayer_t2788059413 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowPlayer_Update_m1580389478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		__this->set_go_13(L_0);
		GameObject_t1113636619 * L_1 = __this->get_go_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0115;
		}
	}
	{
		GameObject_t1113636619 * L_3 = __this->get_go_13();
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = GameObject_get_transform_m1369836730(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_6 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		float L_10 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		__this->set_Distance_7(L_10);
		float L_11 = __this->get_Distance_7();
		float L_12 = __this->get_disM_8();
		if ((!(((float)L_11) < ((float)L_12))))
		{
			goto IL_00f3;
		}
	}
	{
		float L_13 = __this->get_Distance_7();
		float L_14 = __this->get_dism_9();
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_00f3;
		}
	}
	{
		GameObject_t1113636619 * L_15 = __this->get_go_13();
		NullCheck(L_15);
		Transform_t3600365921 * L_16 = GameObject_get_transform_m1369836730(L_15, /*hidden argument*/NULL);
		__this->set_player_6(L_16);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_18 = Transform_get_position_m36019626(L_17, /*hidden argument*/NULL);
		__this->set_pos_11(L_18);
		float L_19 = __this->get_maxSpeed_5();
		float L_20 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m3353183577((&L_21), (0.0f), ((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)), (0.0f), /*hidden argument*/NULL);
		__this->set_velocity_12(L_21);
		Vector3_t3722313464  L_22 = __this->get_pos_11();
		Transform_t3600365921 * L_23 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t2301928331  L_24 = Transform_get_rotation_m3502953881(L_23, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25 = __this->get_velocity_12();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_26 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_27 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_22, L_26, /*hidden argument*/NULL);
		__this->set_pos_11(L_27);
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = __this->get_pos_11();
		NullCheck(L_28);
		Transform_set_position_m3387557959(L_28, L_29, /*hidden argument*/NULL);
		goto IL_0110;
	}

IL_00f3:
	{
		float L_30 = __this->get_Distance_7();
		if ((!(((float)L_30) <= ((float)(1.0f)))))
		{
			goto IL_0108;
		}
	}
	{
		goto IL_0110;
	}

IL_0108:
	{
		__this->set_player_6((Transform_t3600365921 *)NULL);
		return;
	}

IL_0110:
	{
		goto IL_011d;
	}

IL_0115:
	{
		__this->set_player_6((Transform_t3600365921 *)NULL);
		return;
	}

IL_011d:
	{
		Transform_t3600365921 * L_31 = __this->get_player_6();
		NullCheck(L_31);
		Vector3_t3722313464  L_32 = Transform_get_position_m36019626(L_31, /*hidden argument*/NULL);
		Transform_t3600365921 * L_33 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t3722313464  L_34 = Transform_get_position_m36019626(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_35 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_36 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		__this->set_dir_14(L_36);
		Vector2_t2156229523 * L_37 = __this->get_address_of_dir_14();
		Vector2_Normalize_m1906922873((Vector2_t2156229523 *)L_37, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_38 = __this->get_address_of_dir_14();
		float L_39 = L_38->get_y_1();
		Vector2_t2156229523 * L_40 = __this->get_address_of_dir_14();
		float L_41 = L_40->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_42 = atan2f(L_39, L_41);
		__this->set_zAngle_10(((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_42, (float)(57.29578f))), (float)(90.0f))));
		float L_43 = __this->get_zAngle_10();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_44 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), (0.0f), L_43, /*hidden argument*/NULL);
		__this->set_desiredRot_15(L_44);
		Transform_t3600365921 * L_45 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_46 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Quaternion_t2301928331  L_47 = Transform_get_rotation_m3502953881(L_46, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_48 = __this->get_desiredRot_15();
		float L_49 = __this->get_rotSpeed_4();
		float L_50 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_51 = Quaternion_RotateTowards_m3102912458(NULL /*static, unused*/, L_47, L_48, ((float)il2cpp_codegen_multiply((float)L_49, (float)L_50)), /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_set_rotation_m3524318132(L_45, L_51, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Inventory::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Inventory__ctor_m912618405 (Inventory_t1050226016 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void JoyStick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void JoyStick__ctor_m3471657305 (JoyStick_t2364599988 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoyStick::Start()
extern "C" IL2CPP_METHOD_ATTR void JoyStick_Start_m50952947 (JoyStick_t2364599988 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_Start_m50952947_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_bgImg_4(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = Transform_GetChild_m1092972975(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		Image_t2670269651 * L_3 = Component_GetComponent_TisImage_t2670269651_m980647750(L_2, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_joystickImg_5(L_3);
		return;
	}
}
// System.Void JoyStick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void JoyStick_OnDrag_m884893215 (JoyStick_t2364599988 * __this, PointerEventData_t3807901092 * ___ped0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_OnDrag_m884893215_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	JoyStick_t2364599988 * G_B3_0 = NULL;
	JoyStick_t2364599988 * G_B2_0 = NULL;
	Vector3_t3722313464  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	JoyStick_t2364599988 * G_B4_1 = NULL;
	{
		Image_t2670269651 * L_0 = __this->get_bgImg_4();
		NullCheck(L_0);
		RectTransform_t3704657025 * L_1 = Graphic_get_rectTransform_m1167152468(L_0, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_2 = ___ped0;
		NullCheck(L_2);
		Vector2_t2156229523  L_3 = PointerEventData_get_position_m437660275(L_2, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_4 = ___ped0;
		NullCheck(L_4);
		Camera_t4157153871 * L_5 = PointerEventData_get_pressEventCamera_m2613974917(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_6 = RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, L_1, L_3, L_5, (Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0147;
		}
	}
	{
		float L_7 = (&V_0)->get_x_0();
		Image_t2670269651 * L_8 = __this->get_bgImg_4();
		NullCheck(L_8);
		RectTransform_t3704657025 * L_9 = Graphic_get_rectTransform_m1167152468(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t2156229523  L_10 = RectTransform_get_sizeDelta_m2183112744(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_0();
		(&V_0)->set_x_0(((float)((float)L_7/(float)L_11)));
		float L_12 = (&V_0)->get_y_1();
		Image_t2670269651 * L_13 = __this->get_bgImg_4();
		NullCheck(L_13);
		RectTransform_t3704657025 * L_14 = Graphic_get_rectTransform_m1167152468(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector2_t2156229523  L_15 = RectTransform_get_sizeDelta_m2183112744(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = (&V_2)->get_y_1();
		(&V_0)->set_y_1(((float)((float)L_12/(float)L_16)));
		float L_17 = (&V_0)->get_x_0();
		float L_18 = (&V_0)->get_y_1();
		Vector3_t3722313464  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m3353183577((&L_19), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_17, (float)(2.0f))), (float)(1.0f))), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_18, (float)(2.0f))), (float)(1.0f))), (0.0f), /*hidden argument*/NULL);
		__this->set_inputVector_6(L_19);
		Vector3_t3722313464 * L_20 = __this->get_address_of_inputVector_6();
		float L_21 = Vector3_get_magnitude_m27958459((Vector3_t3722313464 *)L_20, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if ((!(((float)L_21) > ((float)(1.0f)))))
		{
			G_B3_0 = __this;
			goto IL_00cd;
		}
	}
	{
		Vector3_t3722313464 * L_22 = __this->get_address_of_inputVector_6();
		Vector3_t3722313464  L_23 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)L_22, /*hidden argument*/NULL);
		G_B4_0 = L_23;
		G_B4_1 = G_B2_0;
		goto IL_00d3;
	}

IL_00cd:
	{
		Vector3_t3722313464  L_24 = __this->get_inputVector_6();
		G_B4_0 = L_24;
		G_B4_1 = G_B3_0;
	}

IL_00d3:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_inputVector_6(G_B4_0);
		Image_t2670269651 * L_25 = __this->get_joystickImg_5();
		NullCheck(L_25);
		RectTransform_t3704657025 * L_26 = Graphic_get_rectTransform_m1167152468(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464 * L_27 = __this->get_address_of_inputVector_6();
		float L_28 = L_27->get_x_2();
		Image_t2670269651 * L_29 = __this->get_bgImg_4();
		NullCheck(L_29);
		RectTransform_t3704657025 * L_30 = Graphic_get_rectTransform_m1167152468(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector2_t2156229523  L_31 = RectTransform_get_sizeDelta_m2183112744(L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		float L_32 = (&V_3)->get_x_0();
		Vector3_t3722313464 * L_33 = __this->get_address_of_inputVector_6();
		float L_34 = L_33->get_y_3();
		Image_t2670269651 * L_35 = __this->get_bgImg_4();
		NullCheck(L_35);
		RectTransform_t3704657025 * L_36 = Graphic_get_rectTransform_m1167152468(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector2_t2156229523  L_37 = RectTransform_get_sizeDelta_m2183112744(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		float L_38 = (&V_4)->get_y_1();
		Vector3_t3722313464  L_39;
		memset(&L_39, 0, sizeof(L_39));
		Vector3__ctor_m1719387948((&L_39), ((float)il2cpp_codegen_multiply((float)L_28, (float)((float)((float)L_32/(float)(3.0f))))), ((float)il2cpp_codegen_multiply((float)L_34, (float)((float)((float)L_38/(float)(3.0f))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_40 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchoredPosition_m4126691837(L_26, L_40, /*hidden argument*/NULL);
	}

IL_0147:
	{
		return;
	}
}
// System.Void JoyStick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void JoyStick_OnPointerDown_m2446534182 (JoyStick_t2364599988 * __this, PointerEventData_t3807901092 * ___ped0, const RuntimeMethod* method)
{
	{
		PointerEventData_t3807901092 * L_0 = ___ped0;
		VirtActionInvoker1< PointerEventData_t3807901092 * >::Invoke(7 /* System.Void JoyStick::OnDrag(UnityEngine.EventSystems.PointerEventData) */, __this, L_0);
		return;
	}
}
// System.Void JoyStick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void JoyStick_OnPointerUp_m2954618842 (JoyStick_t2364599988 * __this, PointerEventData_t3807901092 * ___ped0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_OnPointerUp_m2954618842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputVector_6(L_0);
		Image_t2670269651 * L_1 = __this->get_joystickImg_5();
		NullCheck(L_1);
		RectTransform_t3704657025 * L_2 = Graphic_get_rectTransform_m1167152468(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectTransform_set_anchoredPosition_m4126691837(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Single JoyStick::GetHorizontalValue()
extern "C" IL2CPP_METHOD_ATTR float JoyStick_GetHorizontalValue_m882128001 (JoyStick_t2364599988 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464 * L_0 = __this->get_address_of_inputVector_6();
		float L_1 = L_0->get_x_2();
		return L_1;
	}
}
// System.Single JoyStick::GetVerticalValue()
extern "C" IL2CPP_METHOD_ATTR float JoyStick_GetVerticalValue_m220030021 (JoyStick_t2364599988 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464 * L_0 = __this->get_address_of_inputVector_6();
		float L_1 = L_0->get_y_3();
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Joystick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Joystick__ctor_m1272315817 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__ctor_m1272315817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_handleLimit_4((1.0f));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_inputVector_6(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Joystick::get_Horizontal()
extern "C" IL2CPP_METHOD_ATTR float Joystick_get_Horizontal_m3614099306 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523 * L_0 = __this->get_address_of_inputVector_6();
		float L_1 = L_0->get_x_0();
		return L_1;
	}
}
// System.Single Joystick::get_Vertical()
extern "C" IL2CPP_METHOD_ATTR float Joystick_get_Vertical_m3105283820 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523 * L_0 = __this->get_address_of_inputVector_6();
		float L_1 = L_0->get_y_1();
		return L_1;
	}
}
// UnityEngine.Vector2 Joystick::get_Direction()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Joystick_get_Direction_m3832432503 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Joystick_get_Horizontal_m3614099306(__this, /*hidden argument*/NULL);
		float L_1 = Joystick_get_Vertical_m3105283820(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3970636864((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void Joystick_OnDrag_m1548353041 (Joystick_t9498292 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void Joystick_OnPointerDown_m1243099850 (Joystick_t9498292 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void Joystick_OnPointerUp_m3173419772 (Joystick_t9498292 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Joystick::ClampJoystick()
extern "C" IL2CPP_METHOD_ATTR void Joystick_ClampJoystick_m2501349361 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_joystickMode_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0027;
		}
	}
	{
		Vector2_t2156229523 * L_1 = __this->get_address_of_inputVector_6();
		float L_2 = L_1->get_x_0();
		Vector2_t2156229523  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m3970636864((&L_3), L_2, (0.0f), /*hidden argument*/NULL);
		__this->set_inputVector_6(L_3);
	}

IL_0027:
	{
		int32_t L_4 = __this->get_joystickMode_5();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_004e;
		}
	}
	{
		Vector2_t2156229523 * L_5 = __this->get_address_of_inputVector_6();
		float L_6 = L_5->get_y_1();
		Vector2_t2156229523  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m3970636864((&L_7), (0.0f), L_6, /*hidden argument*/NULL);
		__this->set_inputVector_6(L_7);
	}

IL_004e:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainMenu__ctor_m2617117415 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::PlayGame()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_PlayGame_m1630219755 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t2348375561  L_0 = SceneManager_GetActiveScene_m1825203488(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m270272723((Scene_t2348375561 *)(&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::QuitGame()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_QuitGame_m2024773731 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveForward::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MoveForward__ctor_m3766805889 (MoveForward_t2564727450 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoveForward::Start()
extern "C" IL2CPP_METHOD_ATTR void MoveForward_Start_m632200807 (MoveForward_t2564727450 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		__this->set_pos_5(L_1);
		float L_2 = __this->get_maxSpeed_4();
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (0.0f), ((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)), (0.0f), /*hidden argument*/NULL);
		__this->set_velocity_6(L_4);
		return;
	}
}
// System.Void MoveForward::Update()
extern "C" IL2CPP_METHOD_ATTR void MoveForward_Update_m1581178352 (MoveForward_t2564727450 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MoveForward_Update_m1581178352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = __this->get_pos_5();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t2301928331  L_2 = Transform_get_rotation_m3502953881(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = __this->get_velocity_6();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_4, /*hidden argument*/NULL);
		__this->set_pos_5(L_5);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = __this->get_pos_5();
		NullCheck(L_6);
		Transform_set_position_m3387557959(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PauseMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu__ctor_m3854158124 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenu::Start()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Start_m517218302 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_Start_m517218302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3326048179, /*hidden argument*/NULL);
		__this->set_ButtonSound_8(L_0);
		return;
	}
}
// System.Void PauseMenu::Paused()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Paused_m2854264695 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_Paused_m2854264695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		bool L_0 = ((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->get_GameIsPaused_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		PauseMenu_Resume_m690786508(__this, /*hidden argument*/NULL);
		goto IL_001b;
	}

IL_0015:
	{
		PauseMenu_Pause_m890285182(__this, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void PauseMenu::Resume()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Resume_m690786508 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_Resume_m690786508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_ButtonSound_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		GameObject_t1113636619 * L_1 = __this->get_pauseButton_7();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_pauseMenuUI_5();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_playScreen_6();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_GameIsPaused_4((bool)0);
		return;
	}
}
// System.Void PauseMenu::Pause()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_Pause_m890285182 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_Pause_m890285182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_ButtonSound_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		GameObject_t1113636619 * L_1 = __this->get_pauseButton_7();
		NullCheck(L_1);
		GameObject_SetActive_m796801857(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = __this->get_playScreen_6();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = __this->get_pauseMenuUI_5();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_GameIsPaused_4((bool)1);
		return;
	}
}
// System.Void PauseMenu::LoadMenu()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_LoadMenu_m3831042958 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_LoadMenu_m3831042958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_ButtonSound_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		Time_set_timeScale_m1127545364(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_GameIsPaused_4((bool)0);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, _stringLiteral1555075383, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenu::QuitGame()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu_QuitGame_m3587716712 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_QuitGame_m3587716712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = __this->get_ButtonSound_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		Application_Quit_m470877999(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenu::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PauseMenu__cctor_m1544404692 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pickup::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Pickup__ctor_m4184042691 (Pickup_t999226966 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pickup::Start()
extern "C" IL2CPP_METHOD_ATTR void Pickup_Start_m928129965 (Pickup_t999226966 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pickup_Start_m928129965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		NullCheck(L_0);
		Inventory_t1050226016 * L_1 = GameObject_GetComponent_TisInventory_t1050226016_m3648803814(L_0, /*hidden argument*/GameObject_GetComponent_TisInventory_t1050226016_m3648803814_RuntimeMethod_var);
		__this->set_inventory_4(L_1);
		return;
	}
}
// System.Void Pickup::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Pickup_OnTriggerEnter2D_m3957925105 (Pickup_t999226966 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pickup_OnTriggerEnter2D_m3957925105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1113636619 * V_1 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___other0;
		NullCheck(L_0);
		bool L_1 = Component_CompareTag_m1328479619(L_0, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0094;
		}
	}
	{
		V_0 = 0;
		goto IL_0081;
	}

IL_0017:
	{
		Inventory_t1050226016 * L_2 = __this->get_inventory_4();
		NullCheck(L_2);
		BooleanU5BU5D_t2897418192* L_3 = L_2->get_isFull_4();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		if (L_6)
		{
			goto IL_007d;
		}
	}
	{
		Inventory_t1050226016 * L_7 = __this->get_inventory_4();
		NullCheck(L_7);
		BooleanU5BU5D_t2897418192* L_8 = L_7->get_isFull_4();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (bool)1);
		GameObject_t1113636619 * L_10 = __this->get_pickup_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_11 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_1 = L_11;
		GameObject_t1113636619 * L_12 = __this->get_itemButton_5();
		Inventory_t1050226016 * L_13 = __this->get_inventory_4();
		NullCheck(L_13);
		GameObjectU5BU5D_t3328599146* L_14 = L_13->get_slots_5();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		GameObject_t1113636619 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m4130575780(NULL /*static, unused*/, L_12, L_18, (bool)0, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4130575780_RuntimeMethod_var);
		GameObject_t1113636619 * L_19 = V_1;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_19, (1.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_20 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_0094;
	}

IL_007d:
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0081:
	{
		int32_t L_22 = V_0;
		Inventory_t1050226016 * L_23 = __this->get_inventory_4();
		NullCheck(L_23);
		GameObjectU5BU5D_t3328599146* L_24 = L_23->get_slots_5();
		NullCheck(L_24);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))))))
		{
			goto IL_0017;
		}
	}

IL_0094:
	{
		return;
	}
}
// System.Void Pickup::Update()
extern "C" IL2CPP_METHOD_ATTR void Pickup_Update_m1745370817 (Pickup_t999226966 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerController::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerController__ctor_m1333951952 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Update()
extern "C" IL2CPP_METHOD_ATTR void PlayerController_Update_m848427540 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Update_m848427540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		FloatingJoystick_t3402721920 * L_1 = __this->get_joystick_4();
		NullCheck(L_1);
		float L_2 = Joystick_get_Horizontal_m3614099306(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		FloatingJoystick_t3402721920 * L_5 = __this->get_joystick_4();
		NullCheck(L_5);
		float L_6 = Joystick_get_Vertical_m3105283820(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector3_t3722313464  L_9 = V_0;
		Vector3_t3722313464  L_10 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_11 = Vector3_op_Inequality_m315980366(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_13 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_15 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_rotation_m3524318132(L_12, L_15, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerMovement::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerMovement__ctor_m3994561284 (PlayerMovement_t2731566919 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerMovement::Update()
extern "C" IL2CPP_METHOD_ATTR void PlayerMovement_Update_m1995258020 (PlayerMovement_t2731566919 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerMovement_Update_m1995258020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		__this->set_h_5(L_0);
		float L_1 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		__this->set_v_6(L_1);
		return;
	}
}
// System.Void PlayerMovement::FixedUpdate()
extern "C" IL2CPP_METHOD_ATTR void PlayerMovement_FixedUpdate_m2788581829 (PlayerMovement_t2731566919 * __this, const RuntimeMethod* method)
{
	{
		PlayerMovement_Move_m167645979(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerMovement::Move()
extern "C" IL2CPP_METHOD_ATTR void PlayerMovement_Move_m167645979 (PlayerMovement_t2731566919 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464 * L_2 = (&V_0);
		float L_3 = L_2->get_y_3();
		float L_4 = __this->get_v_6();
		float L_5 = __this->get_MoveSpeed_4();
		float L_6 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_2->set_y_3(((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), (float)L_6)))));
		Vector3_t3722313464 * L_7 = (&V_0);
		float L_8 = L_7->get_x_2();
		float L_9 = __this->get_h_5();
		float L_10 = __this->get_MoveSpeed_4();
		float L_11 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_7->set_x_2(((float)il2cpp_codegen_add((float)L_8, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), (float)L_11)))));
		float L_12 = (&V_0)->get_y_3();
		if ((!(((float)L_12) > ((float)(40.0f)))))
		{
			goto IL_0070;
		}
	}
	{
		(&V_0)->set_y_3((40.0f));
		goto IL_0092;
	}

IL_0070:
	{
		float L_13 = (&V_0)->get_y_3();
		if ((!(((float)L_13) < ((float)(-40.0f)))))
		{
			goto IL_0092;
		}
	}
	{
		(&V_0)->set_y_3((-40.0f));
		goto IL_0092;
	}

IL_0092:
	{
		float L_14 = (&V_0)->get_x_2();
		if ((!(((float)L_14) > ((float)(40.0f)))))
		{
			goto IL_00b4;
		}
	}
	{
		(&V_0)->set_x_2((40.0f));
		goto IL_00d6;
	}

IL_00b4:
	{
		float L_15 = (&V_0)->get_x_2();
		if ((!(((float)L_15) < ((float)(-40.0f)))))
		{
			goto IL_00d6;
		}
	}
	{
		(&V_0)->set_x_2((-40.0f));
		goto IL_00d6;
	}

IL_00d6:
	{
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = V_0;
		NullCheck(L_16);
		Transform_set_position_m3387557959(L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerShooting::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerShooting__ctor_m2527024264 (PlayerShooting_t3504758017 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShooting__ctor_m2527024264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_ttag_6(_stringLiteral760905195);
		__this->set_targetdistance_8((5.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShooting::Start()
extern "C" IL2CPP_METHOD_ATTR void PlayerShooting_Start_m3760994824 (PlayerShooting_t3504758017 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShooting_Start_m3760994824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_InvokeRepeating_m650519629(__this, _stringLiteral3517658432, (0.0f), (0.5f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2590551002, /*hidden argument*/NULL);
		__this->set_shootSound_4(L_0);
		return;
	}
}
// System.Void PlayerShooting::Update()
extern "C" IL2CPP_METHOD_ATTR void PlayerShooting_Update_m411263274 (PlayerShooting_t3504758017 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShooting_Update_m411263274_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = __this->get_target_7();
		__this->set_Enemy_12(L_0);
		Transform_t3600365921 * L_1 = __this->get_Enemy_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		Transform_t3600365921 * L_3 = __this->get_target_7();
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		float L_9 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		__this->set_Distance_11(L_9);
		float L_10 = __this->get_Distance_11();
		if ((!(((float)L_10) < ((float)(5.0f)))))
		{
			goto IL_00d6;
		}
	}
	{
		float L_11 = __this->get_cooldownTimer_13();
		float L_12 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cooldownTimer_13(((float)il2cpp_codegen_subtract((float)L_11, (float)L_12)));
		float L_13 = __this->get_cooldownTimer_13();
		if ((!(((float)L_13) <= ((float)(0.0f)))))
		{
			goto IL_00d6;
		}
	}
	{
		float L_14 = __this->get_fireDelay_10();
		__this->set_cooldownTimer_13(L_14);
		GameObject_t1113636619 * L_15 = __this->get_shootSound_4();
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t3722313464  L_17 = Transform_get_position_m36019626(L_16, /*hidden argument*/NULL);
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t2301928331  L_19 = Transform_get_rotation_m3502953881(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_20 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_15, L_17, L_19, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_0 = L_20;
		GameObject_t1113636619 * L_21 = __this->get_bulletPrefab_5();
		Transform_t3600365921 * L_22 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_23 = Transform_get_position_m36019626(L_22, /*hidden argument*/NULL);
		Transform_t3600365921 * L_24 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Quaternion_t2301928331  L_25 = Transform_get_rotation_m3502953881(L_24, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_21, L_23, L_25, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		GameObject_t1113636619 * L_26 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_26, (1.0f), /*hidden argument*/NULL);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void PlayerShooting::GetClosestEnemy()
extern "C" IL2CPP_METHOD_ATTR void PlayerShooting_GetClosestEnemy_m980920295 (PlayerShooting_t3504758017 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShooting_GetClosestEnemy_m980920295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	float V_1 = 0.0f;
	Transform_t3600365921 * V_2 = NULL;
	GameObject_t1113636619 * V_3 = NULL;
	GameObjectU5BU5D_t3328599146* V_4 = NULL;
	int32_t V_5 = 0;
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		String_t* L_0 = __this->get_ttag_6();
		GameObjectU5BU5D_t3328599146* L_1 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = (std::numeric_limits<float>::infinity());
		V_2 = (Transform_t3600365921 *)NULL;
		GameObjectU5BU5D_t3328599146* L_2 = V_0;
		V_4 = L_2;
		V_5 = 0;
		goto IL_0085;
	}

IL_001f:
	{
		GameObjectU5BU5D_t3328599146* L_3 = V_4;
		int32_t L_4 = V_5;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		GameObject_t1113636619 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_3 = L_6;
		GameObject_t1113636619 * L_7 = V_3;
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		V_6 = L_9;
		Vector3_t3722313464  L_10 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		__this->set_dist_9(L_15);
		float L_16 = __this->get_dist_9();
		float L_17 = __this->get_targetdistance_8();
		if ((!(((float)L_16) < ((float)L_17))))
		{
			goto IL_007f;
		}
	}
	{
		float L_18 = __this->get_dist_9();
		float L_19 = V_1;
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_007f;
		}
	}
	{
		float L_20 = __this->get_dist_9();
		V_1 = L_20;
		GameObject_t1113636619 * L_21 = V_3;
		NullCheck(L_21);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		V_2 = L_22;
	}

IL_007f:
	{
		int32_t L_23 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0085:
	{
		int32_t L_24 = V_5;
		GameObjectU5BU5D_t3328599146* L_25 = V_4;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_25)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		Transform_t3600365921 * L_26 = V_2;
		__this->set_target_7(L_26);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SecondSlot::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SecondSlot__ctor_m40498293 (SecondSlot_t2904157877 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject SecondSlot::get_Item()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * SecondSlot_get_Item_m1488879554 (SecondSlot_t2904157877 * __this, const RuntimeMethod* method)
{
	GameObject_t1113636619 * G_B3_0 = NULL;
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m3145433196(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Transform_GetChild_m1092972975(L_2, 0, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = ((GameObject_t1113636619 *)(NULL));
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// System.Void SecondSlot::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void SecondSlot_OnDrop_m1888909481 (SecondSlot_t2904157877 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SecondSlot_OnDrop_m1888909481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = SecondSlot_get_Item_m1488879554(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		GameObject_t1113636619 * L_2 = ((DragHandler_t4187724030_StaticFields*)il2cpp_codegen_static_fields_for(DragHandler_t4187724030_il2cpp_TypeInfo_var))->get_itemBeingDragged_4();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_SetParent_m381167889(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SelfDestruct::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SelfDestruct__ctor_m3408981355 (SelfDestruct_t1181332799 * __this, const RuntimeMethod* method)
{
	{
		__this->set_timer_4((1.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SelfDestruct::Start()
extern "C" IL2CPP_METHOD_ATTR void SelfDestruct_Start_m1129399379 (SelfDestruct_t1181332799 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void SelfDestruct::Update()
extern "C" IL2CPP_METHOD_ATTR void SelfDestruct_Update_m3543763843 (SelfDestruct_t1181332799 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelfDestruct_Update_m3543763843_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_timer_4();
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_4(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		float L_2 = __this->get_timer_4();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Slot::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Slot__ctor_m1538157551 (Slot_t3333057830 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Slot::Start()
extern "C" IL2CPP_METHOD_ATTR void Slot_Start_m2837351715 (Slot_t3333057830 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Slot_Start_m2837351715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		NullCheck(L_0);
		Inventory_t1050226016 * L_1 = GameObject_GetComponent_TisInventory_t1050226016_m3648803814(L_0, /*hidden argument*/GameObject_GetComponent_TisInventory_t1050226016_m3648803814_RuntimeMethod_var);
		__this->set_inventory_4(L_1);
		GameObject_t1113636619 * L_2 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1536896214, /*hidden argument*/NULL);
		__this->set_deleteSound_6(L_2);
		return;
	}
}
// System.Void Slot::Update()
extern "C" IL2CPP_METHOD_ATTR void Slot_Update_m3174921985 (Slot_t3333057830 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m3145433196(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0024;
		}
	}
	{
		Inventory_t1050226016 * L_2 = __this->get_inventory_4();
		NullCheck(L_2);
		BooleanU5BU5D_t2897418192* L_3 = L_2->get_isFull_4();
		int32_t L_4 = __this->get_i_5();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (bool)0);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Slot::DropItem()
extern "C" IL2CPP_METHOD_ATTR void Slot_DropItem_m733037294 (Slot_t3333057830 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Slot_DropItem_m733037294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RuntimeObject* L_1 = Transform_GetEnumerator_m2717073726(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003f;
		}

IL_0011:
		{
			RuntimeObject* L_2 = V_1;
			NullCheck(L_2);
			RuntimeObject * L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_3, Transform_t3600365921_il2cpp_TypeInfo_var));
			GameObject_t1113636619 * L_4 = __this->get_deleteSound_6();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			GameObject_t1113636619 * L_5 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
			V_2 = L_5;
			GameObject_t1113636619 * L_6 = V_2;
			Object_Destroy_m3118546832(NULL /*static, unused*/, L_6, (1.0f), /*hidden argument*/NULL);
			Transform_t3600365921 * L_7 = V_0;
			NullCheck(L_7);
			GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
			Object_Destroy_m565254235(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		}

IL_003f:
		{
			RuntimeObject* L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x63, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_11 = V_1;
			RuntimeObject* L_12 = ((RuntimeObject*)IsInst((RuntimeObject*)L_11, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_3 = L_12;
			if (!L_12)
			{
				goto IL_0062;
			}
		}

IL_005c:
		{
			RuntimeObject* L_13 = V_3;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_13);
		}

IL_0062:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x63, IL_0063)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0063:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spawn::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Spawn__ctor_m1877167295 (Spawn_t617419884 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spawn::SpawnDroppedItem(UnityEngine.Transform,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Spawn_SpawnDroppedItem_m1584144246 (Spawn_t617419884 * __this, Transform_t3600365921 * ___trans0, int32_t ___i1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Spawn_SpawnDroppedItem_m1584144246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i1;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_007b;
			}
			case 2:
			{
				goto IL_00db;
			}
			case 3:
			{
				goto IL_013b;
			}
		}
	}
	{
		goto IL_019b;
	}

IL_001b:
	{
		GameObject_t1113636619 * L_1 = __this->get_item_4();
		Transform_t3600365921 * L_2 = ___trans0;
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = ___trans0;
		NullCheck(L_4);
		Quaternion_t2301928331  L_5 = Transform_get_rotation_m3502953881(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_6 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_3, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_6);
		Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = ___trans0;
		NullCheck(L_7);
		Transform_SetParent_m381167889(L_7, L_8, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_ThreeDirection_5();
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		GameObject_t1113636619 * L_11 = __this->get_ThreeDirection_5();
		Transform_t3600365921 * L_12 = ___trans0;
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = ___trans0;
		NullCheck(L_14);
		Quaternion_t2301928331  L_15 = Transform_get_rotation_m3502953881(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_16 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_11, L_13, L_15, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		Transform_t3600365921 * L_18 = ___trans0;
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = Transform_GetChild_m1092972975(L_18, 0, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_SetParent_m381167889(L_17, L_19, /*hidden argument*/NULL);
	}

IL_0076:
	{
		goto IL_019b;
	}

IL_007b:
	{
		GameObject_t1113636619 * L_20 = __this->get_item_4();
		Transform_t3600365921 * L_21 = ___trans0;
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		Transform_t3600365921 * L_23 = ___trans0;
		NullCheck(L_23);
		Quaternion_t2301928331  L_24 = Transform_get_rotation_m3502953881(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_25 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_20, L_22, L_24, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = GameObject_get_transform_m1369836730(L_25, /*hidden argument*/NULL);
		Transform_t3600365921 * L_27 = ___trans0;
		NullCheck(L_26);
		Transform_SetParent_m381167889(L_26, L_27, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_28 = __this->get_ThreeDirection_5();
		bool L_29 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_28, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00d6;
		}
	}
	{
		GameObject_t1113636619 * L_30 = __this->get_ThreeDirection_5();
		Transform_t3600365921 * L_31 = ___trans0;
		NullCheck(L_31);
		Vector3_t3722313464  L_32 = Transform_get_position_m36019626(L_31, /*hidden argument*/NULL);
		Transform_t3600365921 * L_33 = ___trans0;
		NullCheck(L_33);
		Quaternion_t2301928331  L_34 = Transform_get_rotation_m3502953881(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_35 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_30, L_32, L_34, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_35);
		Transform_t3600365921 * L_36 = GameObject_get_transform_m1369836730(L_35, /*hidden argument*/NULL);
		Transform_t3600365921 * L_37 = ___trans0;
		NullCheck(L_37);
		Transform_t3600365921 * L_38 = Transform_GetChild_m1092972975(L_37, 0, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_SetParent_m381167889(L_36, L_38, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		goto IL_019b;
	}

IL_00db:
	{
		GameObject_t1113636619 * L_39 = __this->get_item_4();
		Transform_t3600365921 * L_40 = ___trans0;
		NullCheck(L_40);
		Vector3_t3722313464  L_41 = Transform_get_position_m36019626(L_40, /*hidden argument*/NULL);
		Transform_t3600365921 * L_42 = ___trans0;
		NullCheck(L_42);
		Quaternion_t2301928331  L_43 = Transform_get_rotation_m3502953881(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_44 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_39, L_41, L_43, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_44);
		Transform_t3600365921 * L_45 = GameObject_get_transform_m1369836730(L_44, /*hidden argument*/NULL);
		Transform_t3600365921 * L_46 = ___trans0;
		NullCheck(L_45);
		Transform_SetParent_m381167889(L_45, L_46, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_47 = __this->get_ThreeDirection_5();
		bool L_48 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_47, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0136;
		}
	}
	{
		GameObject_t1113636619 * L_49 = __this->get_ThreeDirection_5();
		Transform_t3600365921 * L_50 = ___trans0;
		NullCheck(L_50);
		Vector3_t3722313464  L_51 = Transform_get_position_m36019626(L_50, /*hidden argument*/NULL);
		Transform_t3600365921 * L_52 = ___trans0;
		NullCheck(L_52);
		Quaternion_t2301928331  L_53 = Transform_get_rotation_m3502953881(L_52, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_54 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_49, L_51, L_53, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_54);
		Transform_t3600365921 * L_55 = GameObject_get_transform_m1369836730(L_54, /*hidden argument*/NULL);
		Transform_t3600365921 * L_56 = ___trans0;
		NullCheck(L_56);
		Transform_t3600365921 * L_57 = Transform_GetChild_m1092972975(L_56, 0, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_SetParent_m381167889(L_55, L_57, /*hidden argument*/NULL);
	}

IL_0136:
	{
		goto IL_019b;
	}

IL_013b:
	{
		GameObject_t1113636619 * L_58 = __this->get_item_4();
		Transform_t3600365921 * L_59 = ___trans0;
		NullCheck(L_59);
		Vector3_t3722313464  L_60 = Transform_get_position_m36019626(L_59, /*hidden argument*/NULL);
		Transform_t3600365921 * L_61 = ___trans0;
		NullCheck(L_61);
		Quaternion_t2301928331  L_62 = Transform_get_rotation_m3502953881(L_61, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_63 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_58, L_60, L_62, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_63);
		Transform_t3600365921 * L_64 = GameObject_get_transform_m1369836730(L_63, /*hidden argument*/NULL);
		Transform_t3600365921 * L_65 = ___trans0;
		NullCheck(L_64);
		Transform_SetParent_m381167889(L_64, L_65, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_66 = __this->get_ThreeDirection_5();
		bool L_67 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_66, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0196;
		}
	}
	{
		GameObject_t1113636619 * L_68 = __this->get_ThreeDirection_5();
		Transform_t3600365921 * L_69 = ___trans0;
		NullCheck(L_69);
		Vector3_t3722313464  L_70 = Transform_get_position_m36019626(L_69, /*hidden argument*/NULL);
		Transform_t3600365921 * L_71 = ___trans0;
		NullCheck(L_71);
		Quaternion_t2301928331  L_72 = Transform_get_rotation_m3502953881(L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_73 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_68, L_70, L_72, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		NullCheck(L_73);
		Transform_t3600365921 * L_74 = GameObject_get_transform_m1369836730(L_73, /*hidden argument*/NULL);
		Transform_t3600365921 * L_75 = ___trans0;
		NullCheck(L_75);
		Transform_t3600365921 * L_76 = Transform_GetChild_m1092972975(L_75, 0, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_SetParent_m381167889(L_74, L_76, /*hidden argument*/NULL);
	}

IL_0196:
	{
		goto IL_019b;
	}

IL_019b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TouchEvent__ctor_m2904000897 (TouchEvent_t1418210080 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchEvent::Start()
extern "C" IL2CPP_METHOD_ATTR void TouchEvent_Start_m3344650181 (TouchEvent_t1418210080 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cam_5(L_0);
		return;
	}
}
// System.Void TouchEvent::Update()
extern "C" IL2CPP_METHOD_ATTR void TouchEvent_Update_m3259198139 (TouchEvent_t1418210080 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchEvent_Update_m3259198139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Touch_t1921856868  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1113636619 * V_2 = NULL;
	{
		bool L_0 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009e;
		}
	}
	{
		Camera_t4157153871 * L_2 = __this->get_cam_5();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_4 = Camera_ScreenToWorldPoint_m3978588570(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_WorldPoint_4(L_5);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = __this->get_WorldPoint_4();
		float L_10 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_10)))) < ((double)(0.3)))))
		{
			goto IL_009e;
		}
	}
	{
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = __this->get_SpawnItem_6();
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3722313464  L_14 = Transform_get_position_m36019626(L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Quaternion_t2301928331  L_16 = Transform_get_rotation_m3502953881(L_15, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_12, L_14, L_16, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		GameObject_t1113636619 * L_17 = __this->get_UnequipSound_7();
		GameObject_t1113636619 * L_18 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_0 = L_18;
		GameObject_t1113636619 * L_19 = V_0;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_19, (1.0f), /*hidden argument*/NULL);
	}

IL_009e:
	{
		goto IL_0146;
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_20 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = Touch_get_phase_m214549210((Touch_t1921856868 *)(&V_1), /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_0146;
		}
	}
	{
		Camera_t4157153871 * L_22 = __this->get_cam_5();
		Vector2_t2156229523  L_23 = Touch_get_position_m3109777936((Touch_t1921856868 *)(&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_24 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t3722313464  L_25 = Camera_ScreenToWorldPoint_m3978588570(L_22, L_24, /*hidden argument*/NULL);
		Vector2_t2156229523  L_26 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		__this->set_WorldPoint_4(L_26);
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t3722313464  L_28 = Transform_get_position_m36019626(L_27, /*hidden argument*/NULL);
		Vector2_t2156229523  L_29 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Vector2_t2156229523  L_30 = __this->get_WorldPoint_4();
		float L_31 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_31)))) < ((double)(0.3)))))
		{
			goto IL_0146;
		}
	}
	{
		GameObject_t1113636619 * L_32 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_33 = __this->get_SpawnItem_6();
		Transform_t3600365921 * L_34 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t3722313464  L_35 = Transform_get_position_m36019626(L_34, /*hidden argument*/NULL);
		Transform_t3600365921 * L_36 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_36);
		Quaternion_t2301928331  L_37 = Transform_get_rotation_m3502953881(L_36, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_33, L_35, L_37, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		GameObject_t1113636619 * L_38 = __this->get_UnequipSound_7();
		GameObject_t1113636619 * L_39 = Object_Instantiate_TisGameObject_t1113636619_m4070250708(NULL /*static, unused*/, L_38, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m4070250708_RuntimeMethod_var);
		V_2 = L_39;
		GameObject_t1113636619 * L_40 = V_2;
		Object_Destroy_m3118546832(NULL /*static, unused*/, L_40, (1.0f), /*hidden argument*/NULL);
	}

IL_0146:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void VariableJoystick::.ctor()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick__ctor_m1597116632 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick__ctor_m1597116632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isFixed_9((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_joystickCenter_11(L_0);
		Joystick__ctor_m1272315817(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariableJoystick::Start()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_Start_m900724618 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isFixed_9();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		VariableJoystick_OnFixed_m4141959043(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		VariableJoystick_OnFloat_m165815926(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void VariableJoystick::ChangeFixed(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_ChangeFixed_m3111545193 (VariableJoystick_t2643911586 * __this, bool ___joystickFixed0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___joystickFixed0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VariableJoystick_OnFixed_m4141959043(__this, /*hidden argument*/NULL);
		goto IL_0017;
	}

IL_0011:
	{
		VariableJoystick_OnFloat_m165815926(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		bool L_1 = ___joystickFixed0;
		__this->set_isFixed_9(L_1);
		return;
	}
}
// System.Void VariableJoystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnDrag_m3080315221 (VariableJoystick_t2643911586 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick_OnDrag_m3080315221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	VariableJoystick_t2643911586 * G_B2_0 = NULL;
	VariableJoystick_t2643911586 * G_B1_0 = NULL;
	Vector2_t2156229523  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	VariableJoystick_t2643911586 * G_B3_1 = NULL;
	{
		PointerEventData_t3807901092 * L_0 = ___eventData0;
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = PointerEventData_get_position_m437660275(L_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = __this->get_joystickCenter_11();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Vector2_get_magnitude_m2752892833((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_5);
		Vector2_t2156229523  L_6 = RectTransform_get_sizeDelta_m2183112744(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_x_0();
		G_B1_0 = __this;
		if ((!(((float)L_4) > ((float)((float)((float)L_7/(float)(2.0f)))))))
		{
			G_B2_0 = __this;
			goto IL_0044;
		}
	}
	{
		Vector2_t2156229523  L_8 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_8;
		G_B3_1 = G_B1_0;
		goto IL_0063;
	}

IL_0044:
	{
		Vector2_t2156229523  L_9 = V_0;
		RectTransform_t3704657025 * L_10 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_10);
		Vector2_t2156229523  L_11 = RectTransform_get_sizeDelta_m2183112744(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_13 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_9, ((float)((float)L_12/(float)(2.0f))), /*hidden argument*/NULL);
		G_B3_0 = L_13;
		G_B3_1 = G_B2_0;
	}

IL_0063:
	{
		NullCheck(G_B3_1);
		((Joystick_t9498292 *)G_B3_1)->set_inputVector_6(G_B3_0);
		Joystick_ClampJoystick_m2501349361(__this, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_15 = ((Joystick_t9498292 *)__this)->get_inputVector_6();
		RectTransform_t3704657025 * L_16 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_16);
		Vector2_t2156229523  L_17 = RectTransform_get_sizeDelta_m2183112744(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = (&V_3)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_19 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_19, (2.0f), /*hidden argument*/NULL);
		float L_21 = ((Joystick_t9498292 *)__this)->get_handleLimit_4();
		Vector2_t2156229523  L_22 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		NullCheck(L_14);
		RectTransform_set_anchoredPosition_m4126691837(L_14, L_22, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnPointerDown_m4235218688 (VariableJoystick_t2643911586 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick_OnPointerDown_m4235218688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isFixed_9();
		if (L_0)
		{
			goto IL_004e;
		}
	}
	{
		RectTransform_t3704657025 * L_1 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = ((Joystick_t9498292 *)__this)->get_background_7();
		PointerEventData_t3807901092 * L_4 = ___eventData0;
		NullCheck(L_4);
		Vector2_t2156229523  L_5 = PointerEventData_get_position_m437660275(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m3387557959(L_3, L_6, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_7 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_8 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_set_anchoredPosition_m4126691837(L_7, L_8, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_9 = ___eventData0;
		NullCheck(L_9);
		Vector2_t2156229523  L_10 = PointerEventData_get_position_m437660275(L_9, /*hidden argument*/NULL);
		__this->set_joystickCenter_11(L_10);
	}

IL_004e:
	{
		return;
	}
}
// System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnPointerUp_m3792644295 (VariableJoystick_t2643911586 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick_OnPointerUp_m3792644295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isFixed_9();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		RectTransform_t3704657025 * L_1 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_3 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Joystick_t9498292 *)__this)->set_inputVector_6(L_3);
		RectTransform_t3704657025 * L_4 = ((Joystick_t9498292 *)__this)->get_handle_8();
		Vector2_t2156229523  L_5 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectTransform_set_anchoredPosition_m4126691837(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariableJoystick::OnFixed()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnFixed_m4141959043 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick_OnFixed_m4141959043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2156229523  L_0 = __this->get_fixedScreenPosition_10();
		__this->set_joystickCenter_11(L_0);
		RectTransform_t3704657025 * L_1 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = ((Joystick_t9498292 *)__this)->get_handle_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectTransform_set_anchoredPosition_m4126691837(L_3, L_4, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_5 = ((Joystick_t9498292 *)__this)->get_background_7();
		Vector2_t2156229523  L_6 = __this->get_fixedScreenPosition_10();
		NullCheck(L_5);
		RectTransform_set_anchoredPosition_m4126691837(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VariableJoystick::OnFloat()
extern "C" IL2CPP_METHOD_ATTR void VariableJoystick_OnFloat_m165815926 (VariableJoystick_t2643911586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VariableJoystick_OnFloat_m165815926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = ((Joystick_t9498292 *)__this)->get_handle_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m4126691837(L_0, L_1, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_2 = ((Joystick_t9498292 *)__this)->get_background_7();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
