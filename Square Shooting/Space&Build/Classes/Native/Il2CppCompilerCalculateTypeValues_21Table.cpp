﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// FloatingJoystick
struct FloatingJoystick_t3402721920;
// Inventory
struct Inventory_t1050226016;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef JOYSTICKMODE_T3732604888_H
#define JOYSTICKMODE_T3732604888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickMode
struct  JoystickMode_t3732604888 
{
public:
	// System.Int32 JoystickMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JoystickMode_t3732604888, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMODE_T3732604888_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef JOYSTICK_T9498292_H
#define JOYSTICK_T9498292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t9498292  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Joystick::handleLimit
	float ___handleLimit_4;
	// JoystickMode Joystick::joystickMode
	int32_t ___joystickMode_5;
	// UnityEngine.Vector2 Joystick::inputVector
	Vector2_t2156229523  ___inputVector_6;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t3704657025 * ___background_7;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t3704657025 * ___handle_8;

public:
	inline static int32_t get_offset_of_handleLimit_4() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___handleLimit_4)); }
	inline float get_handleLimit_4() const { return ___handleLimit_4; }
	inline float* get_address_of_handleLimit_4() { return &___handleLimit_4; }
	inline void set_handleLimit_4(float value)
	{
		___handleLimit_4 = value;
	}

	inline static int32_t get_offset_of_joystickMode_5() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___joystickMode_5)); }
	inline int32_t get_joystickMode_5() const { return ___joystickMode_5; }
	inline int32_t* get_address_of_joystickMode_5() { return &___joystickMode_5; }
	inline void set_joystickMode_5(int32_t value)
	{
		___joystickMode_5 = value;
	}

	inline static int32_t get_offset_of_inputVector_6() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___inputVector_6)); }
	inline Vector2_t2156229523  get_inputVector_6() const { return ___inputVector_6; }
	inline Vector2_t2156229523 * get_address_of_inputVector_6() { return &___inputVector_6; }
	inline void set_inputVector_6(Vector2_t2156229523  value)
	{
		___inputVector_6 = value;
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___background_7)); }
	inline RectTransform_t3704657025 * get_background_7() const { return ___background_7; }
	inline RectTransform_t3704657025 ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(RectTransform_t3704657025 * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((&___background_7), value);
	}

	inline static int32_t get_offset_of_handle_8() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___handle_8)); }
	inline RectTransform_t3704657025 * get_handle_8() const { return ___handle_8; }
	inline RectTransform_t3704657025 ** get_address_of_handle_8() { return &___handle_8; }
	inline void set_handle_8(RectTransform_t3704657025 * value)
	{
		___handle_8 = value;
		Il2CppCodeGenWriteBarrier((&___handle_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T9498292_H
#ifndef PICKUP_T999226966_H
#define PICKUP_T999226966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pickup
struct  Pickup_t999226966  : public MonoBehaviour_t3962482529
{
public:
	// Inventory Pickup::inventory
	Inventory_t1050226016 * ___inventory_4;
	// UnityEngine.GameObject Pickup::itemButton
	GameObject_t1113636619 * ___itemButton_5;
	// UnityEngine.GameObject Pickup::pickup
	GameObject_t1113636619 * ___pickup_6;

public:
	inline static int32_t get_offset_of_inventory_4() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___inventory_4)); }
	inline Inventory_t1050226016 * get_inventory_4() const { return ___inventory_4; }
	inline Inventory_t1050226016 ** get_address_of_inventory_4() { return &___inventory_4; }
	inline void set_inventory_4(Inventory_t1050226016 * value)
	{
		___inventory_4 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_4), value);
	}

	inline static int32_t get_offset_of_itemButton_5() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___itemButton_5)); }
	inline GameObject_t1113636619 * get_itemButton_5() const { return ___itemButton_5; }
	inline GameObject_t1113636619 ** get_address_of_itemButton_5() { return &___itemButton_5; }
	inline void set_itemButton_5(GameObject_t1113636619 * value)
	{
		___itemButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___itemButton_5), value);
	}

	inline static int32_t get_offset_of_pickup_6() { return static_cast<int32_t>(offsetof(Pickup_t999226966, ___pickup_6)); }
	inline GameObject_t1113636619 * get_pickup_6() const { return ___pickup_6; }
	inline GameObject_t1113636619 ** get_address_of_pickup_6() { return &___pickup_6; }
	inline void set_pickup_6(GameObject_t1113636619 * value)
	{
		___pickup_6 = value;
		Il2CppCodeGenWriteBarrier((&___pickup_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUP_T999226966_H
#ifndef PLAYERCONTROLLER_T2064355688_H
#define PLAYERCONTROLLER_T2064355688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t2064355688  : public MonoBehaviour_t3962482529
{
public:
	// FloatingJoystick PlayerController::joystick
	FloatingJoystick_t3402721920 * ___joystick_4;

public:
	inline static int32_t get_offset_of_joystick_4() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___joystick_4)); }
	inline FloatingJoystick_t3402721920 * get_joystick_4() const { return ___joystick_4; }
	inline FloatingJoystick_t3402721920 ** get_address_of_joystick_4() { return &___joystick_4; }
	inline void set_joystick_4(FloatingJoystick_t3402721920 * value)
	{
		___joystick_4 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2064355688_H
#ifndef PLAYERMOVEMENT_T2731566919_H
#define PLAYERMOVEMENT_T2731566919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMovement
struct  PlayerMovement_t2731566919  : public MonoBehaviour_t3962482529
{
public:
	// System.Single PlayerMovement::MoveSpeed
	float ___MoveSpeed_4;
	// System.Single PlayerMovement::h
	float ___h_5;
	// System.Single PlayerMovement::v
	float ___v_6;

public:
	inline static int32_t get_offset_of_MoveSpeed_4() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___MoveSpeed_4)); }
	inline float get_MoveSpeed_4() const { return ___MoveSpeed_4; }
	inline float* get_address_of_MoveSpeed_4() { return &___MoveSpeed_4; }
	inline void set_MoveSpeed_4(float value)
	{
		___MoveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_h_5() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___h_5)); }
	inline float get_h_5() const { return ___h_5; }
	inline float* get_address_of_h_5() { return &___h_5; }
	inline void set_h_5(float value)
	{
		___h_5 = value;
	}

	inline static int32_t get_offset_of_v_6() { return static_cast<int32_t>(offsetof(PlayerMovement_t2731566919, ___v_6)); }
	inline float get_v_6() const { return ___v_6; }
	inline float* get_address_of_v_6() { return &___v_6; }
	inline void set_v_6(float value)
	{
		___v_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMOVEMENT_T2731566919_H
#ifndef PLAYERSHOOTING_T3504758017_H
#define PLAYERSHOOTING_T3504758017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerShooting
struct  PlayerShooting_t3504758017  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerShooting::shootSound
	GameObject_t1113636619 * ___shootSound_4;
	// UnityEngine.GameObject PlayerShooting::bulletPrefab
	GameObject_t1113636619 * ___bulletPrefab_5;
	// System.String PlayerShooting::ttag
	String_t* ___ttag_6;
	// UnityEngine.Transform PlayerShooting::target
	Transform_t3600365921 * ___target_7;
	// System.Single PlayerShooting::targetdistance
	float ___targetdistance_8;
	// System.Single PlayerShooting::dist
	float ___dist_9;
	// System.Single PlayerShooting::fireDelay
	float ___fireDelay_10;
	// System.Single PlayerShooting::Distance
	float ___Distance_11;
	// UnityEngine.Transform PlayerShooting::Enemy
	Transform_t3600365921 * ___Enemy_12;
	// System.Single PlayerShooting::cooldownTimer
	float ___cooldownTimer_13;

public:
	inline static int32_t get_offset_of_shootSound_4() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___shootSound_4)); }
	inline GameObject_t1113636619 * get_shootSound_4() const { return ___shootSound_4; }
	inline GameObject_t1113636619 ** get_address_of_shootSound_4() { return &___shootSound_4; }
	inline void set_shootSound_4(GameObject_t1113636619 * value)
	{
		___shootSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___shootSound_4), value);
	}

	inline static int32_t get_offset_of_bulletPrefab_5() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___bulletPrefab_5)); }
	inline GameObject_t1113636619 * get_bulletPrefab_5() const { return ___bulletPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_bulletPrefab_5() { return &___bulletPrefab_5; }
	inline void set_bulletPrefab_5(GameObject_t1113636619 * value)
	{
		___bulletPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_5), value);
	}

	inline static int32_t get_offset_of_ttag_6() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___ttag_6)); }
	inline String_t* get_ttag_6() const { return ___ttag_6; }
	inline String_t** get_address_of_ttag_6() { return &___ttag_6; }
	inline void set_ttag_6(String_t* value)
	{
		___ttag_6 = value;
		Il2CppCodeGenWriteBarrier((&___ttag_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___target_7)); }
	inline Transform_t3600365921 * get_target_7() const { return ___target_7; }
	inline Transform_t3600365921 ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(Transform_t3600365921 * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_targetdistance_8() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___targetdistance_8)); }
	inline float get_targetdistance_8() const { return ___targetdistance_8; }
	inline float* get_address_of_targetdistance_8() { return &___targetdistance_8; }
	inline void set_targetdistance_8(float value)
	{
		___targetdistance_8 = value;
	}

	inline static int32_t get_offset_of_dist_9() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___dist_9)); }
	inline float get_dist_9() const { return ___dist_9; }
	inline float* get_address_of_dist_9() { return &___dist_9; }
	inline void set_dist_9(float value)
	{
		___dist_9 = value;
	}

	inline static int32_t get_offset_of_fireDelay_10() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___fireDelay_10)); }
	inline float get_fireDelay_10() const { return ___fireDelay_10; }
	inline float* get_address_of_fireDelay_10() { return &___fireDelay_10; }
	inline void set_fireDelay_10(float value)
	{
		___fireDelay_10 = value;
	}

	inline static int32_t get_offset_of_Distance_11() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___Distance_11)); }
	inline float get_Distance_11() const { return ___Distance_11; }
	inline float* get_address_of_Distance_11() { return &___Distance_11; }
	inline void set_Distance_11(float value)
	{
		___Distance_11 = value;
	}

	inline static int32_t get_offset_of_Enemy_12() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___Enemy_12)); }
	inline Transform_t3600365921 * get_Enemy_12() const { return ___Enemy_12; }
	inline Transform_t3600365921 ** get_address_of_Enemy_12() { return &___Enemy_12; }
	inline void set_Enemy_12(Transform_t3600365921 * value)
	{
		___Enemy_12 = value;
		Il2CppCodeGenWriteBarrier((&___Enemy_12), value);
	}

	inline static int32_t get_offset_of_cooldownTimer_13() { return static_cast<int32_t>(offsetof(PlayerShooting_t3504758017, ___cooldownTimer_13)); }
	inline float get_cooldownTimer_13() const { return ___cooldownTimer_13; }
	inline float* get_address_of_cooldownTimer_13() { return &___cooldownTimer_13; }
	inline void set_cooldownTimer_13(float value)
	{
		___cooldownTimer_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSHOOTING_T3504758017_H
#ifndef SECONDSLOT_T2904157877_H
#define SECONDSLOT_T2904157877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SecondSlot
struct  SecondSlot_t2904157877  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECONDSLOT_T2904157877_H
#ifndef SELFDESTRUCT_T1181332799_H
#define SELFDESTRUCT_T1181332799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelfDestruct
struct  SelfDestruct_t1181332799  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SelfDestruct::timer
	float ___timer_4;

public:
	inline static int32_t get_offset_of_timer_4() { return static_cast<int32_t>(offsetof(SelfDestruct_t1181332799, ___timer_4)); }
	inline float get_timer_4() const { return ___timer_4; }
	inline float* get_address_of_timer_4() { return &___timer_4; }
	inline void set_timer_4(float value)
	{
		___timer_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELFDESTRUCT_T1181332799_H
#ifndef SLOT_T3333057830_H
#define SLOT_T3333057830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slot
struct  Slot_t3333057830  : public MonoBehaviour_t3962482529
{
public:
	// Inventory Slot::inventory
	Inventory_t1050226016 * ___inventory_4;
	// System.Int32 Slot::i
	int32_t ___i_5;
	// UnityEngine.GameObject Slot::deleteSound
	GameObject_t1113636619 * ___deleteSound_6;

public:
	inline static int32_t get_offset_of_inventory_4() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___inventory_4)); }
	inline Inventory_t1050226016 * get_inventory_4() const { return ___inventory_4; }
	inline Inventory_t1050226016 ** get_address_of_inventory_4() { return &___inventory_4; }
	inline void set_inventory_4(Inventory_t1050226016 * value)
	{
		___inventory_4 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_4), value);
	}

	inline static int32_t get_offset_of_i_5() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___i_5)); }
	inline int32_t get_i_5() const { return ___i_5; }
	inline int32_t* get_address_of_i_5() { return &___i_5; }
	inline void set_i_5(int32_t value)
	{
		___i_5 = value;
	}

	inline static int32_t get_offset_of_deleteSound_6() { return static_cast<int32_t>(offsetof(Slot_t3333057830, ___deleteSound_6)); }
	inline GameObject_t1113636619 * get_deleteSound_6() const { return ___deleteSound_6; }
	inline GameObject_t1113636619 ** get_address_of_deleteSound_6() { return &___deleteSound_6; }
	inline void set_deleteSound_6(GameObject_t1113636619 * value)
	{
		___deleteSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___deleteSound_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T3333057830_H
#ifndef SPAWN_T617419884_H
#define SPAWN_T617419884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spawn
struct  Spawn_t617419884  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Spawn::item
	GameObject_t1113636619 * ___item_4;
	// UnityEngine.GameObject Spawn::ThreeDirection
	GameObject_t1113636619 * ___ThreeDirection_5;

public:
	inline static int32_t get_offset_of_item_4() { return static_cast<int32_t>(offsetof(Spawn_t617419884, ___item_4)); }
	inline GameObject_t1113636619 * get_item_4() const { return ___item_4; }
	inline GameObject_t1113636619 ** get_address_of_item_4() { return &___item_4; }
	inline void set_item_4(GameObject_t1113636619 * value)
	{
		___item_4 = value;
		Il2CppCodeGenWriteBarrier((&___item_4), value);
	}

	inline static int32_t get_offset_of_ThreeDirection_5() { return static_cast<int32_t>(offsetof(Spawn_t617419884, ___ThreeDirection_5)); }
	inline GameObject_t1113636619 * get_ThreeDirection_5() const { return ___ThreeDirection_5; }
	inline GameObject_t1113636619 ** get_address_of_ThreeDirection_5() { return &___ThreeDirection_5; }
	inline void set_ThreeDirection_5(GameObject_t1113636619 * value)
	{
		___ThreeDirection_5 = value;
		Il2CppCodeGenWriteBarrier((&___ThreeDirection_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWN_T617419884_H
#ifndef TOUCHEVENT_T1418210080_H
#define TOUCHEVENT_T1418210080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchEvent
struct  TouchEvent_t1418210080  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 TouchEvent::WorldPoint
	Vector2_t2156229523  ___WorldPoint_4;
	// UnityEngine.Camera TouchEvent::cam
	Camera_t4157153871 * ___cam_5;
	// UnityEngine.GameObject TouchEvent::SpawnItem
	GameObject_t1113636619 * ___SpawnItem_6;
	// UnityEngine.GameObject TouchEvent::UnequipSound
	GameObject_t1113636619 * ___UnequipSound_7;

public:
	inline static int32_t get_offset_of_WorldPoint_4() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___WorldPoint_4)); }
	inline Vector2_t2156229523  get_WorldPoint_4() const { return ___WorldPoint_4; }
	inline Vector2_t2156229523 * get_address_of_WorldPoint_4() { return &___WorldPoint_4; }
	inline void set_WorldPoint_4(Vector2_t2156229523  value)
	{
		___WorldPoint_4 = value;
	}

	inline static int32_t get_offset_of_cam_5() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___cam_5)); }
	inline Camera_t4157153871 * get_cam_5() const { return ___cam_5; }
	inline Camera_t4157153871 ** get_address_of_cam_5() { return &___cam_5; }
	inline void set_cam_5(Camera_t4157153871 * value)
	{
		___cam_5 = value;
		Il2CppCodeGenWriteBarrier((&___cam_5), value);
	}

	inline static int32_t get_offset_of_SpawnItem_6() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___SpawnItem_6)); }
	inline GameObject_t1113636619 * get_SpawnItem_6() const { return ___SpawnItem_6; }
	inline GameObject_t1113636619 ** get_address_of_SpawnItem_6() { return &___SpawnItem_6; }
	inline void set_SpawnItem_6(GameObject_t1113636619 * value)
	{
		___SpawnItem_6 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnItem_6), value);
	}

	inline static int32_t get_offset_of_UnequipSound_7() { return static_cast<int32_t>(offsetof(TouchEvent_t1418210080, ___UnequipSound_7)); }
	inline GameObject_t1113636619 * get_UnequipSound_7() const { return ___UnequipSound_7; }
	inline GameObject_t1113636619 ** get_address_of_UnequipSound_7() { return &___UnequipSound_7; }
	inline void set_UnequipSound_7(GameObject_t1113636619 * value)
	{
		___UnequipSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___UnequipSound_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHEVENT_T1418210080_H
#ifndef FIXEDJOYSTICK_T2618381211_H
#define FIXEDJOYSTICK_T2618381211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixedJoystick
struct  FixedJoystick_t2618381211  : public Joystick_t9498292
{
public:
	// UnityEngine.Vector2 FixedJoystick::joystickPosition
	Vector2_t2156229523  ___joystickPosition_9;
	// UnityEngine.Camera FixedJoystick::cam
	Camera_t4157153871 * ___cam_10;

public:
	inline static int32_t get_offset_of_joystickPosition_9() { return static_cast<int32_t>(offsetof(FixedJoystick_t2618381211, ___joystickPosition_9)); }
	inline Vector2_t2156229523  get_joystickPosition_9() const { return ___joystickPosition_9; }
	inline Vector2_t2156229523 * get_address_of_joystickPosition_9() { return &___joystickPosition_9; }
	inline void set_joystickPosition_9(Vector2_t2156229523  value)
	{
		___joystickPosition_9 = value;
	}

	inline static int32_t get_offset_of_cam_10() { return static_cast<int32_t>(offsetof(FixedJoystick_t2618381211, ___cam_10)); }
	inline Camera_t4157153871 * get_cam_10() const { return ___cam_10; }
	inline Camera_t4157153871 ** get_address_of_cam_10() { return &___cam_10; }
	inline void set_cam_10(Camera_t4157153871 * value)
	{
		___cam_10 = value;
		Il2CppCodeGenWriteBarrier((&___cam_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDJOYSTICK_T2618381211_H
#ifndef VARIABLEJOYSTICK_T2643911586_H
#define VARIABLEJOYSTICK_T2643911586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VariableJoystick
struct  VariableJoystick_t2643911586  : public Joystick_t9498292
{
public:
	// System.Boolean VariableJoystick::isFixed
	bool ___isFixed_9;
	// UnityEngine.Vector2 VariableJoystick::fixedScreenPosition
	Vector2_t2156229523  ___fixedScreenPosition_10;
	// UnityEngine.Vector2 VariableJoystick::joystickCenter
	Vector2_t2156229523  ___joystickCenter_11;

public:
	inline static int32_t get_offset_of_isFixed_9() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___isFixed_9)); }
	inline bool get_isFixed_9() const { return ___isFixed_9; }
	inline bool* get_address_of_isFixed_9() { return &___isFixed_9; }
	inline void set_isFixed_9(bool value)
	{
		___isFixed_9 = value;
	}

	inline static int32_t get_offset_of_fixedScreenPosition_10() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___fixedScreenPosition_10)); }
	inline Vector2_t2156229523  get_fixedScreenPosition_10() const { return ___fixedScreenPosition_10; }
	inline Vector2_t2156229523 * get_address_of_fixedScreenPosition_10() { return &___fixedScreenPosition_10; }
	inline void set_fixedScreenPosition_10(Vector2_t2156229523  value)
	{
		___fixedScreenPosition_10 = value;
	}

	inline static int32_t get_offset_of_joystickCenter_11() { return static_cast<int32_t>(offsetof(VariableJoystick_t2643911586, ___joystickCenter_11)); }
	inline Vector2_t2156229523  get_joystickCenter_11() const { return ___joystickCenter_11; }
	inline Vector2_t2156229523 * get_address_of_joystickCenter_11() { return &___joystickCenter_11; }
	inline void set_joystickCenter_11(Vector2_t2156229523  value)
	{
		___joystickCenter_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEJOYSTICK_T2643911586_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (Pickup_t999226966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[3] = 
{
	Pickup_t999226966::get_offset_of_inventory_4(),
	Pickup_t999226966::get_offset_of_itemButton_5(),
	Pickup_t999226966::get_offset_of_pickup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (PlayerController_t2064355688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	PlayerController_t2064355688::get_offset_of_joystick_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (PlayerMovement_t2731566919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[3] = 
{
	PlayerMovement_t2731566919::get_offset_of_MoveSpeed_4(),
	PlayerMovement_t2731566919::get_offset_of_h_5(),
	PlayerMovement_t2731566919::get_offset_of_v_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PlayerShooting_t3504758017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[10] = 
{
	PlayerShooting_t3504758017::get_offset_of_shootSound_4(),
	PlayerShooting_t3504758017::get_offset_of_bulletPrefab_5(),
	PlayerShooting_t3504758017::get_offset_of_ttag_6(),
	PlayerShooting_t3504758017::get_offset_of_target_7(),
	PlayerShooting_t3504758017::get_offset_of_targetdistance_8(),
	PlayerShooting_t3504758017::get_offset_of_dist_9(),
	PlayerShooting_t3504758017::get_offset_of_fireDelay_10(),
	PlayerShooting_t3504758017::get_offset_of_Distance_11(),
	PlayerShooting_t3504758017::get_offset_of_Enemy_12(),
	PlayerShooting_t3504758017::get_offset_of_cooldownTimer_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (SecondSlot_t2904157877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (SelfDestruct_t1181332799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[1] = 
{
	SelfDestruct_t1181332799::get_offset_of_timer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Slot_t3333057830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[3] = 
{
	Slot_t3333057830::get_offset_of_inventory_4(),
	Slot_t3333057830::get_offset_of_i_5(),
	Slot_t3333057830::get_offset_of_deleteSound_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (Spawn_t617419884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	Spawn_t617419884::get_offset_of_item_4(),
	Spawn_t617419884::get_offset_of_ThreeDirection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (TouchEvent_t1418210080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[4] = 
{
	TouchEvent_t1418210080::get_offset_of_WorldPoint_4(),
	TouchEvent_t1418210080::get_offset_of_cam_5(),
	TouchEvent_t1418210080::get_offset_of_SpawnItem_6(),
	TouchEvent_t1418210080::get_offset_of_UnequipSound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (Joystick_t9498292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[5] = 
{
	Joystick_t9498292::get_offset_of_handleLimit_4(),
	Joystick_t9498292::get_offset_of_joystickMode_5(),
	Joystick_t9498292::get_offset_of_inputVector_6(),
	Joystick_t9498292::get_offset_of_background_7(),
	Joystick_t9498292::get_offset_of_handle_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (JoystickMode_t3732604888)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[4] = 
{
	JoystickMode_t3732604888::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (FixedJoystick_t2618381211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	FixedJoystick_t2618381211::get_offset_of_joystickPosition_9(),
	FixedJoystick_t2618381211::get_offset_of_cam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (VariableJoystick_t2643911586), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	VariableJoystick_t2643911586::get_offset_of_isFixed_9(),
	VariableJoystick_t2643911586::get_offset_of_fixedScreenPosition_10(),
	VariableJoystick_t2643911586::get_offset_of_joystickCenter_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
